import { Redirect, Route } from 'react-router-dom';
import React from 'react';

const PermissionRoute = ({
  component: Component,
  url,
  permissionUrl,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) => {
      if (permissionUrl.includes(url)) {
        return <Component {...props} />;
      }
      return (
        <Redirect
          to={{
            pathname: '/dashboard',
            state: { from: props.location },
          }}
        />
      );
    }}
  />
);

export default PermissionRoute;
