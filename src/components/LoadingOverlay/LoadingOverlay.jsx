import React from 'react';
import { Spin } from 'antd';

import './loading.scss';

const LoadingOverlay = ({ isLoading, title }) => (
  <div className={`loading__overlay ${isLoading ? 'is-active' : ''}`}>
    <div className="loading__target">
      <Spin />
      <div className="loading__text">{title}</div>
    </div>
  </div>
);
export default LoadingOverlay;
