import React, { Component } from 'react';
import { Card, Checkbox, Col, Drawer, Row, Select, Tag } from 'antd';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import HtmlDiff from '../../../assets/js/htmldiff.min';
import {
  closeCompareVersionDrawer,
  getCompareVersionById,
} from '../../../actions/news.action';
import { isEmpty } from '../../../utils';

const { Option } = Select;
class CompareVersionDrawer extends Component {
  state = {
    oldVersion: null,
    newVersion: null,
    oldDetail: '',
    newDetail: '',
    diffHtml: '',
    isCompare: false,
  };

  handleOldVersionChange = async (encryptId, oldVersion) => {
    const { getCompareVersionByIdConnect } = this.props;
    const oldDetail = await getCompareVersionByIdConnect(encryptId, oldVersion);
    this.setState({ oldVersion, oldDetail }, () => {
      const { isCompare } = this.state;
      if (isCompare) {
        this.handleDiffChange();
      }
    });
  };

  handleNewVersionChange = async (encryptId, newVersion) => {
    const { getCompareVersionByIdConnect } = this.props;
    const newDetail = await getCompareVersionByIdConnect(encryptId, newVersion);
    this.setState({ newVersion, newDetail }, () => {
      const { isCompare } = this.state;
      if (isCompare) {
        this.handleDiffChange();
      }
    });
  };

  handleCompareChange = (e) => {
    this.setState({ isCompare: e.target.checked }, () => {
      const { isCompare } = this.state;
      if (isCompare) {
        this.handleDiffChange();
      }
    });
  };

  handleDiffChange = () => {
    const { oldDetail, newDetail } = this.state;
    const { body: oldHtml } = oldDetail || {};
    const { body: newHtml } = newDetail || {};
    if (!isEmpty(oldHtml) && !isEmpty(newHtml)) {
      const diffHtml = HtmlDiff.execute(oldHtml, newHtml);
      this.setState({ diffHtml });
    }
  };

  handleCloseCompareVersionDrawerConnect = () => {
    const { closeCompareVersionDrawerConnect } = this.props;
    this.setState(
      {
        oldVersion: null,
        newVersion: null,
        oldDetail: '',
        newDetail: '',
        diffHtml: '',
        isCompare: false,
      },
      () => {
        closeCompareVersionDrawerConnect();
      },
    );
  };

  render() {
    const {
      isCompare,
      diffHtml,
      oldVersion,
      newVersion,
      oldDetail,
      newDetail,
    } = this.state;
    const { body: oldHtml } = oldDetail || {};
    const { body: newHtml } = newDetail || {};
    const {
      isVisible,
      newsReducer,
    } = this.props;
    const { newsVersion, currentEncryptId } = newsReducer || {};
    const TitleComponent = (
      <React.Fragment>
        <Select
          value={oldVersion}
          style={{ width: 120, marginRight: '5px' }}
          onChange={(value) =>
            this.handleOldVersionChange(currentEncryptId, value)
          }
          size="small"
        >
          {newsVersion &&
            newsVersion.length > 0 &&
            newsVersion.map((item) => {
              const { id, version } = item;
              return <Option value={id}>{version}</Option>;
            })}
        </Select>
        <Select
          value={newVersion}
          style={{ width: 120, marginRight: '5px' }}
          onChange={(value) =>
            this.handleNewVersionChange(currentEncryptId, value)
          }
          size="small"
        >
          {newsVersion &&
            newsVersion.length > 0 &&
            newsVersion.map((item) => {
              const { id, version } = item;
              return <Option value={id}>{version}</Option>;
            })}
        </Select>
        <Checkbox onChange={this.handleCompareChange}>
          So sánh thay đổi
        </Checkbox>
      </React.Fragment>
    );
    return (
      <Drawer
        placement="bottom"
        getContainer={false}
        className="drawer-post-editor"
        visible={isVisible}
        closable
        destroyOnClose
        title={TitleComponent}
        style={{ textAlign: 'center' }}
        onClose={this.handleCloseCompareVersionDrawerConnect}
      >
        {isCompare && (
          <div
            style={{
              textAlign: 'right',
              padding: '10px',
              width: '100%',
              marginBottom: '10px',
            }}
          >
            <Tag color="grey">Dữ liệu không thay đổi</Tag>
            <Tag color="green">Dữ liệu mới được thêm vào</Tag>
            <Tag color="red">Dữ liệu đã bị xóa</Tag>
          </div>
        )}
        <div className="main-container">
          <Row>
            <Col span={12}>
              <Card style={{ width: '100%' }}>
                {/* eslint-disable-next-line react/no-danger */}
                <div dangerouslySetInnerHTML={{ __html: oldHtml }} />
              </Card>
            </Col>
            <Col span={12}>
              <Card style={{ width: '100%' }}>
                {isCompare ? (
                  <Card style={{ width: '100%' }}>
                    {/* eslint-disable-next-line react/no-danger */}
                    <div dangerouslySetInnerHTML={{ __html: diffHtml }} />
                  </Card>
                ) : (
                  // eslint-disable-next-line react/no-danger
                  <div dangerouslySetInnerHTML={{ __html: newHtml }} />
                )}
              </Card>
            </Col>
          </Row>
        </div>
      </Drawer>
    );
  }
}

const mapStateToProps = (state) => ({
  newsReducer: state.news,
});

const mapDispatchToProps = {
  closeCompareVersionDrawerConnect: closeCompareVersionDrawer,
  getCompareVersionByIdConnect: getCompareVersionById,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(CompareVersionDrawer),
);
