import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Drawer, Select, message, Modal, Input } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import isEqual from 'react-fast-compare';

import {
  EnumDisplayPosition,
  EnumNewsRelationModal,
  EnumNewsSidebar,
} from '../../../constants/enum';

import NewsEditor from './components/NewsEditor/NewsEditor';
import {
  closeAddNewsDrawer,
  getNewsRelation, reloadNews,
  saveNews,
  showAddNewsDrawer,
  updateNewsInUsing,
  updateNewsStatus,
} from '../../../actions/news.action';
import NewsToolbar from './components/NewsToolbar/NewsToolbar';
import {
  addNewsPermission,
  commonConst,
  EnumNewsType,
  EnumType,
} from '../../../constants';
import { onFieldChange, uploadAvatar, validateBeforeSubmit } from './routines';
import NewsAction from './components/NewsAction/NewsAction';
import { isEmpty } from '../../../utils';

const { Option } = Select;
const { confirm, error } = Modal;
const { TextArea } = Input;

class AddNewsDrawer extends Component {
  constructor(props) {
    super(props);
    const { newsReducer, royaltyReducer } = props;
    const { initData } = royaltyReducer || {};
    const { detail, newsRelationChose } = newsReducer || {};
    const cloneDetail = { ...detail };
    this.state = {
      editingNews: cloneDetail,
      newsEditor: cloneDetail.newsEditor,
      basicInfo: cloneDetail.basicInfo,
      releaseConfig: cloneDetail.releaseConfig,
      royaltyConfig: cloneDetail.royaltyConfig,
      currentRoyalty: initData,
      newsRelationChose,
      optionsType: Object.entries(EnumType).map(([key, data]) => (
        <Option key={data.id} value={data.id}>
          {data.description}
        </Option>
      )),
      optionsNewsType: Object.entries(EnumNewsType).map(([key, data]) => (
        <Option key={data.id} value={data.id}>
          {data.description}
        </Option>
      )),
      optionDisplayPosition: Object.entries(EnumDisplayPosition).map(
        ([key, data]) => (
          <Option key={data.id} value={data.id}>
            {data.description}
          </Option>
        ),
      ),
      newsRelationModal: {
        isVisible: false,
        isSubmit: false,
        title: EnumNewsRelationModal.RELATION.description,
        maxItemChoose: 3,
        keyword: '',
        zoneId: -1,
        pageSize: 5,
        pageIndex: 1,
      },
      isVisibleRoyaltyModal: false,
    };
  }

  componentDidUpdate(prevProps, prevState, prevContext) {
    const { newsReducer: prevNewsReducer } = prevProps;
    const { detail: prevDetail, newsRelationChose: prevNewsRelationChose } =
      prevNewsReducer || {};
    const { newsReducer: thisNewsReducer } = this.props;
    const { detail: thisDetail, newsRelationChose } = thisNewsReducer || {};
    if (!isEqual(prevDetail, thisDetail)) {
      const cloneThisDetail = { ...thisDetail };
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        editingNews: cloneThisDetail,
        newsEditor: cloneThisDetail.newsEditor,
        basicInfo: cloneThisDetail.basicInfo,
        releaseConfig: cloneThisDetail.releaseConfig,
        royaltyConfig: cloneThisDetail.royaltyConfig,
      });
    }

    if (!isEqual(prevNewsRelationChose, newsRelationChose)) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({ newsRelationChose });
    }
  }

  // Editor
  handleTitleChange = (value) => {
    const { newsEditor } = this.state;
    const cloneNewsEditor = { ...newsEditor };
    cloneNewsEditor.title = value;
    this.setState({
      newsEditor: cloneNewsEditor,
    });
  };
  handleSapoChange = (value) => {
    const { newsEditor } = this.state;
    const cloneNewsEditor = { ...newsEditor };
    cloneNewsEditor.sapo = value;
    this.setState({
      newsEditor: cloneNewsEditor,
    });
  };
  handleBodyChange = (event, editor) => {
    const data = editor.getData();
    const { newsEditor } = this.state;
    const cloneNewsEditor = { ...newsEditor };
    cloneNewsEditor.body = data;
    this.setState({
      newsEditor: cloneNewsEditor,
    });
  };

  // BasicInfo
  handleBasicInfoChange = (name, value) => {
    const { basicInfo } = this.state;
    const cloneBasicInfo = { ...basicInfo };
    cloneBasicInfo[name] = value;
    this.setState({
      basicInfo: cloneBasicInfo,
    });
  };
  handleBeforeUpload = (file) => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJpgOrPng && isLt2M;
  };
  handleNormalAvatarChange = (info) => {
    if (info.file.status === 'done') {
      this.handleBasicInfoChange('avatarNormal', info.file.response.file.url);
    }
  };
  handleVerticalAvatarChange = (info) => {
    if (info.file.status === 'done') {
      this.handleBasicInfoChange('avatarVertical', info.file.response.file.url);
    }
  };
  handleSquareAvatarChange = (info) => {
    if (info.file.status === 'done') {
      this.handleBasicInfoChange('avatarSquare', info.file.response.file.url);
    }
  };
  handleFacebookAvatarChange = (info) => {
    if (info.file.status === 'done') {
      this.handleBasicInfoChange('avatarFacebook', info.file.response.file.url);
    }
  };

  // ReleaseConfig
  handleReleaseConfigChange = (name, value) => {
    const { releaseConfig } = this.state;
    const cloneReleaseConfig = { ...releaseConfig };
    cloneReleaseConfig[name] = value;
    this.setState({
      releaseConfig: cloneReleaseConfig,
    });
  };

  // SEO Config
  handleSEOConfigClick = () => {
    Modal.info({
      title: 'Thông báo',
      content: (
        <div>
          <p>Tính năng đang phát triển</p>
        </div>
      ),
      onOk() {},
    });
  };

  // News Relative
  handleNewsRelationModalAction = (isVisible, modalType, isSubmit = false) => {
    const { newsRelationModal, newsRelationChose } = this.state;
    const { relationItems, relationSpecialItems } = newsRelationChose || {};
    const cloneNewsRelationModal = { ...newsRelationModal };
    cloneNewsRelationModal.isVisible = isVisible;
    cloneNewsRelationModal.isSubmit = isSubmit;
    if (isVisible) {
      if (modalType === EnumNewsRelationModal.RELATION.id) {
        cloneNewsRelationModal.title =
          EnumNewsRelationModal.RELATION.description;
        cloneNewsRelationModal.maxItemChoose =
          EnumNewsRelationModal.RELATION.maxItemChoose;
      } else {
        cloneNewsRelationModal.title =
          EnumNewsRelationModal.RELATION_SPECIAL.description;
        cloneNewsRelationModal.maxItemChoose =
          EnumNewsRelationModal.RELATION_SPECIAL.maxItemChoose;
      }
      this.setState({ newsRelationModal: cloneNewsRelationModal }, () => {
        this.props.getNewsRelationConnect();
      });
    } else {
      // eslint-disable-next-line no-lonely-if
      // if (isSubmit) {
      //       //   this.setState({ newsRelationModal: cloneNewsRelationModal });
      //       // }
      // let needConfirm;
      // if (modalType === EnumNewsRelationModal.RELATION.id) {
      //   needConfirm = relationItems && relationItems.length > 0;
      // } else {
      //   needConfirm = relationSpecialItems && relationSpecialItems.length > 0;
      // }
      // if (needConfirm) {
      //   Modal.confirm({
      //     title: 'Cảnh báo',
      //     content:
      //       'Đóng popup sẽ xóa dữ liệu hiện tại. Bạn có chắc chắn muốn đóng không?',
      //     okText: 'Đồng ý',
      //     cancelText: 'Hủy',
      //     onOk: () => {
      //       this.setState({ newsRelationModal: cloneNewsRelationModal });
      //     },
      //   });
      // } else {
      //   this.setState({ newsRelationModal: cloneNewsRelationModal });
      // }
      this.setState({ newsRelationModal: cloneNewsRelationModal });
    }
  };
  handleNewsRelationKeywordChange = (keyword) => {
    const { newsRelationModal } = this.state;
    const cloneNewsRelationModal = { ...newsRelationModal };
    cloneNewsRelationModal.keyword = keyword;
    this.setState({ newsRelationModal: cloneNewsRelationModal });
  };
  handleNewsRelationZoneChange = (zoneId) => {
    const { newsRelationModal } = this.state;
    const cloneNewsRelationModal = { ...newsRelationModal };
    cloneNewsRelationModal.zoneId = zoneId;
    this.setState({ newsRelationModal: cloneNewsRelationModal });
  };
  handleNewsRelationReset = async (type, isSubmit) => {
    // if (!isSubmit) {
    //   await this.handleRelationChoseReset(type);
    // }
    await this.handleRelationModalReset();
  };
  handleRelationModalReset = async () => {
    const { newsRelationModal } = this.state;
    const cloneNewsRelationModal = { ...newsRelationModal };
    cloneNewsRelationModal.zoneId = commonConst.defaultOptions;
    cloneNewsRelationModal.maxItemChoose =
      EnumNewsRelationModal.RELATION.maxItemChoose;
    cloneNewsRelationModal.title = EnumNewsRelationModal.RELATION.description;
    cloneNewsRelationModal.keyword = commonConst.defaultObject;
    await this.setState({ newsRelationModal: cloneNewsRelationModal });
  };
  handleRelationChoseReset = async (type) => {
    const { newsRelationChose } = this.state;
    const cloneNewsRelationChose = { ...newsRelationChose };
    if (type === EnumNewsRelationModal.RELATION.id) {
      cloneNewsRelationChose.relationItems = [];
    } else {
      cloneNewsRelationChose.relationSpecialItems = [];
    }
    await this.setState({ newsRelationChose: cloneNewsRelationChose });
  };
  handleNewsRelationSearchClick = (value = 1) => {
    const { newsRelationModal } = this.state;
    const cloneNewsRelationModal = { ...newsRelationModal };
    cloneNewsRelationModal.pageIndex = value;
    this.setState({ newsRelationModal: cloneNewsRelationModal }, () => {
      const { getNewsRelationConnect } = this.props;
      const { newsRelationModal } = this.state;
      const { keyword, zoneId, pageSize, pageIndex } = newsRelationModal || {};
      getNewsRelationConnect(zoneId, keyword, pageSize, pageIndex);
    });
  };
  handleNewsRelationItemClick = (item, type) => {
    const { newsRelationChose } = this.state;
    const cloneNewsRelationChose = { ...newsRelationChose };
    let itemIndex = -1;
    if (type === EnumNewsRelationModal.RELATION.id) {
      if (
        cloneNewsRelationChose.relationItems &&
        cloneNewsRelationChose.relationItems.length > 0
      ) {
        itemIndex = cloneNewsRelationChose.relationItems.findIndex(
          (i) => i.encryptId === item.encryptId,
        );
      }
      if (itemIndex !== -1) {
        cloneNewsRelationChose.relationItems.splice(itemIndex, 1);
      } else if (
        cloneNewsRelationChose.relationItems &&
        cloneNewsRelationChose.relationItems.length <
          EnumNewsRelationModal.RELATION.maxItemChoose
      ) {
        cloneNewsRelationChose.relationItems.push(item);
      } else {
        message.warning(
          `Chỉ được thêm tối đa ${EnumNewsRelationModal.RELATION.maxItemChoose} tin tức liên quan`,
        );
        return;
      }
    } else {
      if (
        cloneNewsRelationChose.relationSpecialItems &&
        cloneNewsRelationChose.relationSpecialItems.length > 0
      ) {
        itemIndex = cloneNewsRelationChose.relationSpecialItems.findIndex(
          (i) => i.encryptId === item.encryptId,
        );
      }

      if (itemIndex !== -1) {
        cloneNewsRelationChose.relationSpecialItems.splice(itemIndex, 1);
      } else if (
        cloneNewsRelationChose.relationSpecialItems &&
        cloneNewsRelationChose.relationSpecialItems.length <
          EnumNewsRelationModal.RELATION_SPECIAL.maxItemChoose
      ) {
        cloneNewsRelationChose.relationSpecialItems.push(item);
      } else {
        message.warning(
          `Chỉ được thêm tối đa ${EnumNewsRelationModal.RELATION_SPECIAL.maxItemChoose} tin tức liên quan chân bài`,
        );
        return;
      }
    }
    this.setState({ newsRelationChose: cloneNewsRelationChose });
  };
  handleNewsRelationChoseRemove = (encryptId, type) => {
    const { newsRelationChose } = this.state;
    const cloneNewsRelationChose = { ...newsRelationChose };
    let itemIndex = -1;
    if (type === EnumNewsRelationModal.RELATION.id) {
      itemIndex = cloneNewsRelationChose.relationItems.findIndex(
        (i) => i.encryptId === encryptId,
      );
      cloneNewsRelationChose.relationItems.splice(itemIndex, 1);
    } else {
      itemIndex = cloneNewsRelationChose.relationSpecialItems.findIndex(
        (i) => i.encryptId === encryptId,
      );
      cloneNewsRelationChose.relationSpecialItems.splice(itemIndex, 1);
    }
    this.setState({ newsRelationChose: cloneNewsRelationChose });
  };
  handleNewsRelationChoseReOrder = (items, type) => {
    const { newsRelationChose } = this.state;
    const cloneNewsRelationChose = { ...newsRelationChose };
    if (type === EnumNewsRelationModal.RELATION.id) {
      cloneNewsRelationChose.relationItems = items;
    } else {
      cloneNewsRelationChose.relationSpecialItems = items;
    }
    this.setState({ newsRelationChose: cloneNewsRelationChose });
  };

  // Royalty
  handleRoyaltyModalVisible = (isVisible) => {
    const { royaltyConfig } = this.state;
    const { royalties } = royaltyConfig || {};
    const { royaltyReducer } = this.props;
    const { initData } = royaltyReducer || {};
    const cloneInitData = { ...initData };
    if (royalties && royalties.length > 0) {
      cloneInitData.categoryId = royalties[0].categoryId;
      cloneInitData.disableCategory = true;
    }
    this.setState({
      isVisibleRoyaltyModal: isVisible,
      currentRoyalty: cloneInitData,
    });
  };
  handleRoyaltyAdd = () => {
    const { royaltyConfig } = this.state;
    const { royalties } = royaltyConfig || {};
    const { royaltyReducer } = this.props;
    const { initData } = royaltyReducer || {};
    const cloneInitData = { ...initData };
    if (royalties && royalties.length > 0) {
      cloneInitData.categoryId = royalties[0].categoryId;
      cloneInitData.disableCategory = true;
    }
    this.setState({
      currentRoyalty: cloneInitData,
    });
  };
  handleRoyaltyEdit = (index) => {
    const { royaltyConfig } = this.state;
    const { royalties } = royaltyConfig || {};
    const { id } = royalties[index] || {};
    if (isEmpty(id)) {
      royalties[index].index = index;
    }
    this.setState({ currentRoyalty: royalties[index] });
  };
  handleRoyaltyDelete = (index) => {
    const { royaltyConfig } = this.state;
    const { royaltyReducer } = this.props;
    const { initData } = royaltyReducer || {};
    const cloneInitData = { ...initData };
    const cloneRoyaltyConfig = { ...royaltyConfig };
    const action = () => {
      cloneRoyaltyConfig.royalties.splice(index, 1);
      if (
        cloneRoyaltyConfig.royalties &&
        cloneRoyaltyConfig.royalties.length > 0
      ) {
        cloneInitData.categoryId = cloneRoyaltyConfig.royalties[0].categoryId;
        cloneInitData.disableCategory = true;
      }
      this.setState({
        royaltyConfig: cloneRoyaltyConfig,
        currentRoyalty: cloneInitData,
      });
    };
    confirm({
      title: 'Xác nhận',
      icon: <ExclamationCircleOutlined />,
      content: 'Bạn có chắc chắn muốn xóa thông tin chấm nhuận không?',
      onOk() {
        action();
      },
    });
  };
  handleRoyaltySave = (values) => {
    const { royaltyConfig, currentRoyalty } = this.state;
    const { id, index } = currentRoyalty || {};
    const { royaltyReducer } = this.props;
    const { initData } = royaltyReducer || {};
    const cloneInitData = { ...initData };
    const cloneRoyaltyConfig = { ...royaltyConfig };
    const { royalties } = cloneRoyaltyConfig || {};
    const {
      categoryId,
      photoCount,
      videoCount,
      qualityValue,
      dedicationValue,
      riskValue,
      contributionValue,
      royaltyFor,
      note,
    } = values || {};
    const royalty = {
      categoryId,
      photoCount,
      videoCount,
      qualityValue,
      dedicationValue,
      riskValue,
      contributionValue,
      royaltyFor,
      note,
    };
    if (royalties && royalties.length > 0) {
      const currentIndex = royalties.findIndex(
        (x) => x.royaltyFor === royaltyFor,
      );
      if (currentIndex > -1) {
        error({
          title: 'Lỗi',
          content: 'Không thể chấm nhuận cho một người nhiều lần!',
        });
        return;
      }
    }
    if (!isEmpty(id)) {
      const currentIndex = royalties.findIndex((x) => x.id === id);
      royalty.id = id;
      royalties[currentIndex] = royalty;
    } else if (!isEmpty(index)) {
      royalties[index] = royalty;
    } else {
      royalties.push(royalty);
    }

    cloneInitData.categoryId = categoryId;
    cloneInitData.disableCategory = true;

    this.setState({
      royaltyConfig: cloneRoyaltyConfig,
      currentRoyalty: cloneInitData,
    });
  };
  handleRoyaltyCancel = () => {
    const { newsReducer } = this.props;
    const { detail } = newsReducer || {};
    const action = () => {
      this.setState({ royaltyConfig: detail.royaltyConfig });
      this.handleRoyaltyModalVisible(false);
    };
    confirm({
      title: 'Xác nhận',
      icon: <ExclamationCircleOutlined />,
      content:
        'Toàn bộ dữ liệu chỉnh sửa mới sẽ không được lưu. Bạn có muốn tiếp tục?',
      onOk() {
        action();
      },
    });
  };
  handleRoyaltySubmit = () => {
    this.handleRoyaltyModalVisible(false);
  };

  // Action
  handleNewsClose = (encryptId, currentStatus) => {
    const {
      royaltyReducer,
      updateNewsInUsingConnect,
      updateNewsStatusConnect,
      closeAddNewsDrawerConnect,
      reloadNewsConnect,
    } = this.props;
    const { initData } = royaltyReducer || {};
    const cloneInitData = { ...initData };
    const action = () => {
      this.setState({
        currentRoyalty: cloneInitData,
      });
    };
    confirm({
      title: 'Xác nhận',
      content: 'Bạn có chắc chắn muốn đóng popup?',
      onOk() {
        action();
        const isEdit = false;
        if (encryptId > 0) {
          updateNewsInUsingConnect(encryptId, isEdit);
        }
        if (currentStatus === EnumNewsSidebar.WAITING_EDITING_POST.id) {
          updateNewsStatusConnect(
            encryptId,
            currentStatus,
            EnumNewsSidebar.RECEIVED_EDITING_POST.id,
          );
        }
        reloadNewsConnect(true);
        closeAddNewsDrawerConnect();
      },
    });
  };
  handleNewsEdit = () => {
    const { editingNews } = this.state;
    const { encryptId } = editingNews || {};
    const { showAddNewsDrawerConnect } = this.props;
    const isDisable = false;
    showAddNewsDrawerConnect(encryptId, isDisable);
  };
  handleNewsSend = async (encryptId, currentStatus) => {
    const { updateNewsStatusConnect, closeAddNewsDrawerConnect } = this.props;
    let nextStatus;
    if (currentStatus === EnumNewsSidebar.TEMP_POST.id) {
      nextStatus = EnumNewsSidebar.WAITING_EDITING_POST.id;
    } else if (currentStatus === EnumNewsSidebar.RECEIVED_EDITING_POST.id) {
      nextStatus = EnumNewsSidebar.WAITING_RELEASE_POST.id;
    }
    await updateNewsStatusConnect(encryptId, currentStatus, nextStatus);
    closeAddNewsDrawerConnect();
  };
  handleNewsRelease = (encryptId) => {
    const action = () => {
      this.handleNewsEdit(encryptId);
    };
    confirm({
      title: 'Xác nhận',
      icon: <ExclamationCircleOutlined />,
      content:
        'Cần vào form soạn bài để kiểm tra thông tin xuất bản. Chuyển sang form soạn bài ngay?',
      onOk() {
        action();
      },
    });
  };
  handleNewsRemove = (encryptId, currentStatus) => {
    const { updateNewsStatusConnect, closeAddNewsDrawerConnect } = this.props;
    updateNewsStatusConnect(
      encryptId,
      currentStatus,
      EnumNewsSidebar.REMOVED_POST.id,
    );
    closeAddNewsDrawerConnect();
  };
  handleNewsReturn = (encryptId, currentStatus) => {
    const { updateNewsStatusConnect } = this.props;
    confirm({
      title: 'Trả lại bài viết',
      icon: <ExclamationCircleOutlined />,
      content: <TextArea rows={4} placeholder="Lời nhắn trả lại bài viết" />,
      onOk() {
        if (currentStatus === EnumNewsSidebar.WAITING_EDITING_POST.id) {
          updateNewsStatusConnect(
            encryptId,
            currentStatus,
            updateNewsStatusConnect(
              encryptId,
              currentStatus,
              EnumNewsSidebar.TEMP_POST.id,
            ),
          );
        } else if (currentStatus === EnumNewsSidebar.WAITING_RELEASE_POST.id) {
          updateNewsStatusConnect(
            encryptId,
            currentStatus,
            EnumNewsSidebar.RECEIVED_EDITING_POST.id,
          );
        } else if (currentStatus === EnumNewsSidebar.RECEIVED_EDITING_POST.id) {
          updateNewsStatusConnect(
            encryptId,
            currentStatus,
            EnumNewsSidebar.WAITING_EDITING_POST.id,
          );
        } else if (currentStatus === EnumNewsSidebar.RECEIVED_RELEASE_POST.id) {
          updateNewsStatusConnect(
            encryptId,
            currentStatus,
            EnumNewsSidebar.RECEIVED_EDITING_POST.id,
          );
        }
      },
    });
  };
  handleNewsSave = (isSend = false, isRelease = false) => {
    const {
      editingNews,
      newsRelationChose,
      newsEditor,
      basicInfo,
      releaseConfig,
      royaltyConfig,
    } = this.state;
    const { saveNewsConnect, closeAddNewsDrawerConnect, reloadNewsConnect } = this.props;
    const cloneEditingNews = { ...editingNews };
    const { status } = cloneEditingNews || {};
    const { relationItems, relationSpecialItems } = newsRelationChose || {};
    const { royalties } = royaltyConfig || {};
    cloneEditingNews.newsEditor = newsEditor;
    cloneEditingNews.basicInfo = basicInfo;
    cloneEditingNews.releaseConfig = releaseConfig;
    cloneEditingNews.royaltyConfig = royaltyConfig;
    if (relationItems && relationItems.length > 0) {
      cloneEditingNews.newsRelation = relationItems
        .map((i) => i.encryptId)
        .join(',');
    }
    if (relationSpecialItems && relationSpecialItems.length > 0) {
      cloneEditingNews.newsRelationSpecial = relationSpecialItems
        .map((i) => i.encryptId)
        .join(',');
    }

    if (isSend) {
      if (
        [EnumNewsSidebar.MY_POST.id, EnumNewsSidebar.TEMP_POST.id].includes(
          status,
        )
      ) {
        cloneEditingNews.status = EnumNewsSidebar.WAITING_EDITING_POST.id;
      } else if (status === EnumNewsSidebar.WAITING_EDITING_POST.id) {
        cloneEditingNews.status = EnumNewsSidebar.RECEIVED_EDITING_POST.id;
      } else if (status === EnumNewsSidebar.RECEIVED_EDITING_POST.id) {
        cloneEditingNews.status = EnumNewsSidebar.WAITING_RELEASE_POST.id;
      } else if (status === EnumNewsSidebar.WAITING_RELEASE_POST.id) {
        cloneEditingNews.status = EnumNewsSidebar.RECEIVED_RELEASE_POST.id;
      }
    }
    if (isRelease) {
      // if (royalties && royalties.length === 0) {
      //   Modal.error({
      //     title: 'Thông báo',
      //     content: 'Vui lòng chấm nhuận trước khi xuất bản bài',
      //   });
      //   return;
      // }
      cloneEditingNews.status = EnumNewsSidebar.RELEASE_POST.id;
    } else if (isEmpty(status)) {
      cloneEditingNews.status = EnumNewsSidebar.TEMP_POST.id;
    }

    this.setState({ editingNews: cloneEditingNews }, async () => {
      const { editingNews } = this.state;
      const erros = validateBeforeSubmit(editingNews);
      if (erros.length > 0) {
        Modal.error({
          title: 'Lưu bài viết không thành công',
          content: erros.map((error) => <p key={error}>{error}</p>),
        });
        return;
      }

      const result = await saveNewsConnect(editingNews);
      if (parseInt(result, 10) > 0) {
        confirm({
          title: 'Xác nhận',
          icon: <ExclamationCircleOutlined />,
          content:
            'Bài viết đã được lưu thành công. Bạn có muốn đóng form chỉnh sửa?',
          onOk() {
            reloadNewsConnect(true);
            closeAddNewsDrawerConnect();
          },
        });
      } else {
        Modal.error({
          title: 'Lỗi',
          content: 'Lưu bài viết không thành công',
        });
      }
    });
  };
  render() {
    const {
      editingNews,
      basicInfo,
      releaseConfig,
      royaltyConfig,
      newsRelationChose,
      optionsType,
      optionsNewsType,
      optionDisplayPosition,
      newsRelationModal,
      isVisibleRoyaltyModal,
      newsEditor,
      currentRoyalty,
    } = this.state;
    const { encryptId, status } = editingNews || {};
    const {
      placement = 'bottom',
      visible,
      isDisable,
      globalReducer,
      newsReducer,
      authReducer,
    } = this.props;
    const { zones } = globalReducer || {};
    const { permission } = authReducer || {};
    const { newsRelation } = newsReducer || {};
    let permissionZone = null;
    if (permission && permission.length > 0) {
      const index = permission.findIndex((x) => x.id === addNewsPermission);
      if (index !== -1) {
        permissionZone = permission[index].zoneIds;
      }
    }
    return (
      <Drawer
        placement={placement}
        onClose={() => this.handleNewsClose(encryptId, status)}
        getContainer={false}
        className="drawer-post-editor"
        visible={visible}
        closable
        forceRender
        destroyOnClose
      >
        <div className="post-editor-body">
          <div className="row no-gutters h-100 flex-nowrap">
            <NewsEditor
              newsEditor={newsEditor}
              isDisable={isDisable}
              onTitleChange={this.handleTitleChange}
              onSapoChange={this.handleSapoChange}
              onBodyChange={this.handleBodyChange}
            />
            <NewsToolbar
              basicInfo={basicInfo}
              onBasicInfoChange={this.handleBasicInfoChange}
              releaseConfig={releaseConfig}
              royaltyConfig={royaltyConfig}
              currentRoyalty={currentRoyalty}
              newsRelation={newsRelation}
              newsRelationChose={newsRelationChose}
              isDisable={isDisable}
              optionsType={optionsType}
              optionsNewsType={optionsNewsType}
              optionDisplayPosition={optionDisplayPosition}
              zones={zones}
              permissionZone={permissionZone}
              newsRelationModal={newsRelationModal}
              isVisibleRoyaltyModal={isVisibleRoyaltyModal}
              onRoyaltyAdd={this.handleRoyaltyAdd}
              onRoyaltyEdit={this.handleRoyaltyEdit}
              onRoyaltyDelete={this.handleRoyaltyDelete}
              onRoyaltySave={this.handleRoyaltySave}
              onRoyaltySubmit={this.handleRoyaltySubmit}
              onRoyaltyCancel={this.handleRoyaltyCancel}
              onRoyaltyModalVisible={this.handleRoyaltyModalVisible}
              onNewsRelationModalAction={this.handleNewsRelationModalAction}
              onNormalAvatarChange={this.handleNormalAvatarChange}
              onVerticalAvatarChange={this.handleVerticalAvatarChange}
              onSquareAvatarChange={this.handleSquareAvatarChange}
              onFacebookAvatarChange={this.handleFacebookAvatarChange}
              onBeforeUpload={this.handleBeforeUpload}
              onReleaseConfigChange={this.handleReleaseConfigChange}
              onNewsRelationKeywordChange={this.handleNewsRelationKeywordChange}
              onNewsRelationZoneChange={this.handleNewsRelationZoneChange}
              onNewsRelationSearchClick={this.handleNewsRelationSearchClick}
              onNewsRelationItemClick={this.handleNewsRelationItemClick}
              onNewsRelationChoseRemove={this.handleNewsRelationChoseRemove}
              onNewsRelationChoseReOrder={this.handleNewsRelationChoseReOrder}
              onNewsRelationReset={this.handleNewsRelationReset}
              onSEOConfigClick={this.handleSEOConfigClick}
            />
          </div>
        </div>
        <NewsAction
          status={status}
          encryptId={encryptId}
          isDisable={isDisable}
          onClose={() => this.handleNewsClose(encryptId, status)}
          onNewsEdit={this.handleNewsEdit}
          onNewsSave={() => this.handleNewsSave()}
          onNewsSaveSend={() => this.handleNewsSave(true, false)}
          onNewsSaveRelease={() => this.handleNewsSave(false, true)}
          onNewsSend={() => this.handleNewsSend(encryptId, status)}
          onNewsRelease={() => this.handleNewsRelease(encryptId)}
          onNewsRemove={() => this.handleNewsRemove(encryptId, status)}
          onNewsReturn={() => this.handleNewsReturn(encryptId, status)}
        />
      </Drawer>
    );
  }
}

const mapStateToProps = (state) => ({
  newsReducer: state.news,
  globalReducer: state.global,
  authReducer: state.auth,
  royaltyReducer: state.royalty,
});
const mapDispatchToProps = {
  saveNewsConnect: saveNews,
  showAddNewsDrawerConnect: showAddNewsDrawer,
  closeAddNewsDrawerConnect: closeAddNewsDrawer,
  getNewsRelationConnect: getNewsRelation,
  updateNewsStatusConnect: updateNewsStatus,
  updateNewsInUsingConnect: updateNewsInUsing,
  reloadNewsConnect: reloadNews,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNewsDrawer);
