import React from 'react';
import { Input } from 'antd';
import PerfectScrollbar from 'react-perfect-scrollbar';
import CKEditor from '@ckeditor/ckeditor5-react';
import CKFinder from '@ckeditor/ckeditor5-ckfinder/src/ckfinder';
// View config from source at https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/advanced-setup.html
// View full toolbar at https://ckeditor.com/docs/ckeditor5/latest/api/index.html
// View full demo at https://ckeditor.com/docs/ckeditor5/latest/features
import Heading from '@ckeditor/ckeditor5-heading/src/heading';
import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials';
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold';
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic';
import Underline from '@ckeditor/ckeditor5-basic-styles/src/underline';
import Strikethrough from '@ckeditor/ckeditor5-basic-styles/src/strikethrough';
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph';
import Table from '@ckeditor/ckeditor5-table/src/table';
import TableToolbar from '@ckeditor/ckeditor5-table/src/tabletoolbar';
import TableProperties from '@ckeditor/ckeditor5-table/src/tableproperties';
import TableCellProperties from '@ckeditor/ckeditor5-table/src/tablecellproperties';
import Link from '@ckeditor/ckeditor5-link/src/link';
import List from '@ckeditor/ckeditor5-list/src/list';
import Alignment from '@ckeditor/ckeditor5-alignment/src/alignment';
import MediaEmbed from '@ckeditor/ckeditor5-media-embed/src/mediaembed';
import Highlight from '@ckeditor/ckeditor5-highlight/src/highlight';
import PasteFromOffice from '@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice';
import Indent from '@ckeditor/ckeditor5-indent/src/indent';
import BlockQuote from '@ckeditor/ckeditor5-block-quote/src/blockquote';
import WordCount from '@ckeditor/ckeditor5-word-count/src/wordcount';
import Image from '@ckeditor/ckeditor5-image/src/image';
import ImageStyle from '@ckeditor/ckeditor5-image/src/imagestyle';
import ImageToolbar from '@ckeditor/ckeditor5-image/src/imagetoolbar';
import ImageCaption from '@ckeditor/ckeditor5-image/src/imagecaption';
import ImageResize from '@ckeditor/ckeditor5-image/src/imageresize';
import ImageUpload from '@ckeditor/ckeditor5-image/src/imageupload';
import ImageInsert from '@ckeditor/ckeditor5-image/src/image/imageinsertcommand';
import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
import { isEmpty } from '../../../../../utils';

import { __API_ROOT__ } from 'global.js';

const { TextArea } = Input;

const editorConfiguration = {
  plugins: [
    Essentials,
    Bold,
    Italic,
    Underline,
    Strikethrough,
    Paragraph,
    Heading,
    Table,
    List,
    Link,
    TableToolbar,
    TableProperties,
    TableCellProperties,
    Alignment,
    MediaEmbed,
    Highlight,
    PasteFromOffice,
    Indent,
    BlockQuote,
    WordCount,
    Image,
    ImageStyle,
    ImageToolbar,
    ImageCaption,
    ImageResize,
    CKFinder,
    ImageUpload,
    ImageInsert,
  ],
  toolbar: {
    items: [
      'heading',
      '|',
      'bold',
      'italic',
      'underline',
      'strikethrough',
      'highlight',
      '|',
      'alignment',
      '|',
      'bulletedList',
      'numberedList',
      '|',
      'indent',
      'outdent',
      '|',
      'link',
      'blockquote',
      'insertTable',
      '|',
      'undo',
      'redo',
      'mediaEmbed',
      'imageUpload',
    ],
    shouldNotGroupWhenFull: true,
  },
  ckfinder: {
    uploadUrl: `${__API_ROOT__}news/uploadImage`,
    error: {
      message:
        'The image upload failed because the image was too big (max 1.5MB).',
    },
    // {"fileName":"logo.png","uploaded":1,"url":
    // "https:\/\/ckeditor.com\/apps\/ckfinder\/userfiles\/files\/logo.png"}
  },
  wordCount: {
    onUpdate: (stats) => {
      const { characters, words } = stats;
      const wordsBox = document.querySelector('.article-count');
      wordsBox.textContent = `Ký tự: ${characters}, Từ: ${words}`;
    },
  },
  image: {
    // You need to configure the image toolbar, too, so it uses the new style buttons.
    toolbar: [
      'imageTextAlternative',
      '|',
      'imageStyle:alignLeft',
      'imageStyle:full',
      'imageStyle:alignRight',
    ],
    styles: [
      // This option is equal to a situation where no style is applied.
      'full',
      // This represents an image aligned to the left.
      'alignLeft',
      // This represents an image aligned to the right.
      'alignRight',
    ],
  },
  highlight: {
    options: [
      {
        model: 'yellowMarker',
        class: 'marker-yellow',
        title: 'Yellow marker',
        color: 'var(--ck-highlight-marker-yellow)',
        type: 'marker',
      },
      {
        model: 'greenMarker',
        class: 'marker-green',
        title: 'Green marker',
        color: 'var(--ck-highlight-marker-green)',
        type: 'marker',
      },
    ],
  },
  table: {
    contentToolbar: [
      'tableColumn',
      'tableRow',
      'mergeTableCells',
      'tableProperties',
      'tableCellProperties',
    ],
  },
  placeholder: 'Hãy viết gì đó ...',
};

const NewsEditor = (props) => {
  const {
    newsEditor,
    onTitleChange,
    onSapoChange,
    onBodyChange,
    isDisable,
  } = props;
  const { title, sapo, body } = newsEditor || {};
  return (
    <div className="col overflow-hidden h-100">
      <div className="post-editor-content">
        <PerfectScrollbar>
          <div className="article-form-container">
            <div className="article-form">
              <div className="article-form-heading">
                <TextArea
                  value={title}
                  autoSize
                  className="article-heading"
                  placeholder="Tiêu đề bài viết"
                  onChange={(e) => onTitleChange(e.target.value)}
                  disabled={isDisable}
                />
              </div>
              <div className="article-form-desc">
                <TextArea
                  value={sapo}
                  autoSize
                  className="article-desc"
                  placeholder="Mô tả ..."
                  onChange={(e) => onSapoChange(e.target.value)}
                  disabled={isDisable}
                />
              </div>
              <div className="article-form-body">
                <div className="article-form-social">
                  <div className="social-icon facebook">
                    <i className="fa fa-facebook-f" />
                  </div>
                  <div className="social-icon comment">
                    <i className="fa fa-commenting" />
                  </div>
                  <div className="social-icon envelope">
                    <i className="fa fa-envelope-o" />
                  </div>
                </div>
                <div className="article-form-writter">
                  {isDisable ? (
                    // eslint-disable-next-line react/no-danger
                    <div dangerouslySetInnerHTML={{ __html: body }} />
                  ) : (
                    <CKEditor
                      editor={ClassicEditor}
                      data={body}
                      onInit={(editor) => {
                        let newBody = '';
                        if (!isEmpty(body)) {
                          newBody = body;
                        }
                        editor.setData(newBody);
                      }}
                      onChange={(event, editor) => onBodyChange(event, editor)}
                      config={editorConfiguration}
                    />
                  )}
                </div>
              </div>
            </div>
            <div className="article-count" />
          </div>
        </PerfectScrollbar>
      </div>
    </div>
  );
};
export default NewsEditor;
