import React from 'react';
import { generateButtonAction } from '../../routines';

const NewsAction = React.memo((props) => {
  const {
    isDisable,
    encryptId,
    status,
    onClose,
    onNewsEdit,
    onNewsSave,
    onNewsSaveSend,
    onNewsSaveRelease,
    onNewsSend,
    onNewsRelease,
    onNewsRemove,
    onNewsReturn,
  } = props;
  return (
    <div className="post-editor-header">
      <div className="post-autosave">
        <i className="fa fa-cloud-upload" /> Tự động lưu 15:23 29/02/2020
      </div>
      <div className="post-editor-item flex-grow-1 text-center">
        {generateButtonAction(
          isDisable,
          encryptId,
          status,
          onClose,
          onNewsSave,
          onNewsEdit,
          onNewsSend,
          onNewsRelease,
          onNewsRemove,
          onNewsReturn,
          onNewsSaveSend,
          onNewsSaveRelease,
        )}
      </div>
    </div>
  );
});

export default NewsAction;
