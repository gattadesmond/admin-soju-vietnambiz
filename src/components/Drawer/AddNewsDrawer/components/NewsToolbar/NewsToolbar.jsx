import React from 'react';
import { Tooltip, Divider, Anchor } from 'antd';
import PerfectScrollbar from 'react-perfect-scrollbar';

import {
  InfoCircleTwoTone,
  SettingTwoTone,
  FundTwoTone,
  InteractionTwoTone,
  CalculatorTwoTone,
  ContainerTwoTone,
} from '@ant-design/icons';

import BasicInfo from './components/BasicInfo';
import ReleaseConfig from './components/ReleaseConfig';
import NewsRelationModal from './components/NewsRelationModal';
import { EnumNewsRelationModal } from '../../../../../constants';
import RoyaltyModal from './components/RoyaltyModal';
import { generateZoneOptions } from '../../../../../utils';

const { Link } = Anchor;

const NewsToolbar = React.memo((props) => {
  const {
    basicInfo,
    currentRoyalty,
    onBasicInfoChange,
    onReleaseConfigChange,
    releaseConfig,
    royaltyConfig,
    isDisable,
    optionsNewsType,
    optionsType,
    optionDisplayPosition,
    zones,
    permissionZone,
    newsRelation,
    newsRelationChose,
    newsRelationModal,
    isVisibleRoyaltyModal,
    onRoyaltyModalVisible,
    onNormalAvatarChange,
    onVerticalAvatarChange,
    onSquareAvatarChange,
    onFacebookAvatarChange,
    onBeforeUpload,
    onNewsRelationModalAction,
    onRoyaltyAdd,
    onRoyaltyEdit,
    onRoyaltyDelete,
    onRoyaltySave,
    onRoyaltySubmit,
    onRoyaltyCancel,
    onNewsRelationKeywordChange,
    onNewsRelationZoneChange,
    onNewsRelationSearchClick,
    onNewsRelationItemClick,
    onNewsRelationChoseRemove,
    onNewsRelationChoseReOrder,
    onNewsRelationReset,
    onNewsRelationSubmit,
    onSEOConfigClick,
  } = props;
  const { relationItems, relationSpecialItems } = newsRelationChose || {};
  const { royalties } = royaltyConfig || {};
  return (
    <React.Fragment>
      <div className="col-auto h-100">
        <div className="post-editor-sidebar h-100">
          <PerfectScrollbar>
            <BasicInfo
              basicInfo={basicInfo}
              isDisable={isDisable}
              onBasicInfoChange={onBasicInfoChange}
              optionsNewsType={optionsNewsType}
              optionsType={optionsType}
              onNormalAvatarChange={onNormalAvatarChange}
              onVerticalAvatarChange={onVerticalAvatarChange}
              onSquareAvatarChange={onSquareAvatarChange}
              onFacebookAvatarChange={onFacebookAvatarChange}
              onBeforeUpload={onBeforeUpload}
            />
            <ReleaseConfig
              optionDisplayPosition={optionDisplayPosition}
              releaseConfig={releaseConfig}
              isDisable={isDisable}
              optionsZone={generateZoneOptions(zones, permissionZone)}
              onReleaseConfigChange={onReleaseConfigChange}
              onRoyaltyModalVisible={onRoyaltyModalVisible}
            />
          </PerfectScrollbar>
        </div>
      </div>
      <div className="col-auto  h-100">
        <div className="post-editor-navleft h-100">
          <Anchor affix={false}>
            <nav className="navLeft">
              <Link
                href="#basic-info"
                title={
                  <Tooltip placement="left" title="Thông tin cơ bản">
                    <div className="navLeft-item is-active">
                      <InfoCircleTwoTone style={{ fontSize: '26px' }} />
                    </div>
                  </Tooltip>
                }
              />
              <Link
                href="#release-config"
                title={
                  <Tooltip placement="left" title="Cài đặt phân phối">
                    <div className="navLeft-item">
                      <SettingTwoTone style={{ fontSize: '26px' }} />
                    </div>
                  </Tooltip>
                }
              />
              <Tooltip placement="left" title="Cài đặt SEO">
                <div className="navLeft-item" onClick={onSEOConfigClick}>
                  <FundTwoTone style={{ fontSize: '26px' }} />
                </div>
              </Tooltip>
              <Divider className="my-2" />
              <Tooltip placement="left" title="Chấm nhuận">
                <div
                  onClick={() => onRoyaltyModalVisible(true)}
                  className="navLeft-item"
                >
                  {royalties && royalties.length > 0 && (
                    <div className="navLeft-item-badge bg-success">
                      <i className="fa fa-check" />
                    </div>
                  )}
                  <CalculatorTwoTone style={{ fontSize: '26px' }} />
                </div>
              </Tooltip>
              <Tooltip placement="left" title="Tin tức liên quan">
                <div
                  onClick={() =>
                    onNewsRelationModalAction(
                      true,
                      EnumNewsRelationModal.RELATION.id,
                    )
                  }
                  className="navLeft-item"
                >
                  {relationItems && relationItems.length > 0 && (
                    <div className="navLeft-item-badge bg-danger">
                      {relationItems.length}
                    </div>
                  )}
                  <InteractionTwoTone style={{ fontSize: '26px' }} />
                </div>
              </Tooltip>
              <Tooltip placement="left" title="Tin tức liên quan chân bài">
                <div
                  onClick={() =>
                    onNewsRelationModalAction(
                      true,
                      EnumNewsRelationModal.RELATION_SPECIAL.id,
                    )
                  }
                  className="navLeft-item"
                >
                  {relationSpecialItems && relationSpecialItems.length > 0 && (
                    <div className="navLeft-item-badge bg-danger">
                      {relationSpecialItems.length}
                    </div>
                  )}
                  <ContainerTwoTone style={{ fontSize: '26px' }} />
                </div>
              </Tooltip>
              <NewsRelationModal
                newsRelation={newsRelation}
                isDisable={isDisable}
                newsRelationModal={newsRelationModal}
                optionsZone={generateZoneOptions(zones, permissionZone, {
                  id: -1,
                  name: 'Tất cả chuyên mục',
                })}
                choseItems={
                  newsRelationModal.maxItemChoose ===
                  EnumNewsRelationModal.RELATION.maxItemChoose
                    ? relationItems
                    : relationSpecialItems
                }
                onNewsRelationModalAction={onNewsRelationModalAction}
                onNewsRelationKeywordChange={onNewsRelationKeywordChange}
                onNewsRelationZoneChange={onNewsRelationZoneChange}
                onNewsRelationSearchClick={onNewsRelationSearchClick}
                onNewsRelationItemClick={onNewsRelationItemClick}
                onNewsRelationChoseRemove={onNewsRelationChoseRemove}
                onNewsRelationReset={onNewsRelationReset}
                onNewsRelationSubmit={onNewsRelationSubmit}
                onNewsRelationChoseReOrder={onNewsRelationChoseReOrder}
              />
              <RoyaltyModal
                royaltyConfig={royaltyConfig}
                currentRoyalty={currentRoyalty}
                isVisibleRoyaltyModal={isVisibleRoyaltyModal}
                onRoyaltyAdd={onRoyaltyAdd}
                onRoyaltyEdit={onRoyaltyEdit}
                onRoyaltyDelete={onRoyaltyDelete}
                onRoyaltySubmit={onRoyaltySubmit}
                onRoyaltyCancel={onRoyaltyCancel}
                onRoyaltySave={onRoyaltySave}
                isDisable={isDisable}
              />
            </nav>
          </Anchor>
        </div>
      </div>
    </React.Fragment>
  );
});
export default NewsToolbar;
