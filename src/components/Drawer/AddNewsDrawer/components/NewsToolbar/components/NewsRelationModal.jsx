import React, { Component } from 'react';
import { Button, Input, Modal, Select, Col, Row, List, Spin } from 'antd';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import NewsRelationItem from './NewsRelationItem';
import { numberWithCommas, reorder } from '../../../../../../utils';
import { commonConst } from '../../../../../../constants/constants';
import NewsRelationChoseItem from './NewsRelationChoseItem';
import { EnumNewsRelationModal } from '../../../../../../constants';

const { Search } = Input;

const listData = [];
const listData2 = [];
// eslint-disable-next-line no-plusplus
for (let i = 0; i < 23; i++) {
  if (i < 3) {
    listData2.push({
      id: `${i}`,
      href: 'http://ant.design',
      title: `ant design part ${i}`,
      avatar:
        'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
      description:
        'Ant Design, a design language for background applications, is refined by Ant UED Team.',
      content:
        'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
    });
  }
  listData.push({
    href: 'http://ant.design',
    title: `ant design part ${i}`,
    avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
    description:
      'Ant Design, a design language for background applications, is refined by Ant UED Team.',
    content:
      'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
  });
}

class NewsRelationModal extends Component {
  componentDidUpdate(prevProps, prevState, prevContext) {
    const { newsRelationModal: prevNewsRelationModal } = prevProps;
    const { newsRelationModal, onNewsRelationReset } = this.props;
    const { maxItemChoose } = newsRelationModal || {};
    const { isVisible: prevIsVisible } = prevNewsRelationModal || {};
    const { isVisible, isSubmit } = newsRelationModal || {};
    const type =
      maxItemChoose === EnumNewsRelationModal.RELATION.maxItemChoose
        ? EnumNewsRelationModal.RELATION.id
        : EnumNewsRelationModal.RELATION_SPECIAL.id;
    if (prevIsVisible !== isVisible && !isVisible) {
      onNewsRelationReset(type, isSubmit);
    }
  }

  onDragEnd = (result) => {
    const {
      onNewsRelationChoseReOrder,
      newsRelationModal,
      choseItems,
    } = this.props;
    const { maxItemChoose } = newsRelationModal || {};
    const type =
      maxItemChoose === EnumNewsRelationModal.RELATION.maxItemChoose
        ? EnumNewsRelationModal.RELATION.id
        : EnumNewsRelationModal.RELATION_SPECIAL.id;

    if (!result.destination) {
      return;
    }

    const items = reorder(
      choseItems,
      result.source.index,
      result.destination.index,
    );

    onNewsRelationChoseReOrder(items, type);
  };

  getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',

    // change background colour if dragging
    background: isDragging ? '#ebf5fd' : 'white',
    border: isDragging ? '2px solid rgba(9,30,66,0.71)' : '',
    // styles we need to apply on draggables
    ...draggableStyle,
  });

  getListStyle = (isDraggingOver) => ({
    background: isDraggingOver ? 'rgb(235, 236, 240)' : 'rgb(235, 236, 240)',
  });

  handleOk = () => {
    const { newsRelationModal, onNewsRelationModalAction } = this.props;
    const { maxItemChoose } = newsRelationModal || {};
    const isVisible = false;
    const isSubmit = true;
    const type =
      maxItemChoose === EnumNewsRelationModal.RELATION.maxItemChoose
        ? EnumNewsRelationModal.RELATION.id
        : EnumNewsRelationModal.RELATION_SPECIAL.id;
    onNewsRelationModalAction(isVisible, type, isSubmit);
  };

  render() {
    const {
      newsRelationModal,
      isDisable,
      choseItems,
      optionsZone,
      newsRelation,
      onNewsRelationKeywordChange,
      onNewsRelationZoneChange,
      onNewsRelationSearchClick,
      onNewsRelationModalAction,
      onNewsRelationItemClick,
      onNewsRelationChoseRemove,
    } = this.props;
    const { data, total } = newsRelation || {};
    const { isVisible, title, keyword, zoneId, maxItemChoose } =
      newsRelationModal || {};
    const type =
      maxItemChoose === EnumNewsRelationModal.RELATION.maxItemChoose
        ? EnumNewsRelationModal.RELATION.id
        : EnumNewsRelationModal.RELATION_SPECIAL.id;
    return (
      <Modal
        title={title}
        visible={isVisible}
        onOk={this.handleOk}
        onCancel={() => onNewsRelationModalAction(false, type)}
        width="900px"
        className="modal-postItem"
        destroyOnClose
        centered
      >
        <Row>
          <Col span={15}>
            <div className="news-filter ">
              <div className="d-flex align-items-end  flex-nowrap">
                <div className="filter-form">
                  <div className="filter-item p-0">
                    <div className="font-weight-bold">Từ khoá</div>
                  </div>
                  <div className="filter-item">
                    <Search
                      value={keyword}
                      placeholder="Nhập từ khoá tìm kiếm"
                      disabled={isDisable}
                      onChange={(e) =>
                        onNewsRelationKeywordChange(e.target.value)
                      }
                      style={{ width: 170 }}
                    />
                  </div>

                  <div className="filter-item">
                    <div className="font-weight-bold">Mục</div>
                  </div>

                  <div className="filter-item p-0">
                    <Select
                      value={zoneId}
                      defaultValue="Tất cả chuyên mục"
                      style={{ width: 180 }}
                      onChange={onNewsRelationZoneChange}
                      disabled={isDisable}
                    >
                      {optionsZone}
                    </Select>
                  </div>
                  <div className="filter-item p-0">
                    <Button
                      type="primary"
                      htmlType="submit"
                      disabled={isDisable}
                      onClick={() =>
                        onNewsRelationSearchClick(commonConst.pageIndex)
                      }
                    >
                      Lọc
                    </Button>
                  </div>
                </div>
                <div className="filter-submit" />
              </div>
            </div>
            <List
              itemLayout="vertical"
              pagination={{
                onChange: (page) => {
                  onNewsRelationSearchClick(page);
                },
                position: 'bottom',
                pageSize: 5,
                size: 'small',
                total,
                showTotal: (total) => `Total ${numberWithCommas(total)} items`,
                showQuickJumper: true,
              }}
              dataSource={data}
              className="postItem-relative"
              renderItem={(item) => (
                <NewsRelationItem
                  item={item}
                  maxItemChoose={maxItemChoose}
                  choseItems={choseItems}
                  onNewsRelationItemClick={onNewsRelationItemClick}
                />
              )}
            />
          </Col>
          <Col span={9} style={{ paddingLeft: '15px' }}>
            <div className="card ht-100p">
              <div className="card-header d-flex align-items-center justify-content-between">
                <h6 className="mg-b-0">Tin đã chọn</h6>
              </div>

              {choseItems && choseItems.length > 0 && (
                <DragDropContext onDragEnd={this.onDragEnd}>
                  <Droppable droppableId="droppable">
                    {(provided, snapshot) => (
                      <ul
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        style={this.getListStyle(snapshot.isDraggingOver)}
                        className="postRelative-select"
                      >
                        {choseItems.map((item, index) => (
                          <Draggable
                            key={item.id}
                            draggableId={item.id.toString()}
                            index={index}
                          >
                            {(provided, snapshot) => (
                              <li
                                ref={provided.innerRef}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}
                                style={this.getItemStyle(
                                  snapshot.isDragging,
                                  provided.draggableProps.style,
                                )}
                                className="postRelative-select-item"
                              >
                                <NewsRelationChoseItem
                                  item={item}
                                  maxItemChoose={maxItemChoose}
                                  onNewsRelationChoseRemove={
                                    onNewsRelationChoseRemove
                                  }
                                />
                              </li>
                            )}
                          </Draggable>
                        ))}

                        {provided.placeholder}
                      </ul>
                    )}
                  </Droppable>
                </DragDropContext>
              )}
            </div>
          </Col>
        </Row>
      </Modal>
    );
  }
}
export default NewsRelationModal;
