import React from 'react';
import moment from 'moment';
import { EnumNewsRelationModal } from '../../../../../../constants';
import { __IMAGE_HOST__ , __HOST__} from 'global.js';
const NewsRelationItem = (props) => {
  const { item, choseItems, maxItemChoose, onNewsRelationItemClick } = props;
  const { avatar, title, url, distributionDate } = item || {};
  const avatarSrc = `${__IMAGE_HOST__}${avatar}`;
  const urlLink = `${__HOST__}${url}`;
  const publishDate = moment(distributionDate).format('DD/MM/YYYY - HH:mm');
  const activeItem = choseItems && choseItems.find((i) => i.encryptId === item.encryptId);
  const { encryptId } = activeItem || {};
  const type =
    maxItemChoose === EnumNewsRelationModal.RELATION.maxItemChoose
      ? EnumNewsRelationModal.RELATION.id
      : EnumNewsRelationModal.RELATION_SPECIAL.id;
  return (
    <div
      className={`postItem-relative-item ${
        encryptId === item.encryptId ? 'is-active' : ''
      }`}
      onClick={() => onNewsRelationItemClick(item, type)}
    >
      <div className="postItem-flex">
        <div className="postItem-thumb ">
          <img className="rounded" width={40} alt={title} src={avatarSrc} />
        </div>
        <div className="postItem-heading pl-3 pr-3">
          <div className="mb-1 postItem-relative-name">{title}</div>
          <div className="postItem-relative-link">{urlLink}</div>
          <div className="postItem-relative-date">{publishDate}</div>
        </div>
      </div>
    </div>
  );
};

export default NewsRelationItem;
