import React, { Component } from 'react';
import { UploadOutlined, FormOutlined } from '@ant-design/icons';

import {
  Checkbox,
  DatePicker,
  Input,
  Select,
  Popover,
  Upload,
  Spin,
} from 'antd';
import request from '../../../../../../utils/request';
import { EnumResponseCode } from '../../../../../../constants';

import { __API_ROOT__ } from 'global.js';

const { TextArea } = Input;
const { Option } = Select;

const dateFormat = 'DD/MM/YYYY HH:mm';

class BasicInfo extends Component {
  state = {
    fetching: false,
    tagSource: [],
    expertSource: [],
  };

  fetchTagSource = async (keyword) => {
    if (keyword.length < 3) return;
    this.setState({ tagSource: [] });
    const response = await request.get('tag/suggest', { keyword });
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      const tagSource = reply.map((tag) => {
        const { id, name, newsCount } = tag;
        return {
          text: `${name} (${newsCount})`,
          value: id,
        };
      });
      this.setState({ tagSource });
    }
  };

  fetchExpertSource = async (keyword) => {
    this.setState({ expertSource: [], fetching: true });
    const response = await request.get('expert/suggest', { keyword });
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      const expertSource = reply.map((tag) => {
        const { id, expertName } = tag;
        return {
          text: expertName,
          value: id,
        };
      });
      this.setState({ expertSource, fetching: false });
    }
  };

  handleTagChange = (value) => {
    const { onBasicInfoChange } = this.props;
    this.setState({ tagSource: [], fetching: false }, () => {
      onBasicInfoChange('tag', value);
    });
  };

  handleExpertChange = (value) => {
    const { onBasicInfoChange } = this.props;
    this.setState({ expertSource: [], fetching: false }, () => {
      onBasicInfoChange('expertId', value);
    });
  };

  render() {
    const { fetching, tagSource, expertSource } = this.state;
    const {
      basicInfo,
      isDisable,
      optionsNewsType,
      optionsType,
      onNormalAvatarChange,
      onVerticalAvatarChange,
      onSquareAvatarChange,
      onFacebookAvatarChange,
      onBeforeUpload,
      onBasicInfoChange,
    } = this.props;
    const {
      author,
      type,
      newsType,
      distributionDate,
      note,
      source,
      sourceURL,
      tag,
      expertId,
      isShowAvatar,
      avatarDesc,
      avatarNormal,
      avatarVertical,
      avatarSquare,
      avatarFacebook,
    } = basicInfo || {};

    const UploadComponent = ({ title, url, onUploadChange, isDisable }) => (
      <Popover placement="bottom" title={title}>
        <Upload
          name="avatar"
          listType="picture-card"
          className="avatar-uploader"
          showUploadList={false}
          action={`${__API_ROOT__}news/uploadAvatar`}
          disabled={isDisable}
          beforeUpload={onBeforeUpload}
          onChange={onUploadChange}
        >
          {url ? (
            <img src={url} alt="avatar" style={{ width: '100%' }} />
          ) : (
            <div>
              <UploadOutlined />
            </div>
          )}
        </Upload>
      </Popover>
    );

    return (
      <div className="post-form">
        <div className="post-form-title shadow-sm" id="basic-info">
          Thông tin cơ bản
        </div>
        <div className="post-form-content">
          <div className="post-form-section">
            <div className="mb-2 font-weight-medium">
              Avatar <span className="text-danger">(*)</span>
            </div>
            <div className="row tiny-gutters row-cols-5 mb-3 ">
              <div className="col">
                <UploadComponent
                  title="Avatar thường tỷ lệ 16:10"
                  url={avatarNormal}
                  field="normal"
                  isDisable={isDisable}
                  onUploadChange={onNormalAvatarChange}
                />
              </div>
              <div className="col">
                <UploadComponent
                  title="Avatar dọc tỷ lệ 3:4"
                  url={avatarVertical}
                  field="vertical"
                  isDisable={isDisable}
                  onUploadChange={onVerticalAvatarChange}
                />
              </div>
              <div className="col">
                <UploadComponent
                  title="Avatar vuông tỷ lệ 1:1"
                  url={avatarSquare}
                  field="square"
                  isDisable={isDisable}
                  onUploadChange={onSquareAvatarChange}
                />
              </div>
              <div className="col">
                <UploadComponent
                  title="Avatar Share Facebook 600x315"
                  url={avatarFacebook}
                  field="facebook"
                  isDisable={isDisable}
                  onUploadChange={onFacebookAvatarChange}
                />
              </div>
            </div>
            <Checkbox
              checked={isShowAvatar}
              onChange={(e) => {
                onBasicInfoChange('isShowAvatar', e.target.checked);
              }}
              disabled={isDisable}
            >
              Cho phép hiển thị avatar trang chi tiết
            </Checkbox>
          </div>
          <div className="post-form-section">
            <div className="mb-2 font-weight-medium">Chú thích Avatar</div>
            <TextArea
              rows={2}
              value={avatarDesc}
              onChange={(e) => onBasicInfoChange('avatarDesc', e.target.value)}
              disabled={isDisable}
            />
          </div>
          <div className="post-form-section">
            <div className="mb-2 font-weight-medium">Dạng bài</div>
            <Select
              value={type}
              style={{ width: '100%' }}
              onChange={(value) => onBasicInfoChange('type', value)}
              disabled={isDisable}
            >
              {optionsType}
            </Select>
          </div>
          <div className="post-form-section">
            <div className="mb-2 font-weight-medium">Hiển thị icon</div>
            <Select
              value={newsType}
              style={{ width: '100%' }}
              onChange={(value) => onBasicInfoChange('newsType', value)}
              disabled={isDisable}
            >
              {optionsNewsType}
            </Select>
          </div>
          <div className="post-form-section">
            <div className="mb-2 font-weight-medium">Tác giả</div>
            <Input
              value={author}
              onChange={(e) => onBasicInfoChange('author', e.target.value)}
              disabled={isDisable}
            />
          </div>
          <div className="post-form-section">
            <div className="mb-2 font-weight-medium">Ngày xuất bản</div>
            <DatePicker
              value={distributionDate}
              showTime={{ format: 'HH:mm' }}
              format={dateFormat}
              onChange={(value) => onBasicInfoChange('distributionDate', value)}
              disabled={isDisable}
            />
          </div>
          <div className="post-form-section">
            <div className="mb-2 font-weight-medium">
              <FormOutlined />
              Ghi chú
            </div>
            <TextArea
              value={note}
              rows={2}
              onChange={(e) => onBasicInfoChange('note', e.target.value)}
              disabled={isDisable}
            />
          </div>
          <div className="post-form-section">
            <div className="mb-2 font-weight-medium">Nguồn tin</div>
            <Input
              value={source}
              onChange={(e) => onBasicInfoChange('source', e.target.value)}
              disabled={isDisable}
            />
          </div>
          <div className="post-form-section">
            <div className="mb-2 font-weight-medium">Link bài gốc</div>
            <Input
              value={sourceURL}
              onChange={(e) => onBasicInfoChange('sourceURL', e.target.value)}
              disabled={isDisable}
            />
          </div>
          <div className="post-form-section">
            <div className="mb-2 font-weight-medium">Chuyên gia</div>
            <Select
              showSearch
              showArrow={false}
              value={expertId}
              style={{ width: '100%' }}
              disabled={isDisable}
              notFoundContent={fetching ? <Spin size="small" /> : null}
              onSearch={this.fetchExpertSource}
              onChange={this.handleExpertChange}
              filterOption={false}
              labelInValue
            >
              {expertSource.map((d) => (
                <Option title={d.text} key={d.value}>
                  {d.text}
                </Option>
              ))}
            </Select>
          </div>
          <div className="post-form-section">
            <div className="mb-2 font-weight-medium">
              Tags{' '}
              <span style={{ fontSize: '10px' }}>
                (Nhập ít nhất 3 ký tự để tìm kiếm)
              </span>
            </div>
            <Select
              mode="tags"
              value={tag}
              labelInValue
              style={{ width: '100%' }}
              onSearch={this.fetchTagSource}
              onChange={this.handleTagChange}
              disabled={isDisable}
              filterOption={false}
            >
              {tagSource.map((d) => (
                <Option title={d.text} key={d.value}>
                  {d.text}
                </Option>
              ))}
            </Select>
          </div>
        </div>
      </div>
    );
  }
}

export default BasicInfo;
