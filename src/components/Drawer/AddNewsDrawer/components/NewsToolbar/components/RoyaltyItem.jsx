import React from 'react';

import EditOutlined from '@ant-design/icons/lib/icons/EditOutlined';
import DeleteOutlined from '@ant-design/icons/lib/icons/DeleteOutlined';

const RoyaltyItem = ({
  item,
  index,
  onRoyaltyEdit,
  onRoyaltyDelete,
  royaltyReducer,
}) => {
  const { royaltyFor, categoryId } = item;
  const { coefAccounts, coefTypes } = royaltyReducer || {};
  const currentAccount = coefAccounts.find((x) => x.id === royaltyFor);
  const { fullName } = currentAccount || {};
  const currentType = coefTypes.find((x) => x.id === categoryId);
  const { name } = currentType || {};
  return (
    <div style={{ marginBottom: '5px', marginTop: '30px' }}>
      <span>{`${fullName} - ${name}`}</span>
      <EditOutlined
        onClick={() => onRoyaltyEdit(index)}
        style={{ marginLeft: '10px', cursor: 'pointer' }}
      />
      <DeleteOutlined
        onClick={() => onRoyaltyDelete(index)}
        style={{ marginLeft: '5px', cursor: 'pointer' }}
      />
    </div>
  );
};

export default RoyaltyItem;
