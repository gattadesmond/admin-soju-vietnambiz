import React from 'react';
import { EnumNewsRelationModal } from '../../../../../../constants';

import { __IMAGE_HOST__ } from 'global.js';
const NewsRelationChoseItem = (props) => {
  const { item, onNewsRelationChoseRemove, maxItemChoose } = props;
  const { avatar, title, encryptId } = item || {};
  const avatarSrc = `${__IMAGE_HOST__}${avatar}`;
  const type =
    maxItemChoose === EnumNewsRelationModal.RELATION.maxItemChoose
      ? EnumNewsRelationModal.RELATION.id
      : EnumNewsRelationModal.RELATION_SPECIAL.id;
  return (
    <React.Fragment>
      <div className="postRelative-avatar">
        <img src={avatarSrc} alt={title} width="34" className="rounded" />
      </div>
      <div className="postRelative-name">{title}</div>
      <div className="postRelative-remove">
        <span
          onClick={() => onNewsRelationChoseRemove(encryptId, type)}
          className="badge badge-danger"
        >
          Xoá
        </span>
      </div>
    </React.Fragment>
  );
};

export default NewsRelationChoseItem;
