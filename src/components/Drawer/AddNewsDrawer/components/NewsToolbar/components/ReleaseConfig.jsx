import React, { Component } from 'react';
import { Select, Spin } from 'antd';
import request from '../../../../../../utils/request';
import { EnumResponseCode } from '../../../../../../constants';

const { Option } = Select;

class ReleaseConfig extends Component {
  state = {
    fetching: false,
    topicSource: [],
  };

  fetchTopicSource = async (keyword) => {
    this.setState({ topicSource: [] });
    const response = await request.get('topic/suggest', { keyword });
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      const topicSource = reply.map((topic) => {
        const { id, topicName } = topic;
        return {
          text: topicName,
          value: id,
        };
      });
      this.setState({ topicSource });
    }
  };

  handleTopicChange = (value) => {
    const { onReleaseConfigChange } = this.props;
    this.setState({ topicSource: [], fetching: false }, () => {
      onReleaseConfigChange('topicId', value);
    });
  };

  render() {
    const { topicSource, fetching } = this.state;
    const {
      optionsZone,
      releaseConfig,
      isDisable,
      optionDisplayPosition,
      onReleaseConfigChange,
    } = this.props;
    const { zoneId, zoneIds, topicId, displayPosition } = releaseConfig || {};
    return (
      <div className="post-form" id="release-config">
        <div className="post-form-title shadow-sm">Cài đặt phân phối</div>
        <div className="post-form-content">
          <div className="post-form-section">
            <div className="mb-2 font-weight-bold">
              Chuyên mục chính <span className="text-danger">(*)</span>
            </div>
            <Select
              value={zoneId}
              style={{ width: '100%' }}
              placeholder="Chọn chuyên mục chính"
              onChange={(value) => onReleaseConfigChange('zoneId', value)}
              disabled={isDisable}
            >
              {optionsZone}
            </Select>
          </div>

          <div className="post-form-section">
            <div className="mb-2 font-weight-medium">Chuyên mục liên quan</div>
            <Select
              mode="multiple"
              value={zoneIds}
              style={{ width: '100%' }}
              placeholder="Chọn chuyên mục liên quan"
              onChange={(values) => onReleaseConfigChange('zoneIds', values)}
              disabled={isDisable}
            >
              {optionsZone}
            </Select>
          </div>

          <div className="post-form-section">
            <div className="mb-2 font-weight-medium">Chủ đề</div>
            <Select
              showSearch
              showArrow={false}
              value={topicId}
              style={{ width: '100%' }}
              disabled={isDisable}
              notFoundContent={fetching ? <Spin size="small" /> : null}
              onSearch={this.fetchTopicSource}
              onChange={this.handleTopicChange}
              filterOption={false}
              labelInValue
            >
              {topicSource.map((d) => (
                <Option title={d.text} key={d.value}>
                  {d.text}
                </Option>
              ))}
            </Select>
          </div>
          <div className="post-form-section">
            <div className="mb-2 font-weight-medium">Đề xuất hiển thị</div>
            <Select
              value={displayPosition}
              style={{ width: '100%' }}
              onChange={(values) => onReleaseConfigChange('displayPosition', values)}
              disabled={isDisable}
            >
              {optionDisplayPosition}
            </Select>
          </div>
        </div>
      </div>
    );
  }
}

export default ReleaseConfig;
