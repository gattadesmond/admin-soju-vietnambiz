import React, { Component } from 'react';
import { connect } from 'react-redux';
import isEqual from 'react-fast-compare';
import {
  Input,
  Modal,
  Form,
  Select,
  InputNumber,
  Button,
  Row,
  Col,
} from 'antd';

import { generationCoefficientOptions } from '../../../routines';
import { generateAntOptionsFromArray, isEmpty } from '../../../../../../utils';
import RoyaltyItem from './RoyaltyItem';

const { TextArea } = Input;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

let coefTypeOptions = [];
let coefficientQualityOptions = [];
let coefficientDedicationOptions = [];
let coefficientRiskOptions = [];
let coefficientContributionOptions = [];
let coefAccountOptions = [];

class RoyaltyModal extends Component {
  constructor(props) {
    super(props);
    const { royaltyReducer } = props;
    const { coefTypes, coefAccounts, coefRoyalties } = royaltyReducer;
    const {
      qualityFrom,
      qualityTo,
      dedicationFrom,
      dedicationTo,
      riskFrom,
      riskTo,
      contributionFrom,
      contributionTo,
    } = coefRoyalties || {};
    coefTypeOptions = generateAntOptionsFromArray(
      coefTypes,
      false,
      'id',
      'name',
    );
    coefAccountOptions = generateAntOptionsFromArray(
      coefAccounts,
      false,
      'id',
      'fullName',
    );
    coefficientQualityOptions = generationCoefficientOptions(
      qualityFrom,
      qualityTo,
    );
    coefficientDedicationOptions = generationCoefficientOptions(
      dedicationFrom,
      dedicationTo,
    );
    coefficientRiskOptions = generationCoefficientOptions(riskFrom, riskTo);
    coefficientContributionOptions = generationCoefficientOptions(
      contributionFrom,
      contributionTo,
    );
    this.state = {
      confirmModal: {
        isVisible: false,
        id: '',
        name: '',
        description: '',
      },
    };
  }

  componentDidUpdate(prevProps, prevState, prevContext) {
    const { currentRoyalty: prevCurrentRoyalty } = prevProps;
    const { currentRoyalty } = this.props;
    if (!isEqual(prevCurrentRoyalty, currentRoyalty)) {
      const {
        categoryId,
        photoCount,
        videoCount,
        qualityValue,
        dedicationValue,
        riskValue,
        contributionValue,
        royaltyFor,
        note,
      } = currentRoyalty || {};
      const form = this.formRef.current;
      if (!isEmpty(form)) {
        form.setFieldsValue({
          ...(!isEmpty(categoryId) && { categoryId }),
          ...(!isEmpty(photoCount) && { photoCount }),
          ...(!isEmpty(videoCount) && { videoCount }),
          ...(!isEmpty(qualityValue) && { qualityValue }),
          ...(!isEmpty(dedicationValue) && { dedicationValue }),
          ...(!isEmpty(riskValue) && { riskValue }),
          ...(!isEmpty(contributionValue) && { contributionValue }),
          ...(!isEmpty(royaltyFor) && { royaltyFor }),
          ...(!isEmpty(note) && { note }),
        });
      }
    }
  }

  formRef = React.createRef();

  handleFormSubmit = () => {
    const { onRoyaltySave } = this.props;
    const form = this.formRef.current;
    form.validateFields().then((values) => {
      onRoyaltySave(values);
      form.resetFields();
    });
  };

  handleCategoryChange = (value) => {
    const { royaltyReducer } = this.props;
    const { coefTypes } = royaltyReducer;
    const currentType = coefTypes.find((item) => item.id === value);
    const { name, description } = currentType || {};
    const { confirmModal } = this.state;
    const cloneConfirmModal = { ...confirmModal };
    cloneConfirmModal.id = value;
    cloneConfirmModal.isVisible = true;
    cloneConfirmModal.name = name;
    cloneConfirmModal.description = description;
    this.setState({ confirmModal: cloneConfirmModal });
  };

  handleFormReset = () => {
    const form = this.formRef.current;
    form.resetFields();
  };

  handleModalOk = () => {
    const { confirmModal } = this.state;
    const cloneConfirmModal = { ...confirmModal };
    cloneConfirmModal.isVisible = false;
    this.setState({ confirmModal: cloneConfirmModal });
  };

  handleModalCancel = () => {
    const form = this.formRef.current;
    form.resetFields();
    this.handleModalOk();
  };

  render() {
    const { confirmModal } = this.state;
    const {
      isVisibleRoyaltyModal,
      royaltyConfig,
      isDisable,
      currentRoyalty,
      royaltyReducer,
      onRoyaltyDelete,
      onRoyaltyEdit,
      onRoyaltyAdd,
      onRoyaltyCancel,
      onRoyaltySubmit,
    } = this.props;
    const { royalties } = royaltyConfig || {};
    const { disableCategory } = currentRoyalty || {};
    const { isVisible, name, description } = confirmModal;
    let isDisableCategory = false;
    if (royalties && royalties.length > 0) {
      if (royalties && royalties.length === 1 && !disableCategory) {
        isDisableCategory = false;
      } else {
        isDisableCategory = true;
      }
    }

    return (
      <React.Fragment>
        <Modal
          forceRender
          destroyOnClose
          visible={isVisibleRoyaltyModal}
          title="Chấm nhuận"
          onCancel={onRoyaltyCancel}
          footer={[
            <Button key="back" onClick={onRoyaltyCancel}>
              Hủy
            </Button>,
            <Button
              key="submit"
              type="primary"
              onClick={onRoyaltySubmit}
              disabled={isDisable}
            >
              Hoàn tất
            </Button>,
          ]}
          width="75vw"
        >
          <Row>
            <Col span={6}>
              <Button type="primary" onClick={onRoyaltyAdd}>
                Chấm Nhuận Mới
              </Button>
              {royalties &&
                royalties.length > 0 &&
                royalties.map((item, index) => (
                  <RoyaltyItem
                    // eslint-disable-next-line
                    key={index}
                    index={index}
                    item={item}
                    royaltyReducer={royaltyReducer}
                    onRoyaltyEdit={onRoyaltyEdit}
                    onRoyaltyDelete={onRoyaltyDelete}
                  />
                ))}
            </Col>
            <Col span={18}>
              <Form
                {...layout}
                ref={this.formRef}
                size="middle"
                initialValues={currentRoyalty}
              >
                <Form.Item
                  label="Thể loại"
                  name="categoryId"
                  rules={[
                    { required: true, message: 'Vui lòng chọn thể loại' },
                  ]}
                >
                  <Select
                    showSearch
                    optionFilterProp="children"
                    onChange={this.handleCategoryChange}
                    disabled={isDisable || isDisableCategory}
                  >
                    {coefTypeOptions}
                  </Select>
                </Form.Item>
                <Form.Item
                  label="Số lượng ảnh"
                  name="photoCount"
                  rules={[
                    { required: true, message: 'Vui lòng nhập số lượng ảnh' },
                  ]}
                >
                  <InputNumber min={0} max={999} disabled={isDisable} />
                </Form.Item>
                <Form.Item
                  label="Số lượng video"
                  name="videoCount"
                  rules={[
                    { required: true, message: 'Vui lòng nhập số lượng video' },
                  ]}
                >
                  <InputNumber min={0} max={999} disabled={isDisable} />
                </Form.Item>
                <Form.Item
                  label="Hệ số chất lượng"
                  name="qualityValue"
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng nhập Hệ số chất lượng',
                    },
                  ]}
                >
                  <Select disabled={isDisable}>
                    {coefficientQualityOptions}
                  </Select>
                </Form.Item>
                <Form.Item
                  label="Hệ số cống hiến"
                  name="dedicationValue"
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng nhập Hệ số cống hiến',
                    },
                  ]}
                >
                  <Select disabled={isDisable}>
                    {coefficientDedicationOptions}
                  </Select>
                </Form.Item>
                <Form.Item
                  label="Hệ số rủi ro"
                  name="riskValue"
                  rules={[
                    { required: true, message: 'Vui lòng nhập Hệ số rủi ro' },
                  ]}
                >
                  <Select disabled={isDisable}>{coefficientRiskOptions}</Select>
                </Form.Item>
                <Form.Item
                  label="Hệ số đóng góp"
                  name="contributionValue"
                  rules={[
                    { required: true, message: 'Vui lòng nhập Hệ số đóng góp' },
                  ]}
                >
                  <Select disabled={isDisable}>
                    {coefficientContributionOptions}
                  </Select>
                </Form.Item>
                <Form.Item
                  label="Chấm nhuận cho"
                  name="royaltyFor"
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng chọn Người được chấm nhuận',
                    },
                  ]}
                >
                  <Select
                    showSearch
                    optionFilterProp="children"
                    disabled={isDisable}
                  >
                    {coefAccountOptions}
                  </Select>
                </Form.Item>
                <Form.Item label="Ghi chú" name="note">
                  <TextArea rows={4} disabled={isDisable} />
                </Form.Item>
                <Form.Item {...tailLayout}>
                  <Button onClick={this.handleFormReset}>Reset</Button>
                  <Button
                    key="submit"
                    type="primary"
                    onClick={this.handleFormSubmit}
                    disabled={isDisable}
                  >
                    Lưu
                  </Button>
                </Form.Item>
              </Form>
            </Col>
          </Row>
        </Modal>
        <Modal
          title="Bạn có chắc chắn muốn chọn thể loại này không?"
          visible={isVisible}
          onOk={this.handleModalOk}
          onCancel={this.handleModalCancel}
          okText="Đồng Ý"
          cancelText="Hủy"
          centered
          destroyOnClose
        >
          <span style={{ fontSize: '14px', fontWeight: 'bold' }}>{name}</span>
          <br />
          <span style={{ fontSize: '12px' }}>{description}</span>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  royaltyReducer: state.royalty,
});
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(RoyaltyModal);
