import React from 'react';
import { Button, Select } from 'antd';
import data from './data';

import {
  generateAntOptionsFromArray,
  increment,
  isEmpty,
  removeParamsForServer,
} from '../../../utils';
import { EnumNewsSidebar } from '../../../constants/enum';

const { Option } = Select;

export function onFieldChange(editingNews, objectName, name, value) {
  const cloneEditingNews = { ...editingNews };
  const object = cloneEditingNews[objectName] || {};
  object[name] = value;
  cloneEditingNews[objectName] = removeParamsForServer(object);
  return cloneEditingNews;
}

export function generationNewsTypeOptions() {
  const { newsType } = data;
  return generateAntOptionsFromArray(newsType, false, 'Id', 'Name');
}

export function uploadAvatar(field, file, editingNews) {
  const cloneEditingNews = { ...editingNews };
  cloneEditingNews.basicInfo = cloneEditingNews.basicInfo || {};
  const { basicInfo } = cloneEditingNews || {};
  basicInfo[field] = file.response.file.url;
  return cloneEditingNews;
}

export function generationCoefficientOptions(from, to) {
  const options = [];
  // eslint-disable-next-line
  for (let limit = from; limit <= to; ) {
    options.push(
      <Option key={limit} value={limit}>
        {limit}
      </Option>,
    );
    limit = increment(limit, 0.1);
  }
  return options;
}

export function generationRoyaltyForOptions() {
  const { users } = data;
  return generateAntOptionsFromArray(users, false, 'Id', 'Name');
}

export function validateBeforeSubmit(editingNews) {
  const errors = [];
  const { newsEditor, basicInfo, releaseConfig } = editingNews || {};
  const { title, sapo, body } = newsEditor || {};
  const { avatarNormal } = basicInfo || {};
  const { zoneId } = releaseConfig || {};
  if (isEmpty(title)) {
    errors.push('Title không được để trống');
  }
  if (isEmpty(sapo)) {
    errors.push('Sapo không được để trống');
  }
  if (isEmpty(body)) {
    errors.push('Body không được để trống');
  }
  if (isEmpty(avatarNormal)) {
    errors.push('Avatar không được để trống');
  }
  if (isEmpty(zoneId)) {
    errors.push('Chuyên mục chính không được để trống');
  }
  return errors;
}

export function generateButtonAction(
  isDisable,
  encryptId,
  status,
  onClose,
  onNewsSave,
  onNewsEdit,
  onNewsSend,
  onNewsRelease,
  onNewsRemove,
  onNewsReturn,
  onNewsSaveSend,
  onNewsSaveRelease,
) {
  const buttons = [];
  buttons.push({
    className: 'font-weight-bold text-uppercase mr-3',
    onClick: onClose,
    iClassName: 'fa fa-ban mr-2',
    title: 'Đóng',
  });
  if (isDisable) {
    if (status !== EnumNewsSidebar.REMOVED_POST.id) {
      buttons.push({
        className: 'font-weight-bold text-uppercase mr-3',
        onClick: onNewsEdit,
        iClassName: 'fa fa-pencil mr-2',
        title: 'Sửa',
      });
    }
    buttons.push({
      className: 'font-weight-bold text-uppercase mr-3',
      onClick: onNewsSend,
      iClassName: 'fa fa-paper-plane-o mr-2',
      title: 'Gửi đi',
    });
    buttons.push({
      className: 'font-weight-bold text-uppercase mr-3',
      onClick: onNewsRelease,
      iClassName: 'fa fa-newspaper-o mr-2',
      title: 'Xuất bản',
    });
    buttons.push({
      className: 'font-weight-bold text-uppercase mr-3',
      onClick: onNewsRemove,
      iClassName: 'fa fa-trash-o mr-2',
      title: 'Xóa',
    });
  } else {
    if (
      [
        EnumNewsSidebar.TEMP_POST.id,
        EnumNewsSidebar.WAITING_EDITING_POST.id,
        EnumNewsSidebar.RECEIVED_EDITING_POST.id,
        EnumNewsSidebar.WAITING_RELEASE_POST.id,
        EnumNewsSidebar.RECEIVED_RELEASE_POST.id,
        EnumNewsSidebar.TAKEN_DOWN_POST.id,
      ].includes(status)
    ) {
      buttons.push({
        type: 'primary',
        className: 'font-weight-bold text-uppercase mr-3',
        onClick: onNewsRemove,
        iClassName: 'fa fa-trash-o mr-2',
        title: 'Xóa',
      });
    }

    if (
      [
        EnumNewsSidebar.WAITING_EDITING_POST.id,
        EnumNewsSidebar.RECEIVED_EDITING_POST.id,
        EnumNewsSidebar.WAITING_RELEASE_POST.id,
        EnumNewsSidebar.RECEIVED_RELEASE_POST.id,
        EnumNewsSidebar.TAKEN_DOWN_POST.id,
      ].includes(status)
    ) {
      buttons.push({
        type: 'primary',
        className: 'font-weight-bold text-uppercase mr-3',
        onClick: onNewsReturn,
        iClassName: 'fa fa-reply mr-2',
        title: 'Trả lại',
      });
    }

    if (
      [
        EnumNewsSidebar.WAITING_EDITING_POST.id,
        EnumNewsSidebar.RECEIVED_EDITING_POST.id,
        EnumNewsSidebar.WAITING_RELEASE_POST.id,
        EnumNewsSidebar.RECEIVED_RELEASE_POST.id,
        EnumNewsSidebar.TAKEN_DOWN_POST.id,
      ].includes(status)
    ) {
      buttons.push({
        type: 'primary',
        className: 'font-weight-bold text-uppercase mr-3',
        onClick: onNewsSave,
        iClassName: 'fa fa-unlock mr-2',
        title: 'Nhả ra',
      });
    }

    if (
      [
        EnumNewsSidebar.TEMP_POST.id,
        EnumNewsSidebar.WAITING_EDITING_POST.id,
        EnumNewsSidebar.RECEIVED_EDITING_POST.id,
      ].includes(status)
    ) {
      buttons.push({
        type: 'primary',
        className: 'font-weight-bold text-uppercase mr-3',
        onClick: onNewsSaveSend,
        iClassName: 'fa fa-save mr-2',
        title: 'Lưu và gửi lên',
      });
    }

    if (
      encryptId === 0 ||
      [
        EnumNewsSidebar.TEMP_POST.id,
        EnumNewsSidebar.WAITING_EDITING_POST.id,
        EnumNewsSidebar.RECEIVED_EDITING_POST.id,
        EnumNewsSidebar.WAITING_RELEASE_POST.id,
        EnumNewsSidebar.RECEIVED_RELEASE_POST.id,
        EnumNewsSidebar.TAKEN_DOWN_POST.id,
      ].includes(status)
    ) {
      buttons.push({
        type: 'primary',
        className: 'font-weight-bold text-uppercase mr-3',
        onClick: onNewsSaveRelease,
        iClassName: 'fa fa-save mr-2',
        title: 'Lưu và xuất bản',
      });
    }

    if ([EnumNewsSidebar.RELEASE_POST.id].includes(status)) {
      buttons.push({
        type: 'primary',
        className: 'font-weight-bold text-uppercase mr-3',
        onClick: onNewsSave,
        iClassName: 'fa fa-save mr-2',
        title: 'Gỡ bài',
      });
    }

    buttons.push({
      type: 'primary',
      className: 'font-weight-bold text-uppercase',
      onClick: onNewsSave,
      iClassName: 'fa fa-save mr-2',
      title: 'Lưu bài viết',
    });
  }

  return (
    <React.Fragment>
      {buttons.map((item, index) => {
        const { type, className, onClick, iClassName, title } = item || {};
        return (
          <Button
            // eslint-disable-next-line
            key={index}
            type={type}
            className={className}
            onClick={onClick}
          >
            <i className={iClassName} /> {title}
          </Button>
        );
      })}
    </React.Fragment>
  );
}
