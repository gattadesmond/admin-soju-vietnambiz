import axios from 'axios';
import qs from 'qs';
import { saveAs } from 'file-saver';
import store from '../redux/store';
import { clearLocalStorage } from '../actions/auth.action';
import { isEmpty } from './utils';

import { __API_ROOT__ } from 'global.js';

class Request {
  static header(contentType) {
    const userReducer = store.getState().auth.account;
    if (!isEmpty(userReducer)) {
      const accessToken = store.getState().auth.account.token;
      return {
        'content-type': contentType,
        charset: 'utf-8',
        authorization: `Bearer ${accessToken}`,
      };
    }
    return {
      'content-type': contentType,
    };
  }

  static post(
    endpoint,
    params = {},
    contentType = 'application/json',
    isBlob = false,
  ) {
    if (contentType === 'application/x-www-form-urlencoded') {
      params = qs.stringify(params);
    }
    return axios
      .post(__API_ROOT__ + endpoint, params, {
        headers: Request.header(contentType),
        ...(isBlob && { responseType: 'blob' }),
        withCredentials: true,
        xsrfCookieName: 'csrftoken',
        xsrfHeaderName: 'x-csrftoken',
      })
      .then((response) => {
        // handle success
        const contentType = response.headers['content-type'];
        if (
          contentType ===
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ) {
          const filename = response.headers['content-disposition']
            .split(';')
            .find((n) => n.includes('filename='))
            .replace('filename=', '')
            .trim();
          const url = window.URL.createObjectURL(new Blob([response.data]));
          saveAs(url, filename);
        }
        return response;
      })
      .catch((error) => {
        if (error.response.status === 401) {
          store.dispatch(clearLocalStorage());
        } else if (error.response.status === 500) {
          return error.response;
        } else if (
          error.response.status === 400 &&
          error.response.data &&
          error.response.data.length
        ) {
          const errorData = error.response.data[0];
          return {
            ...error.response,
            data: {
              error_code: 400,
              detail: Object.keys(errorData)
                .map((key) => `${key}: ${errorData[key]}`)
                .join('/n'),
            },
          };
        }
        throw error;
      });
  }

  static put(endpoint, params = {}, contentType = 'application/json') {
    if (contentType === 'application/x-www-form-urlencoded') {
      params = qs.stringify(params);
    }
    return axios
      .put(__API_ROOT__ + endpoint, params, {
        headers: Request.header(contentType),
        withCredentials: true,
        xsrfCookieName: 'csrftoken',
        xsrfHeaderName: 'x-csrftoken',
      })
      .catch((error) => {
        if (error.response.status === 401) {
          store.dispatch(clearLocalStorage());
        } else if (error.response.status === 500) {
          return error.response;
        }
        throw error;
      });
  }

  static delete(endpoint, params = {}, data, contentType = 'application/json') {
    if (contentType === 'application/x-www-form-urlencoded') {
      params = qs.stringify(params);
    }
    return axios
      .delete(__API_ROOT__ + endpoint, {
        params,
        data,
        headers: Request.header(contentType),
        withCredentials: true,
        xsrfCookieName: 'csrftoken',
        xsrfHeaderName: 'x-csrftoken',
      })
      .catch((error) => {
        if (error.response.status === 401) {
          store.dispatch(clearLocalStorage());
        } else if (error.response.status === 500) {
          return error.response;
        }
        throw error;
      });
  }

  static postText(endpoint, params = {}, contentType = 'application/json') {
    if (contentType === 'application/x-www-form-urlencoded') {
      params = qs.stringify(params);
    }
    return axios
      .post(__API_ROOT__ + endpoint, params, {
        headers: Request.header(contentType),
        withCredentials: true,
        xsrfCookieName: 'csrftoken',
        xsrfHeaderName: 'x-csrftoken',
        transformResponse: undefined,
      })
      .catch((error) => {
        if (error.response.status === 401) {
          store.dispatch(clearLocalStorage());
        } else if (error.response.status === 500) {
          return error.response;
        }
        throw error;
      });
  }

  static get(endpoint, params = {}, contentType = 'application/json') {
    if (contentType === 'application/x-www-form-urlencoded') {
      params = qs.stringify(params);
    }
    return axios
      .get(__API_ROOT__ + endpoint, {
        params,
        headers: Request.header(contentType),
        withCredentials: true,
        xsrfCookieName: 'csrftoken',
        xsrfHeaderName: 'x-csrftoken',
        paramsSerializer: (value) => qs.stringify(value),
      })
      .catch((error) => {
        console.log('error', error);
        if (error.response.status === 401) {
          store.dispatch(clearLocalStorage());
        } else if (error.response.status === 500) {
          return error.response;
        }
        throw error;
      });
  }
}

export default Request;
