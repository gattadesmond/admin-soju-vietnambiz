const __API_ROOT__ =  "http://api.vietnewscorp.vn/";
const __IMAGE_HOST__ = "https://cdn.vietnambiz.vn/zoom/60_60/";
const __FROM_DATE_DELTA__ = 25;
const __FROM_MONTH_DELTA__ = -1;
const __CURRENCY_SYMBOL__ = "VND";
const __HOST__ =  "https://vietnambiz.vn/";
export {
    __API_ROOT__,
    __IMAGE_HOST__,
    __FROM_DATE_DELTA__,
    __FROM_MONTH_DELTA__,
    __CURRENCY_SYMBOL__,
    __HOST__
};