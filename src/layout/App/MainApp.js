import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Topbar from '../Topbar';

import AddNewsDrawer from '../../components/Drawer/AddNewsDrawer/AddNewsDrawer';
import { closeAddNewsDrawer } from '../../actions/news.action';
// import CompareVersionDrawer from '../../components/Drawer/CompareVersionDrawer/CompareVersionDrawer';
import Routes from '../../routes/Routes';

export class MainApp extends Component {
  render() {
    const { newsReducer, authReducer, globalReducer } = this.props;
    const { isAppInitDone } = globalReducer || {};
    const { addNewsDrawerOpen, compareVersionOpen, isDisable } =
      newsReducer || {};
    const { permission } = authReducer || {};

    if (isAppInitDone) {
      return (
        <React.Fragment>
          <Topbar />
          <Routes permissionUrl={permission.map((x) => x.url)} />
          {/* Global Modal show here */}
          <AddNewsDrawer
            title="Viết bài mới"
            visible={addNewsDrawerOpen}
            isDisable={isDisable}
          />
          {/* <CompareVersionDrawer isVisible={compareVersionOpen} /> */}

        </React.Fragment>
      );
    }
    return null;
  }
}

const mapStateToProps = (state) => ({
  newsReducer: state.news,
  authReducer: state.auth,
  globalReducer: state.global,
});

const mapDispatchToProps = {
  closeAddNewsDrawerConnect: closeAddNewsDrawer,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(MainApp),
);
