import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { ConfigProvider } from 'antd';
import { IntlProvider } from 'react-intl';
import { withRouter } from 'react-router';

import AppLocale from '../../lngProvider';
import MainApp from './MainApp';

import Login from '../../pages/Login/Login';
import { setUrlRefer } from '../../actions/auth.action';
import RestrictedRoute from '../../components/RestrictedRoute/RestrictedRoute';
import LoadingOverlay from '../../components/LoadingOverlay/LoadingOverlay';

class App extends Component {
  componentDidUpdate(prevProps, prevState, prevContext) {
    const { authenticated: prevAuthenticated } = prevProps;
    const { authenticated: thisAuthenticated, history } = this.props;
    if (prevAuthenticated !== thisAuthenticated && !thisAuthenticated) {
      history.push('/login');
    }
  }

  render() {
    const { globalReducer, authenticated } = this.props;
    const { locale, isLoading, loadingTitle } = globalReducer || {};

    const currentAppLocale = AppLocale[locale.locale];
    return (
      <React.Fragment>
        <ConfigProvider locale={currentAppLocale.antd}>
          <IntlProvider
            locale={currentAppLocale.locale}
            messages={currentAppLocale.messages}
          >
            <Switch>
              <Route exact path="/login" component={Login} />
              <RestrictedRoute component={MainApp} authUser={authenticated} />
            </Switch>
          </IntlProvider>
        </ConfigProvider>
        <LoadingOverlay isLoading={isLoading} title={loadingTitle} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  authenticated: state.auth.authenticated,
  globalReducer: state.global,
});

const mapDispatchToProps = {
  setUrlReferConnect: setUrlRefer,
};
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
