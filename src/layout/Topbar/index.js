import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import {
  FormOutlined,
  PaperClipOutlined,
  DatabaseOutlined,
  MoneyCollectOutlined,
  BarChartOutlined,
  SettingOutlined,
} from '@ant-design/icons';

import { showAddNewsDrawer } from '../../actions/news.action';

import './Topbar.scss';
import { EnumTopMenu } from '../../constants';
import imgLogo from '../../assets/static/vietnambiz-logo.png';

import NavbarRight from './components/NavbarRight/index';
import { clearLocalStorage } from '../../actions/auth.action';

class Topbar extends Component {
  constructor(props) {
    super(props);
    const { location } = props;
    const { pathname } = location || {};
    let id = null;
    Object.entries(EnumTopMenu).forEach(([key, value]) => {
      if (value.path === pathname) {
        id = value.id;
      }
    });
    this.state = {
      menuActive: id,
    };
  }

  handleDrawerShow = async () => {
    const { showAddNewsDrawerConnect } = this.props;
    showAddNewsDrawerConnect();
  };

  handleMenuActive = (active) => {
    this.setState({
      menuActive: active,
    });
  };

  handleLogoutClick = () => {
    this.props.clearLocalStorageConnect();
  };

  render() {
    const { menuActive } = this.state;
    const { authReducer } = this.props;
    const { permission } = authReducer || {};
    const permissionUrl = permission.map((x) => x.url);
    return (
      <React.Fragment>
        <header className="navbar navbar-header navbar-header-fixed navbar-biz navbar-dark">
          <span id="mainMenuOpen" className="burger-menu d-none">
            <i data-feather="menu" />
          </span>
          <span id="filemgrMenu" className="burger-menu d-lg-none">
            <i data-feather="arrow-left" />
          </span>
          <div className="navbar-brand">
            <Link to="/dashboard" className="df-logo">
              <img src={imgLogo} alt="VietnamBiz" width="180" />
            </Link>
          </div>
          <div id="navbarMenu" className="navbar-menu-wrapper">
            <div className="navbar-menu-header">
              <Link to="/dashboard" className="df-logo">
                <img src={imgLogo} alt="VietnamBiz" width="180" />
              </Link>
              <span id="mainMenuClose">
                <i data-feather="x" />
              </span>
            </div>
            <ul className="nav navbar-menu">
              <li className="nav-item" onClick={this.handleDrawerShow}>
                <span className="nav-link">
                  <FormOutlined />
                  Viết bài mới
                </span>
              </li>
              {permissionUrl.includes(EnumTopMenu.PIN_NEWS.path) && (
                <li
                  className={`nav-item ${
                    menuActive === EnumTopMenu.PIN_NEWS.id ? 'is-active' : ''
                  }`}
                  onClick={() => this.handleMenuActive(EnumTopMenu.PIN_NEWS.id)}
                >
                  <Link
                    to={`${EnumTopMenu.PIN_NEWS.path}`}
                    className="nav-link"
                  >
                    <PaperClipOutlined />
                    Cài bài
                  </Link>
                </li>
              )}
              {permissionUrl.includes(EnumTopMenu.NEWS.path) && (
                <li
                  className={`nav-item ${
                    menuActive === EnumTopMenu.NEWS.id ? 'is-active' : ''
                  }`}
                  onClick={() => this.handleMenuActive(EnumTopMenu.NEWS.id)}
                >
                  <Link to={`${EnumTopMenu.NEWS.path}`} className="nav-link">
                    <DatabaseOutlined />
                    Bài viết
                  </Link>
                </li>
              )}
              {permissionUrl.includes(EnumTopMenu.ROYALTY.path) && (
                <li
                  className={`nav-item ${
                    menuActive === EnumTopMenu.ROYALTY.id ? 'is-active' : ''
                  }`}
                  onClick={() => this.handleMenuActive(EnumTopMenu.ROYALTY.id)}
                >
                  <Link to={`${EnumTopMenu.ROYALTY.path}`} className="nav-link">
                    <MoneyCollectOutlined />
                    Nhuận bút
                  </Link>
                </li>
              )}
              {permissionUrl.includes(EnumTopMenu.STATISTIC.path) && (
                <li
                  className={`nav-item ${
                    menuActive === EnumTopMenu.STATISTIC.id ? 'is-active' : ''
                  }`}
                  onClick={() =>
                    this.handleMenuActive(EnumTopMenu.STATISTIC.id)
                  }
                >
                  <Link
                    to={`${EnumTopMenu.STATISTIC.path}`}
                    className="nav-link"
                  >
                    <BarChartOutlined />
                    Thống kê
                  </Link>
                </li>
              )}
              {permissionUrl.includes('/manage') && (
                <li
                  className={`nav-item ${
                    menuActive === EnumTopMenu.MANAGE.id ? 'is-active' : ''
                  }`}
                  onClick={() => this.handleMenuActive(EnumTopMenu.MANAGE.id)}
                >
                  <Link to={`${EnumTopMenu.MANAGE.path}`} className="nav-link">
                    <SettingOutlined />
                    Quản lý
                  </Link>
                </li>
              )}
            </ul>
          </div>
          <NavbarRight onLogoutClick={this.handleLogoutClick} />
        </header>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  settings: state.settings,
  authReducer: state.auth,
});

const mapDispatchToProps = {
  showAddNewsDrawerConnect: showAddNewsDrawer,
  clearLocalStorageConnect: clearLocalStorage,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Topbar));
