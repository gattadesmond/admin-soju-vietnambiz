import React from 'react';
import { Popover } from 'antd';

const content2 = (
  <div>
    <div className="dropdown-notification" style={{ width: '250px' }}>
      <a href="" className="dropdown-item">
        <div className="media">
          <div className="avatar avatar-sm avatar-online">
            <img
              src="http://themepixels.me/dashforge/1.1/assets/img/img1.png"
              className="rounded-circle"
              alt=""
            />
          </div>
          <div className="media-body mg-l-15">
            <p>
              Congratulate <strong>Socrates Itumay</strong> for work
              anniversaries
            </p>
            <span>Mar 15 12:32pm</span>
          </div>
        </div>
      </a>
      <a href="" className="dropdown-item">
        <div className="media">
          <div className="avatar avatar-sm avatar-online">
            <img
              src="http://themepixels.me/dashforge/1.1/assets/img/img1.png"
              className="rounded-circle"
              alt=""
            />
          </div>
          <div className="media-body mg-l-15">
            <p>
              <strong>Joyce Chua</strong> just created a new blog post
            </p>
            <span>Mar 13 04:16am</span>
          </div>
        </div>
      </a>
      <a href="" className="dropdown-item">
        <div className="media">
          <div className="avatar avatar-sm avatar-online">
            <img
              src="http://themepixels.me/dashforge/1.1/assets/img/img1.png"
              className="rounded-circle"
              alt=""
            />
          </div>
          <div className="media-body mg-l-15">
            <p>
              <strong>Althea Cabardo</strong> just created a new blog post
            </p>
            <span>Mar 13 02:56am</span>
          </div>
        </div>
      </a>
      <a href="" className="dropdown-item">
        <div className="media">
          <div className="avatar avatar-sm avatar-online">
            <img
              src="http://themepixels.me/dashforge/1.1/assets/img/img1.png"
              className="rounded-circle"
              alt=""
            />
          </div>
          <div className="media-body mg-l-15">
            <p>
              <strong>Adrian Monino</strong> added new comment on your photo
            </p>
            <span>Mar 12 10:40pm</span>
          </div>
        </div>
      </a>
      <div className="dropdown-footer">
        <a href="">View all Notifications</a>
      </div>
    </div>
  </div>
);

const NavBarRight = (props) => {
  const { onLogoutClick } = props;
  return (
    <div className="navbar-right">
      <Popover
        content={content2}
        title="Thông báo"
        placement="bottomRight"
        trigger="click"
      >
        <div className="dropdown-notification navbar-right-item">
          <div className="dropdown-link new-indicator">
            <i className="fa fa-bell-o" />
            <span>2</span>
          </div>
        </div>
      </Popover>

      <Popover
        content={
          <div className=" dropdown-profile" style={{ width: '220px' }}>
            <div className="avatar avatar-lg mg-b-15">
              <img
                src="http://themepixels.me/dashforge/1.1/assets/img/img1.png"
                className="rounded-circle"
                alt=""
              />
            </div>
            <h6 className="tx-semibold mg-b-5">Katherine Pechon</h6>
            <p className="mg-b-25 tx-12 tx-color-03">Administrator</p>

            <span className="dropdown-item">
              <i className="fa fa-edit" /> Edit Profile
            </span>

            <div className="dropdown-divider" />

            <span className="dropdown-item">
              <i className="fa fa-folder" /> Forum
            </span>
            <span className="dropdown-item">
              <i className="fa fa-user" />
              Account Settings
            </span>
            <span className="dropdown-item">
              <i className="fa fa-warning" />
              Privacy Settings
            </span>
            <span
              style={{ cursor: 'pointer' }}
              onClick={onLogoutClick}
              className="dropdown-item"
            >
              <i className="fa fa-lock" />
              Sign Out
            </span>
          </div>
        }
        title=""
        placement="bottomRight"
        trigger="click"
      >
        <div className="dropdown-profile navbar-right-item">
          <div className="dropdown-link">
            <div className="avatar avatar-sm ">
              <img
                src="http://themepixels.me/dashforge/1.1/assets/img/img1.png"
                className="rounded-circle"
                alt=""
              />
            </div>
          </div>
        </div>
      </Popover>
    </div>
  );
};
export default NavBarRight;
