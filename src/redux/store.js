import { compose, createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import { autoRehydrate, persistStore } from 'redux-persist';
import thunkMiddleware from 'redux-thunk';
import localForage from 'localforage';

import rootReducer from '../reducers';
import { actions } from '../constants/actionTypes';

const loggerMiddleware = createLogger();
const rehydrate = autoRehydrate();

const store = createStore(
  rootReducer,
  compose(applyMiddleware(thunkMiddleware), rehydrate, applyMiddleware(loggerMiddleware))
);

const initPersistentStore = () =>
  persistStore(
    store,
    {
      serialize: false,
      storage: localForage,
      whitelist: ['auth'],
      debounce: 1,
    },
    () => {
      store.dispatch({
        type: actions.REHYDRATE_COMPLETE,
        response: {
          rehydrated: true,
        },
      });
      const pathname = window.location.pathname;
    },
  );

let persistTmp;

localForage.getItem('reduxPersist:auth').then((value) => {
  console.log('reduxPersis_value', value);
  if (typeof value === 'string') {
    console.log('Clear old format db');
    localForage.clear(() => {
      localForage
        .length()
        .then(length => console.log('Clear success', length));
      localForage
        .getItem('reduxPersist:auth')
        .then(data => console.log('reduxPersist:auth', data));
      persistTmp = initPersistentStore();
    });
  } else {
    persistTmp = initPersistentStore();
  }
});

// enable persistent store
export const persist = () => persistTmp;
export default store;
