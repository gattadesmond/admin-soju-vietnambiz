import React from 'react';
import { Switch, Route } from 'react-router-dom';

import NewsRoute from './NewsRoute';
import Statistic from '../pages/Statistic/Statistic';
import Dashboard from '../pages/Dashboard/Dashboard';
import Pin from '../pages/Pin/Pin';
import Royalty from '../pages/Royalty/Royalty';
import Manage from '../pages/Manage/Manage';
import PermissionRoute from '../components/PermissionRoute/PermissionRoute';
import { EnumTopMenu } from '../constants';

const Routes = (props) => {
  const { permissionUrl } = props;
  return (
    <Switch>
      <Route
        exact
        path="/dashboard"
        component={Dashboard}
      />
      <PermissionRoute
        url={EnumTopMenu.NEWS.path}
        permissionUrl={permissionUrl}
        path="/news"
        component={NewsRoute}
      />
      <PermissionRoute
        url={EnumTopMenu.STATISTIC.path}
        permissionUrl={permissionUrl}
        path={EnumTopMenu.STATISTIC.path}
        component={Statistic}
      />
      <PermissionRoute
        url={EnumTopMenu.PIN_NEWS.path}
        permissionUrl={permissionUrl}
        exact
        path={EnumTopMenu.PIN_NEWS.path}
        component={Pin}
      />
      <PermissionRoute
        url={EnumTopMenu.ROYALTY.path}
        permissionUrl={permissionUrl}
        exact
        path={EnumTopMenu.ROYALTY.path}
        component={Royalty}
      />
      <Route path="/manage" component={Manage} />
      <Route exact path="/" component={Dashboard} />
    </Switch>
  );
};

export default Routes;
