import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import News from '../pages/News/News';

class NewsRoute extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/news/*" component={News} />
      </Switch>
    );
  }
}

export default NewsRoute;
