import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Statistic from '../pages/Statistic/Statistic';

class StatisticRoute extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/statistic" component={Statistic} />
      </Switch>
    );
  }
}

export default StatisticRoute;
