import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Dashboard from '../pages/Dashboard/Dashboard';

class DashboardRoute extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/dashboard" component={Dashboard} />
      </Switch>
    );
  }
}

export default DashboardRoute;
