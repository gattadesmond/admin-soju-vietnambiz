import React from 'react';
import { Provider } from 'react-redux';
import { withRouter } from 'react-router-dom';
import 'react-perfect-scrollbar/dist/css/styles.css';

import store from './redux/store';
import App from './layout/App';

import './assets/scss/soju.scss';

const NextApp = () => (
  <Provider store={store}>
    <React.Fragment>
      <App />
    </React.Fragment>
  </Provider>
);
export default withRouter(NextApp);
