import moment from 'moment';
import { actions } from '../constants';
import { isEmpty } from '../utils';

const initialState = {
  hasMore: true,
  newslog: {
    data: [],
    total: 0,
  },
};
function newsLogReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_NEWS_LOG_RECEIVED: {
      return {
        ...state,
        newslog: {
          ...state.newslog,
          data: state.newslog.data.concat(action.reply),
          total: action.total,
        },
        hasMore: action.total < action.pageSize ? !state.hasMore : state.hasMore,
      };
    }
    case actions.RESET_NEWS_LOG_RECEIVED: {
      return {
        ...state,
        newslog: {
          ...state.newslog,
          data: [],
          total: 0,
        },
        hasMore: true,
      };
    }
    default: {
      return state;
    }
  }
}

export default newsLogReducer;