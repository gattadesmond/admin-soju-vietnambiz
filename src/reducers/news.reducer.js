import moment from 'moment';
import { actions, EnumNewsType } from '../constants';
import { isEmpty } from '../utils';
import { EnumType } from '../constants/enum';

import { __IMAGE_HOST__ } from 'global.js';



const initialState = {
  addNewsDrawerOpen: false,
  compareVersionOpen: false,
  news: {
    data: [],
    total: 0,
  },
  newsRelation: {
    data: [],
    total: 0,
  },
  newsRelationChose: {},
  detail: {},
  statistic: {},
  newsVersion: null,
  currentEncryptId: null,
  isReload: false,
};

const initDetail = {
  encryptId: 0,
  newsEditor: {
    title: '',
    sapo: '',
    body: '',
  },
  basicInfo: {
    avatarNormal: '',
    avatarVertical: '',
    avatarSquare: '',
    avatarFacebook: '',
    isShowAvatar: false,
    avatarDesc: '',
    type: EnumType.NORMAL.id,
    newsType: EnumNewsType.NOT_DISPLAY.id,
    author: '',
    distributionDate: moment(),
    note: '',
    source: '',
    sourceURL: '',
    expertId: '',
    tag: [],
  },
  releaseConfig: {
    zoneId: null,
    zoneIds: [],
    topicId: '',
    displayPosition: 1,
  },
  royaltyConfig: {
    royalties: [],
  },
};
const initNewsRelationChose = {
  relationItems: [],
  relationSpecialItems: [],
};

function formatTag(tagItem, tagSource) {
  const tag = [];
  tagItem.forEach((item, index) => {
    const currentItem = tagSource.find((t) => t.id === parseInt(item, 10));
    const { id, name, newsCount } = currentItem;
    tag.push({
      value: id,
      label: `${name} (${newsCount})`,
    });
  });
  return tag;
}

function formatExpert(expertItem, expertSource) {
  let expert = {};
  if (expertSource && expertSource.length > 0) {
    const currentItem = expertSource.find(
      (t) => t.id === parseInt(expertItem, 10),
    );
    const { id, expertName } = currentItem;
    expert = {
      value: id,
      label: expertName,
    };
  }
  return expert;
}

function formatTopic(topicItem, topicSource) {
  let topic = {};
  if (topicSource && topicSource.length > 0) {
    const currentItem = topicSource.find(
      (t) => t.id === parseInt(topicItem, 10),
    );
    const { id, topicName } = currentItem;
    topic = {
      value: id,
      label: topicName,
    };
  }
  return topic;
}

function formatResponseData(
  data,
  tagSource,
  expertSource,
  topicSource,
  royalty,
) {
  const {
    id,
    encryptId,
    title,
    sapo,
    body,
    avatar,
    avatar3,
    avatar4,
    avatar5,
    type,
    author,
    newsType,
    distributionDate,
    note,
    source,
    sourceURL,
    tag,
    zoneId,
    zoneIds,
    topicId,
    expertId,
    newsRelation,
    newsRelationSpecial,
    avatarDesc,
    isShowAvatar,
    displayPosition,
  } = data || {};
  return {
    ...data,
    id,
    encryptId,
    newsEditor: {
      title,
      sapo,
      body,
    },
    basicInfo: {
      avatarNormal: `${__IMAGE_HOST__}${avatar}`,
      avatarVertical: `${__IMAGE_HOST__}${avatar3}`,
      avatarSquare: `${__IMAGE_HOST__}${avatar4}`,
      avatarFacebook: `${__IMAGE_HOST__}${avatar5}`,
      isShowAvatar,
      avatarDesc,
      type,
      newsType,
      author,
      distributionDate: moment(distributionDate),
      note,
      source,
      sourceURL,
      ...(!isEmpty(expertId)
        ? { expertId: formatExpert(expertId, expertSource) }
        : { expertId: {} }),
      ...(!isEmpty(tag)
        ? { tag: formatTag(tag.split(','), tagSource) }
        : { tag: [] }),
    },
    releaseConfig: {
      zoneId,
      ...(!isEmpty(zoneIds)
        ? { zoneIds: zoneIds.split(',').map(Number) }
        : { zoneIds: [] }),
      ...(!isEmpty(topicId)
        ? { topicId: formatTopic(topicId, topicSource) }
        : { topicId: {} }),
      displayPosition,
    },
    royaltyConfig: {
      royalties: royalty,
    },
    ...(!isEmpty(newsRelation) && { newsRelation }),
    ...(!isEmpty(newsRelationSpecial) && { newsRelationSpecial }),
  };
}

function newsReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_NEWS_DETAIL_RECEIVED: {
      const { detail, tag } = action.reply;
      return {
        ...state,
        detail,
        tag,
      };
    }
    case actions.ADD_NEWS_DRAWER_SHOW: {
      const { detail, newsRelationChose, tag, expert, topic, royalty } = action;
      return {
        ...state,
        addNewsDrawerOpen: true,
        isDisable: action.isDisable,
        detail: detail.encryptId
          ? formatResponseData(detail, tag, expert, topic, royalty)
          : detail,
        newsRelationChose,
      };
    }
    case actions.COMPARE_VERSION_SHOW: {
      const { newsVersion, currentEncryptId } = action;
      return {
        ...state,
        compareVersionOpen: true,
        newsVersion,
        currentEncryptId,
      };
    }
    case actions.RELOAD_NEWS: {
      return {
        ...state,
        isReload: action.isReload,
      };
    }
    case actions.COMPARE_VERSION_CLOSE: {
      return {
        ...state,
        compareVersionOpen: false,
        newsVersion: null,
        currentEncryptId: null,
      };
    }
    case actions.GET_NEWS_STATISTIC_RECEIVED: {
      const { reply } = action;
      return {
        ...state,
        statistic: reply,
      };
    }
    case actions.ADD_NEWS_DRAWER_CLOSE: {
      return {
        ...state,
        addNewsDrawerOpen: false,
        isDisable: false,
        detail: { ...initDetail },
        newsRelationChose: { ...initNewsRelationChose },
      };
    }
    case actions.GET_NEWS_RECEIVED: {
      return {
        ...state,
        news: { ...state.news, data: action.reply, total: action.total },
      };
    }
    case actions.GET_NEWS_RELATION_ITEMS_RECEIVED: {
      return {
        ...state,
        detail: {
          ...state.detail,
          choseItems: {
            ...state.detail.choseItems,
            newsRelation: action.reply,
          },
        },
      };
    }
    case actions.GET_NEWS_RELATION_SPECIAL_ITEMS_RECEIVED: {
      return {
        ...state,
        detail: {
          ...state.detail,
          choseItems: {
            ...state.detail.choseItems,
            newsRelationSpecial: action.reply,
          },
        },
      };
    }
    case actions.GET_NEWS_RELATION_RECEIVED: {
      return {
        ...state,
        newsRelation: {
          ...state.newsRelation,
          data: action.reply,
          total: action.total,
        },
      };
    }
    default: {
      return state;
    }
  }
}

export default newsReducer;
