import { combineReducers } from 'redux';
import auth from './auth.reducer';
import news from './news.reducer';
import global from './global.reducer';
import tag from './tag.reducer';
import newslog from './newslog.reducer';
import newsLogAction from './newslogaction.reducer';
import account from './account.reducer';
import permission from './permission.reducer';
import royalty from './royalty.reducer';

const rootReducer = combineReducers({
  auth,
  news,
  global,
  tag,
  newslog,
  newsLogAction,
  account,
  permission,
  royalty,
});

export default rootReducer;
