import { actions } from '../constants';

const initialState = {
  permissionGroups: [],
  permissionGroupTotal: 0,
  permissionCheckedKeys: [],
  permissionZoneMap: new Map(),
  configPermissionModalOpen: false,
  permissionGroupId: null,
};

function permissionReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_PERMISSION_GROUP_RECEIVED: {
      return {
        ...state,
        permissionGroups: action.reply,
        permissionGroupTotal: action.total,
      };
    }
    case actions.GET_CONFIG_PERMISSION_RECEIVED: {
      const { reply, permissionGroupId } = action;
      const permissionConfig = reply;
      const permissionZoneMap = new Map();
      const permissionCheckedKeys = [];
      permissionConfig.forEach((permission, index) => {
        const { id, zoneIds } = permission;
        permissionZoneMap.set(id, zoneIds.split(',').map(Number));
        permissionCheckedKeys.push(id);
      });
      return {
        ...state,
        permissionCheckedKeys,
        permissionZoneMap,
        permissionGroupId,
      };
    }
    case actions.CONFIG_PERMISSION_MODAL_OPEN: {
      const { reply, permissionGroupId } = action;
      const permissionConfig = reply;
      const permissionZoneMap = new Map();
      const permissionCheckedKeys = [];
      permissionConfig.forEach((permission, index) => {
        const { id, zoneIds } = permission;
        permissionZoneMap.set(id, zoneIds.split(',').map(Number));
        permissionCheckedKeys.push(id);
      });
      return {
        ...state,
        configPermissionModalOpen: true,
        permissionCheckedKeys,
        permissionZoneMap,
        permissionGroupId,
      };
    }
    case actions.CONFIG_PERMISSION_MODAL_CLOSE: {
      return {
        ...state,
        configPermissionModalOpen: false,
        permissionCheckedKeys: [],
        permissionZoneMap: null,
        permissionGroupId: null,
      };
    }
    default: {
      return { ...state };
    }
  }
}
export default permissionReducer;
