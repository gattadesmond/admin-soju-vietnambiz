import moment from 'moment';
import { actions, EnumNewsLogTypeAction } from '../constants';
import { isEmpty } from '../utils';

const initialState = {
  data: [],
};

function newsLogActionReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_LOG_ACTION_BY_PARENT: {
      return {
        ...state,
        data: action.data,
      };
    }
    default: {
      return state;
    }
  }
}

export default newsLogActionReducer;