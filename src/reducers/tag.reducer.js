import { actions } from '../constants';

const initialState = {
  tags: [],
};

function tagReducer(state = initialState, action) {
  switch (action.type) {
    case actions.CLEAR_LOCAL_STORAGE: {
      return {
        ...initialState,
      };
    }
    case actions.LOGIN_SUCCESSFULLY: {
      const { response } = action;
      const { username, errorMessage } = response || {};
      return {
        ...initialState,
        username,
        errorMessage,
        authenticated: true,
      };
    }

    case actions.LOGIN_FAIL: {
      return {
        ...state,
        username: '',
        errorMessage: null,
        authenticated: false,
      };
    }
    default: {
      return state;
    }
  }
}

export default tagReducer;
