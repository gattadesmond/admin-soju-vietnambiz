import { actions } from '../constants';
import { commonConst } from '../constants/constants';

const initialSettings = {
  locale: {
    languageId: 'english',
    locale: 'en',
    name: 'English',
    icon: 'us',
  },
  zones: [],
  permissions: [],
  isAppInitDone: false,
  rehydrated: false,
  loadingTitle: commonConst.defaultGlobalLoadingTitle,
};

const app = (state = initialSettings, action) => {
  switch (action.type) {
    case actions.GET_ZONE_RECEIVED: {
      return {
        ...state,
        zones: action.reply,
      };
    }
    case actions.GET_PERMISSION_RECEIVED: {
      return {
        ...state,
        permissions: action.reply,
      };
    }
    case actions.REHYDRATE_COMPLETE: {
      const { response } = action;
      return {
        ...state,
        rehydrated: response.rehydrated,
      };
    }
    case actions.APP_INIT_DONE: {
      return {
        ...state,
        isAppInitDone: true,
      };
    }
    case actions.START_APP_LOADING: {
      return {
        ...state,
        isLoading: true,
        loadingTitle: action.title,
      };
    }

    case actions.STOP_APP_LOADING: {
      return {
        ...state,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

export default app;
