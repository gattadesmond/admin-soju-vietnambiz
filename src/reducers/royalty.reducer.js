import { actions } from '../constants';

const initialState = {
  coefTypes: [],
  coefAccounts: [],
  coefRoyalties: [],
  initData: {
    categoryId: null,
    photoCount: 0,
    videoCount: 0,
    qualityValue: 1,
    dedicationValue: 1,
    riskValue: 1,
    contributionValue: 1,
    royaltyFor: null,
    note: '',
  },
};

function royaltyReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_COEF_TYPE_RECEIVED: {
      return {
        ...state,
        coefTypes: action.reply,
      };
    }
    case actions.GET_ACCOUNT_COEF_RECEIVED: {
      return {
        ...state,
        coefAccounts: action.reply,
      };
    }
    case actions.GET_COEF_ROYALTY_RECEIVED: {
      const { reply } = action;
      const {
        contributionDefault,
        dedicationDefault,
        qualityDefault,
        riskDefault,
      } = reply;
      return {
        ...state,
        coefRoyalties: reply,
        initData: {
          ...state.initData,
          qualityValue: qualityDefault,
          dedicationValue: dedicationDefault,
          riskValue: riskDefault,
          contributionValue: contributionDefault,
        },
      };
    }
    default: {
      return state;
    }
  }
}

export default royaltyReducer;
