import { actions } from '../constants';

const initialState = {
  account: {},
  authenticated: false,
  urlRefer: '',
  permission: [],
};

function userAuthen(state = initialState, action) {
  switch (action.type) {
    case actions.CLEAR_LOCAL_STORAGE: {
      return {
        ...initialState,
      };
    }
    case actions.LOGIN_SUCCESSFULLY: {
      const { reply } = action;
      const { account, permission } = reply || {};
      return {
        ...initialState,
        account,
        permission,
        authenticated: true,
      };
    }

    case actions.LOGIN_FAIL: {
      return {
        ...state,
        account: null,
        permission: [],
        authenticated: false,
      };
    }
    default: {
      return state;
    }
  }
}

export default userAuthen;
