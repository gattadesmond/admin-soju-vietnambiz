import { actions } from '../constants';
import { generateAntOptionsFromArray } from '../utils';

const initialState = {
  accounts: [],
  total: 0,
  detail: {},
  addEditProfileModalOpen: false,
  resetPasswordModalOpen: false,
  addEditPermissionModalOpen: false,
  permissions: [],
  permissionGroups: [],
  isReload: false,
  permissionCheckedKeys: [],
  permissionZoneMap: new Map(),
  permissionGroupOptions: [],
  accountId: null,
};

function accountReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_ACCOUNT_RECEIVED: {
      return {
        ...state,
        accounts: action.reply,
        total: action.total,
      };
    }
    case actions.RELOAD_ACCOUNT: {
      return {
        ...state,
        isReload: action.isReload,
      };
    }
    case actions.ADD_EDIT_PROFILE_MODAL_OPEN: {
      const { detail } = action.reply;
      return {
        ...state,
        detail,
        addEditProfileModalOpen: true,
      };
    }
    case actions.ADD_EDIT_PROFILE_MODAL_CLOSE: {
      return {
        ...state,
        detail: {},
        addEditProfileModalOpen: false,
      };
    }
    case actions.GET_ACCOUNT_PERMISSION_RECEIVED: {
      const permissions = action.reply;
      const permissionZoneMap = new Map();
      const permissionCheckedKeys = [];
      permissions.forEach((permission, index) => {
        const { id, zoneIds } = permission;
        permissionZoneMap.set(id, zoneIds.split(',').map(Number));
        permissionCheckedKeys.push(id);
      });
      return {
        ...state,
        permissions: action.reply,
        permissionCheckedKeys,
        permissionZoneMap,
      };
    }
    case actions.GET_ACCOUNT_PERMISSION_GROUP_RECEIVED: {
      return {
        ...state,
        permissionGroups: action.reply,
        permissionGroupOptions: generateAntOptionsFromArray(
          action.reply,
          null,
          'id',
          'name',
        ),
      };
    }
    case actions.APPROVE_PERMISSION: {
      const approve = action.reply;
      const permissionZoneMap = new Map();
      const permissionCheckedKeys = [];
      approve.forEach((permission) => {
        const { id, zoneIds } = permission;
        permissionZoneMap.set(id, zoneIds.split(',').map(Number));
        permissionCheckedKeys.push(id);
      });
      return {
        ...state,
        permissionCheckedKeys,
        permissionZoneMap,
      };
    }
    case actions.ADD_EDIT_PERMISSION_MODAL_OPEN: {
      return {
        ...state,
        addEditPermissionModalOpen: true,
        accountId: action.accountId,
      };
    }
    case actions.ADD_EDIT_PERMISSION_MODAL_CLOSE: {
      return {
        ...state,
        addEditPermissionModalOpen: false,
        permissionCheckedKeys: [],
        permissionZoneMap: new Map(),
        accountId: null,
      };
    }
    case actions.RESET_PASSWORD_MODAL_OPEN: {
      return {
        ...state,
        resetPasswordModalOpen: true,
      };
    }
    case actions.RESET_PASSWORD_MODAL_CLOSE: {
      return {
        ...state,
        resetPasswordModalOpen: false,
      };
    }
    default: {
      return { ...state };
    }
  }
}
export default accountReducer;
