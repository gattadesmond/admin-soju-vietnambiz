/* eslint-disable import/first,consistent-return */
/* eslint-disable consistent-return */
import React from 'react';

export const EnumTopMenu = {
  ADD_NEWS: {
    id: 0,
    description: 'ADD_NEWS',
  },
  PIN_NEWS: {
    id: 1,
    path: '/pin',
    description: 'PIN_NEWS',
  },
  NEWS: {
    id: 2,
    path: '/news/my-post',
    description: 'NEWS',
  },
  ROYALTY: {
    id: 3,
    path: '/royalty',
    description: 'ROYALTY',
  },
  STATISTIC: {
    id: 4,
    path: '/statistic',
    description: 'STATISTIC',
  },
  MANAGE: {
    id: 5,
    path: '/manage/account',
    description: 'MANAGE',
  },
};

export const EnumNewsSidebar = {
  MY_POST: {
    id: -1,
    path: '/news/my-post',
    description: 'MY_POST',
  },
  TEMP_POST: {
    id: 1,
    path: '/news/temp-post',
    description: 'TEMP_POST',
  },
  WAITING_EDITING_POST: {
    id: 2,
    path: '/news/waiting-editing-post',
    description: 'WAITING_EDITING_POST',
  },
  RECEIVED_EDITING_POST: {
    id: 3,
    path: '/news/received-editing-post',
    description: 'RECEIVED_EDITING_POST',
  },
  WAITING_RELEASE_POST: {
    id: 5,
    path: '/news/waiting-release-post',
    description: 'WAITING_RELEASE_POST',
  },
  RECEIVED_RELEASE_POST: {
    id: 6,
    path: '/news/received-release-post',
    description: 'RECEIVED_RELEASE_POST',
  },
  RELEASE_POST: {
    id: 8,
    path: '/news/release-post',
    description: 'RELEASE_POST',
  },
  MY_RETURN_POST: {
    id: 20,
    path: '/news/my-return-post',
    description: 'MY_RETURN_POST',
  },
  TAKEN_DOWN_POST: {
    id: 10,
    path: '/news/taken-down-post',
    description: 'TAKEN_DOWN_POST',
  },
  REMOVED_POST: {
    id: 9,
    path: '/news/removed-post',
    description: 'REMOVED_POST',
  },
  PROCESSING_POST: {
    id: 30,
    path: '/news/processing-post',
    description: 'PROCESSING_POST',
  },
  EVOLVE_POST: {
    id: 40,
    path: '/news/evolve-post',
    description: 'EVOLVE_POST',
  },
  MAGAZINE_POST: {
    id: 50,
    path: '/news/magazine-post',
    description: 'MAGAZINE_POST',
  },
};

export const EnumResponseCode = {
  SERVER_ERROR: {
    id: -1,
    description: 'ServerError',
  },
  SUCCESS: {
    id: 0,
    description: 'Success',
  },
  REQUIRED_LOGIN: {
    id: 1,
    description: 'RequiredLogin',
  },
  PARAMS_INVALID: {
    id: 2,
    description: 'ParamsInvalid',
  },
  EXISTED_USERNAME: {
    id: 3,
    description: 'ExistedUserName',
  },
  EXISTED_EMAIL: {
    id: 4,
    description: 'ExistedEmail',
  },
  ACCOUNT_INACTIVE: {
    id: 5,
    description: 'AccountInActive',
  },
};

export const EnumType = {
  NORMAL: {
    id: 0,
    description: 'Bài thường (Size M)',
  },
  MAGAZINE: {
    id: 27,
    description: 'Bài Magazine',
  },
  MINI_MAGAZINE: {
    id: 28,
    description: 'Bài Mini Magazine',
  },
  EVOLVE: {
    id: 10,
    description: 'Bài tường thuật sự kiện',
  },
  INFOGRAPHIC: {
    id: 20,
    description: 'Bài Infographic',
  },
  PHOTOSTORY: {
    id: 29,
    description: 'Bài Photostory',
  },
};

export const EnumNewsRelationModal = {
  RELATION: {
    id: 1,
    maxItemChoose: 5,
    description: 'Tin liên quan',
  },
  RELATION_SPECIAL: {
    id: 2,
    maxItemChoose: 3,
    description: 'Tin liên quan chân bài',
  },
};

export const EnumNewsType = {
  NOT_DISPLAY: {
    id: 1,
    description: 'Không hiển thị Icon',
  },
  PHOTO_ICON: {
    id: 2,
    description: 'Hiển thị icon ảnh',
  },
  VIDEO_ICON: {
    id: 3,
    description: 'Hiển thị icon video',
  },
  MULTIPLE: {
    id: 4,
    description: 'Hiển thị icon ảnh và video',
  },
};

export const EnumDisplayPosition = {
  NORMAL: {
    id: 0,
    description: 'Tin thông thường',
  },
  HIGHLIGHT_HOME: {
    id: 1,
    description: 'Nổi bật trang chủ',
  },
  HIGHLIGHT_ZONE: {
    id: 2,
    description: 'Nổi bật danh mục',
  },
};
export const EnumManageSidebar = {
  MANAGE_ACCOUNT: {
    id: 1,
    path: '/manage/account',
    description: 'MANAGE_ACCOUNT',
  },
  MANAGE_PERMISSION: {
    id: 2,
    path: '/manage/permission',
    description: 'MANAGE_PERMISSION',
  },
  MANAGE_TAG: {
    id: 3,
    path: '/manage/tags',
    description: 'MANAGE_TAG',
  },
  MANAGE_ZONE: {
    id: 4,
    path: '/manage/zone',
    description: 'MANAGE_ZONE',
  },
  MANAGE_SEO_META_TAG: {
    id: 5,
    path: '/manage/seo-meta-tag',
    description: 'MANAGE_SEO_META_TAG',
  },
  MANAGE_CONFIG: {
    id: 6,
    path: '/manage/config-manager',
    description: 'MANAGE_CONFIG',
  },
  MANAGE_TOPIC: {
    id: 7,
    path: '/manage/topic',
    description: 'MANAGE_TOPIC',
  },
  MANAGE_EXPERTS: {
    id: 8,
    path: '/manage/experts',
    description: 'MANAGE_EXPERTS',
  },
  NEWS_LOG: {
    id: 9,
    path: '/manage/news-log',
    description: 'NEWS_LOG',
  },
  MANAGE_FILE: {
    id: 10,
    path: '/manage/file-manager',
    description: 'MANAGE_FILE',
  },
  MANAGE_BOX_VIDEO: {
    id: 11,
    path: '/manage/box-video',
    description: 'MANAGE_BOX_VIDEO',
  },
  MANAGE_BOX_ONPAGE: {
    id: 12,
    path: '/manage/box-on-page',
    description: 'MANAGE_BOX_ONPAGE',
  },
  MANAGE_BOX_TAG: {
    id: 13,
    path: '/manage/box-tag',
    description: 'MANAGE_BOX_TAG',
  },
  MANAGE_BOX_TOPIC: {
    id: 14,
    path: '/manage/box-topic',
    description: 'MANAGE_BOX_TOPIC',
  },
  MANAGE_BOX_EXPERTS: {
    id: 15,
    path: '/manage/box-experts',
    description: 'MANAGE_BOX_EXPERTS',
  },
};
export const EnumNewsLogType = {
  LOG_TYPE_NEWS: {
    id: 1,
    description: 'Tin, Bài',
  },
  LOG_TYPE_VIDOE: {
    id: 2,
    description: 'Video',
  },
  LOG_TYPE_COMMENT: {
    id: 3,
    description: 'Bình luận',
  },
  LOG_TYPE_LOGIN: {
    id: 4,
    description: 'Log đăng nhập',
  },
  LOG_TYPE_NEWS_HISTORY: {
    id: 5,
    description: 'Lịch sử Tin, Bài',
  },
  LOG_TYPE_USER: {
    id: 6,
    description: 'User',
  },
};
export const EnumNewsLogTypeAction = {
  LOG_NEWS_ALL: {
    id: -1,
    parentid: 1,
    description: 'Tất cả',
  },
  LOG_VIDEO_ALL: {
    id: -1,
    parentid: 2,
    description: 'Tất cả',
  },
  LOG_NEWS_ADD: {
    id: 0,
    parentid: 1,
    description: 'Thêm mới',
  },
  LOG_NEWS_EDIT: {
    id: 1,
    parentid: 1,
    description: 'Cập nhật',
  },
  LOG_NEWS_DELETE: {
    id: 2,
    parentid: 1,
    description: 'Xóa',
  },
  LOG_NEWS_SEND_WAIT_EDITOR: {
    id: 3,
    parentid: 1,
    description: 'Gửi lên chờ biên tập',
  },
  LOG_NEWS_SEND_WAIT_PUBLISH: {
    id: 4,
    parentid: 1,
    description: 'Gửi lên chờ xuất bản',
  },
  LOG_NEWS_RETURN_EDITOR: {
    id: 5,
    parentid: 1,
    description: 'Trả lại biên tập viên',
  },
  LOG_NEWS_RETURN_REPORTER: {
    id: 6,
    parentid: 1,
    description: 'Trả lại phóng viên',
  },
  LOG_NEWS_RECEIVED_EDITOR: {
    id: 7,
    parentid: 1,
    description: 'Nhận sửa',
  },
  LOG_NEWS_RECEIVED_PUBLISH: {
    id: 8,
    parentid: 1,
    description: 'Nhận xuất bản',
  },
  LOG_NEWS_PUBLISH: {
    id: 9,
    parentid: 1,
    description: 'Xuất bản',
  },
  LOG_NEWS_TAKE_DOWN: {
    id: 10,
    parentid: 1,
    description: 'Gỡ bỏ',
  },
  LOG_NEWS_REVERSE: {
    id: 11,
    parentid: 1,
    description: 'Đổi chỗ bài nổi bật',
  },
  LOG_VIDEO_UPLOAD: {
    id: 1,
    parentid: 2,
    description: 'Upload',
  },
  LOG_VIDEO_ADD_INFO: {
    id: 2,
    parentid: 2,
    description: 'Thêm mới',
  },
  LOG_VIDEO_UPDATE: {
    id: 3,
    parentid: 2,
    description: 'Cập nhật',
  },
  LOG_VIDEO_DELETE: {
    id: 4,
    parentid: 2,
    description: 'Xóa',
  },
  LOG_VIDEO_SEND_WAIT_PUBLISH: {
    id: 5,
    parentid: 2,
    description: 'Gửi lên chờ xuất bản',
  },
  LOG_VIDEO_RETURN: {
    id: 6,
    parentid: 2,
    description: 'Trả lại',
  },
  LOG_VIDEO_PUBLISH: {
    id: 7,
    parentid: 2,
    description: 'Xuất bản',
  },
  LOG_VIDEO_TAKE_DOWN: {
    id: 8,
    parentid: 2,
    description: 'Gỡ bỏ',
  },
  LOG_VIDEO_PIN: {
    id: 9,
    parentid: 2,
    description: 'Cài video nổi bật',
  },
};

export const EnumAccountKeywordFilter = {
  USERNAME: {
    id: 1,
    description: 'Tên tài khoản',
  },
  FULLNAME: {
    id: 2,
    description: 'Tên người dùng',
  },
  EMAIL: {
    id: 3,
    description: 'Email',
  },
  MOBILE: {
    id: 4,
    description: 'Điện thoại',
  },
};

export const EnumAccountActionFilter = {
  LOCK_ACCOUNT: {
    id: 0,
    description: 'Khóa tài khoản được chọn',
  },
  UNLOCK_ACCOUNT: {
    id: 1,
    description: 'Mở khóa tài khoản được chọn',
  },
};

export const EnumAccountStatus = {
  LOCK_ACCOUNT: {
    id: 0,
    description: 'Tài khoản bị khóa',
  },
  UNLOCK_ACCOUNT: {
    id: 1,
    description: 'Tài khoản không bị khóa',
  },
};
