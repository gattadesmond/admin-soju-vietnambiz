export const commonConst = {
  pageIndex: 1,
  pageSize: 10000,
  pageSizeSearch: 50,
  sideBarPageSize: 50,
  defaultOptions: -1,
  defaultObject: '',
  defaultArray: [],
  defaultGlobalLoadingTitle: 'Đang xử lý ...',
};

export const addNewsPermission = 28;

export const TIME_TO_QUERY_AFTER_STOP_TYPING = 250;

export const colorCodes = [
  '#C0EFB2',
  '#72a5ca',
  '#64C855',
  '#CD8DDA',
  '#2767de',
  '#C49DF9',
  '#9ABE9D',
  '#D78281',
  '#F5E25D',
  '#DFB6A0',
  '#67E3E4',
];

export const WHOLE_COUNTRY = -1;
