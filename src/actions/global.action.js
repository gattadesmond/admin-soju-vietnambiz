/* eslint-disable */
import request from '../utils/request';
import { actions, EnumResponseCode } from '../constants';

export function startAppLoading(title) {
  return {
    type: actions.START_APP_LOADING,
    title,
  };
}

export function stopAppLoading() {
  return {
    type: actions.STOP_APP_LOADING,
  };
}

export function getZones() {
  return async (dispatch) => {
    const response = await request.get('zones');
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({ type: actions.GET_ZONE_RECEIVED, reply });
      return response;
    }
  };
}

export function getPermissions() {
  return async (dispatch) => {
    const response = await request.get('permissions');
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({ type: actions.GET_PERMISSION_RECEIVED, reply });
      return response;
    }
  };
}

export function getCoefRoyalty() {
  return async (dispatch) => {
    const response = await request.get('coef/royalty');
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({ type: actions.GET_COEF_ROYALTY_RECEIVED, reply });
      return response;
    }
  };
}

export function getAllCoefType() {
  return async (dispatch) => {
    const response = await request.get('coef/type');
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({ type: actions.GET_COEF_TYPE_RECEIVED, reply });
      return response;
    }
  };
}

export function getAccountCoef() {
  return async (dispatch) => {
    const response = await request.get('accounts/coef');
    const { responseCode, reply, total } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({ type: actions.GET_ACCOUNT_COEF_RECEIVED, reply, total });
    }
  };
}

export function initApp() {
  return (dispatch) => {
    Promise.all([
      dispatch(getZones()),
      dispatch(getPermissions()),
      dispatch(getCoefRoyalty()),
      dispatch(getAllCoefType()),
      dispatch(getAccountCoef()),
    ]).then(() => {
      dispatch({
        type: actions.APP_INIT_DONE,
      });
      dispatch(stopAppLoading());
    });
  };
}
