import { actions, EnumResponseCode } from '../constants';
import { persist } from '../redux/store';
import { isEmpty, isEmptyObject } from '../utils';
import request from '../utils/request';
import { initApp, startAppLoading, stopAppLoading } from './global.action';

export function setUrlRefer(url_refer) {
  return (dispatch) => {
    dispatch({ type: actions.SET_URL_REFER, response: { url_refer } });
  };
}

export function clearLocalStorage() {
  return (dispatch) => {
    persist().purge();
    dispatch({
      type: actions.CLEAR_LOCAL_STORAGE,
    });
  };
}

export function login(usernameRequest, password) {
  return async (dispatch) => {
    dispatch(startAppLoading('Đang khởi tạo CMS ...'));
    const response = await request.get('account/auth', {
      username: usernameRequest,
      password,
    });
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      if (isEmptyObject(reply)) {
        dispatch({
          type: actions.LOGIN_FAIL,
        });
        return {
          status: 'fail',
          errorMsg: 'Tên đăng nhập hoặc mật khẩu không đúng',
        };
      }
      dispatch({
        type: actions.LOGIN_SUCCESSFULLY,
        reply,
      });
      dispatch(initApp());
      return {
        status: 'success',
        errorMsg: '',
      };
    }
    dispatch(stopAppLoading());
    return {
      status: 'fail',
      errorMsg: 'Tên đăng nhập hoặc mật khẩu không đúng',
    };
  };
}
