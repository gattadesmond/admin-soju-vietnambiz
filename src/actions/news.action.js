import moment from 'moment';
import { actions, EnumDisplayPosition } from '../constants';
import request from '../utils/request';
import { EnumNewsType, EnumResponseCode, EnumType } from '../constants/enum';
import { isEmpty, removeParamsForServer } from '../utils';
import { startAppLoading, stopAppLoading } from './global.action';

const initDetail = {
  encryptId: 0,
  newsEditor: {
    title: '',
    sapo: '',
    body: '',
  },
  basicInfo: {
    avatarNormal: '',
    avatarVertical: '',
    avatarSquare: '',
    avatarFacebook: '',
    isShowAvatar: false,
    avatarDesc: '',
    type: EnumType.NORMAL.id,
    newsType: EnumNewsType.NOT_DISPLAY.id,
    author: '',
    distributionDate: moment(),
    note: '',
    source: '',
    sourceURL: '',
    expertId: '',
    tag: [],
  },
  releaseConfig: {
    zoneId: null,
    zoneIds: [],
    topicId: '',
    displayPosition: EnumDisplayPosition.NORMAL.id,
  },
  royaltyConfig: {
    royalties: [],
  },
};
const initNewsRelationChose = {
  relationItems: [],
  relationSpecialItems: [],
};

function formatRequestData(data) {
  const {
    id,
    encryptId,
    newsRelation,
    newsRelationSpecial,
    createdBy,
    lastModifiedBy,
    editedBy,
    publishedBy,
  } = data || {};
  const { basicInfo, releaseConfig, newsEditor, royaltyConfig } = data || {};
  const { title, sapo, body } = newsEditor || {};
  const {
    avatarNormal,
    avatarVertical,
    avatarSquare,
    avatarFacebook,
    avatarDesc,
    isShowAvatar,
    type,
    author,
    distributionDate,
    note,
    expertId,
    source,
    sourceURL,
    tag,
    newsType,
  } = basicInfo || {};
  const { zoneId, zoneIds, topicId, displayPosition } = releaseConfig || {};
  return {
    ...data,
    id,
    encryptId,
    title,
    sapo,
    body,
    createdBy,
    lastModifiedBy,
    editedBy,
    publishedBy,
    basicInfo: {
      ...(!isEmpty(avatarNormal) && { avatar: avatarNormal }),
      ...(!isEmpty(avatarNormal) && { avatar2: avatarNormal }),
      ...(!isEmpty(avatarVertical) && { avatar3: avatarVertical }),
      ...(!isEmpty(avatarSquare) && { avatar4: avatarSquare }),
      ...(!isEmpty(avatarFacebook) && { avatar5: avatarFacebook }),
      isShowAvatar,
      avatarDesc,
      type,
      newsType,
      author,
      distributionDate: moment(distributionDate).format('YYYY-MM-DD HH:MM:ss'),
      note,
      source,
      sourceURL,
      ...(expertId && { expertId: expertId.value }),
      ...(tag && tag.length > 0 && { tag }),
    },
    releaseConfig: {
      zoneId,
      ...(zoneIds && zoneIds.length > 0 && { zoneIds: zoneIds.join(',') }),
      ...(topicId && { topicId: topicId.value }),
      displayPosition,
    },
    royaltyConfig,
    ...(!isEmpty(newsRelation) && { newsRelation }),
    ...(!isEmpty(newsRelationSpecial) && { newsRelationSpecial }),
  };
}

export function reloadNews(isReload) {
  return { type: actions.RELOAD_NEWS, isReload };
}

export function getNews(
  status = null,
  zoneId = null,
  keyword = null,
  from = null,
  to = null,
  author = null,
  penName = null,
  pageSize = 10,
  pageIndex = 1,
) {
  const queryString = {
    ...(!isEmpty(status) && { status }),
    ...(!isEmpty(zoneId) && zoneId !== -1 && { zoneId }),
    ...(!isEmpty(keyword) && { keyword }),
    ...(!isEmpty(from) && {
      from: moment(from, 'DD/MM/YYYY').format('YYYY/MM/DD'),
    }),
    ...(!isEmpty(to) && { to: moment(to, 'DD/MM/YYYY').format('YYYY/MM/DD') }),
    ...(!isEmpty(author) && { author }),
    ...(!isEmpty(penName) && { penName }),
    pageSize,
    pageIndex,
  };
  return async (dispatch) => {
    dispatch(startAppLoading('Đang lấy dữ liệu ...'));
    const response = await request.get(
      'news',
      removeParamsForServer(queryString, [], null),
    );
    const { responseCode, reply, total } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({ type: actions.GET_NEWS_RECEIVED, reply, total });
    }
    dispatch(stopAppLoading());
  };
}

export function getNewsStatistic() {
  return async (dispatch) => {
    const response = await request.get('news/statistic');
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({ type: actions.GET_NEWS_STATISTIC_RECEIVED, reply });
    }
  };
}

export function updateNewsStatus(encryptId, currentStatus, nextStatus) {
  return async (dispatch) => {
    const response = await request.post('news/update_status', {
      encryptId,
      status: nextStatus.toString(),
    });
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({
        type: actions.UPDATE_NEWS_STATUS_SUCCESS,
        reply,
      });
      dispatch(getNewsStatistic());
      dispatch(getNews(currentStatus));
    }
  };
}

export function updateNewsInUsing(encryptId, isEdit) {
  return async (dispatch) => {
    const response = await request.post('news/update_in_using', {
      encryptId,
      isEdit,
    });
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({
        type: actions.UPDATE_NEWS_IN_USING_SUCCESS,
      });
      return true;
    }
    return false;
  };
}

export function getNewsRelation(
  zoneId = null,
  keyword = null,
  pageSize = 5,
  pageIndex = 1,
) {
  const queryString = {
    ...(!isEmpty(zoneId) && zoneId !== -1 && { zoneId }),
    ...(!isEmpty(keyword) && { keyword }),
    pageSize,
    pageIndex,
  };
  return async (dispatch) => {
    const response = await request.get(
      'news',
      removeParamsForServer(queryString, [], null),
    );
    const { responseCode, reply, total } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({ type: actions.GET_NEWS_RELATION_RECEIVED, reply, total });
    }
  };
}

export function saveNews(news) {
  return async (dispatch) => {
    dispatch(startAppLoading('Đang xử lý ...'));
    const response = await request.post('news/save', formatRequestData(news));
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({ type: actions.SAVE_NEWS_SUCCESS, reply });
      dispatch(stopAppLoading());
      return reply;
    }
    dispatch(stopAppLoading());
    return 0;
  };
}

export function showAddNewsDrawer(id = null, isDisable = false) {
  return async (dispatch) => {
    let detail = initDetail;
    let tag = [];
    let expert = [];
    let topic = [];
    let royalty = [];
    const newsRelationChose = { ...initNewsRelationChose };
    if (id !== null) {
      dispatch(startAppLoading('Đang lấy dữ liệu ...'));
      const response = await request.get(`news/${id}`);
      const { responseCode, reply } = response.data;
      const {
        detail: detailReply,
        tag: tagReply,
        expert: expertReply,
        topic: topicReply,
        royalty: royaltyReply,
        newsRelation,
        newsRelationSpecial,
      } = reply;
      if (responseCode === EnumResponseCode.SUCCESS.id) {
        detail = detailReply;
        tag = tagReply;
        expert = expertReply;
        topic = topicReply;
        royalty = royaltyReply;
        newsRelationChose.relationItems = newsRelation;
        newsRelationChose.relationSpecialItems = newsRelationSpecial;
      }
      dispatch(stopAppLoading());
    }
    dispatch({
      type: actions.ADD_NEWS_DRAWER_SHOW,
      isDisable,
      detail,
      tag,
      expert,
      topic,
      royalty,
      newsRelationChose,
    });
  };
}

export function closeAddNewsDrawer() {
  return (dispatch) => {
    dispatch({
      type: actions.ADD_NEWS_DRAWER_CLOSE,
      detail: { ...initDetail },
      newsRelationChose: { ...initNewsRelationChose },
    });
  };
}

export function showCompareVersionDrawer(id) {
  return async (dispatch) => {
    const response = await request.get(`news/${id}/version`);
    const { responseCode, reply } = response.data;
    if (reply && reply.length > 0) {
      if (responseCode === EnumResponseCode.SUCCESS.id) {
        dispatch({
          type: actions.COMPARE_VERSION_SHOW,
          newsVersion: reply,
          currentEncryptId: id,
        });
      }
      return 'success';
    }
    return 'error';
  };
}

export function closeCompareVersionDrawer() {
  return (dispatch) => {
    dispatch({
      type: actions.COMPARE_VERSION_CLOSE,
      newsVersion: null,
    });
  };
}

export function getCompareVersionById(encryptId, id) {
  return async () => {
    const response = await request.get(`news/${encryptId}/version/${id}`);
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      return reply;
    }
    return null;
  };
}
