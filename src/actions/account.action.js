import { actions, EnumResponseCode } from '../constants';
import { isEmpty, removeParamsForServer } from '../utils';
import request from '../utils/request';
import { startAppLoading, stopAppLoading } from './global.action';
import { commonConst } from '../constants/constants';

function formatAccountPermissionMap(permissionZoneMap, accountId) {
  const accountPermissionMaps = [];
  permissionZoneMap.forEach((value, key, map) => {
    const accountPermissionMap = {
      accountId,
      permissionId: key,
      zoneIds: value.join(','),
    };
    accountPermissionMaps.push(accountPermissionMap);
  });
  return accountPermissionMaps;
}

export function getAccounts(
  status = null,
  username = null,
  fullname = null,
  email = null,
  mobile = null,
  pageSize = 10,
  pageIndex = 1,
) {
  const queryString = {
    ...(!isEmpty(status) && status !== -1 && { status }),
    ...(!isEmpty(username) && { username }),
    ...(!isEmpty(fullname) && { fullname }),
    ...(!isEmpty(mobile) && { mobile }),
    ...(!isEmpty(email) && { email }),
    pageSize,
    pageIndex,
  };
  return async (dispatch) => {
    dispatch(startAppLoading(commonConst.defaultGlobalLoadingTitle));
    const response = await request.get(
      'accounts',
      removeParamsForServer(queryString, [], null),
    );
    const { responseCode, reply, total } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({ type: actions.GET_ACCOUNT_RECEIVED, reply, total });
      dispatch(stopAppLoading());
    }
  };
}

export function reloadAccount(isReload) {
  return { type: actions.RELOAD_ACCOUNT, isReload };
}

export function addEditProfileOpen(encryptId) {
  return async (dispatch) => {
    if (!isEmpty(encryptId)) {
      dispatch(startAppLoading(commonConst.defaultGlobalLoadingTitle));
      const response = await request.post('accounts/get_by_id', {
        encryptId,
      });
      const { responseCode, reply } = response.data;
      if (responseCode === EnumResponseCode.SUCCESS.id) {
        dispatch({ type: actions.ADD_EDIT_PROFILE_MODAL_OPEN, reply });
        dispatch(stopAppLoading());
      }
    } else {
      dispatch({
        type: actions.ADD_EDIT_PROFILE_MODAL_OPEN,
        reply: { detail: { id: -1, birthDay: null } },
      });
    }
  };
}

export function addEditProfileClose() {
  return async (dispatch) => {
    dispatch({ type: actions.ADD_EDIT_PROFILE_MODAL_CLOSE });
  };
}

export function getAccountPermission(accountId) {
  return async (dispatch) => {
    dispatch(startAppLoading(commonConst.defaultGlobalLoadingTitle));
    const response = await request.post('permissions/get_by_account_id', {
      accountId,
    });
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch(stopAppLoading());
      dispatch({ type: actions.GET_ACCOUNT_PERMISSION_RECEIVED, reply });
    }
  };
}

export function approvePermission(permissionGroupId) {
  return async (dispatch) => {
    dispatch(startAppLoading(commonConst.defaultGlobalLoadingTitle));
    const response = await request.get(
      `permission/groups/${permissionGroupId}/config_permission`,
    );
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch(stopAppLoading());
      dispatch({
        type: actions.APPROVE_PERMISSION,
        reply,
      });
    }
  };
}

export function accountPermissionSave(permissionZoneMap, accountId) {
  return async (dispatch) => {
    dispatch(startAppLoading(commonConst.defaultGlobalLoadingTitle));
    const accountPermissionMaps = formatAccountPermissionMap(
      permissionZoneMap,
      accountId,
    );
    const response = await request.post(
      'accounts/permission/save',
      { accountPermissionMaps, accountId },
    );
    const { responseCode } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({
        type: actions.ACCOUNT_PERMISSION_SAVE_SUCCESS,
      });
      dispatch(stopAppLoading());
      return true;
    }
    dispatch(stopAppLoading());
    return false;
  };
}

export function getAccountPermissionGroup() {
  const queryString = {
    pageSize: 1000,
    pageIndex: 1,
  };
  return async (dispatch) => {
    const response = await request.get(
      'permission/groups',
      removeParamsForServer(queryString, [], null),
    );
    const { responseCode, reply, total } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({
        type: actions.GET_ACCOUNT_PERMISSION_GROUP_RECEIVED,
        reply,
        total,
      });
    }
  };
}

export function addEditPermissionOpen(accountId) {
  return async (dispatch) => {
    dispatch(startAppLoading(commonConst.defaultGlobalLoadingTitle));

    Promise.all([
      dispatch(getAccountPermission(accountId)),
      dispatch(getAccountPermissionGroup()),
    ]).then(() => {
      dispatch({
        type: actions.ADD_EDIT_PERMISSION_MODAL_OPEN,
        accountId,
      });
      dispatch(stopAppLoading());
    });
  };
}

export function addEditPermissionClose() {
  return async (dispatch) => {
    dispatch({ type: actions.ADD_EDIT_PERMISSION_MODAL_CLOSE });
  };
}

export function resetPasswordOpen() {
  return {
    type: actions.RESET_PASSWORD_MODAL_OPEN,
  };
}

export function resetPasswordClose() {
  return {
    type: actions.RESET_PASSWORD_MODAL_CLOSE,
  };
}

export function saveAccount(account) {
  return async (dispatch) => {
    dispatch(startAppLoading(commonConst.defaultGlobalLoadingTitle));
    const response = await request.post('accounts/save', account);
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({ type: actions.SAVE_ACCOUNT_SUCCESS, reply });
      dispatch(stopAppLoading());
      return reply;
    }
    dispatch(stopAppLoading());
    return 0;
  };
}

export function resetPassword(encryptId, password) {
  return async (dispatch) => {
    dispatch(startAppLoading(commonConst.defaultGlobalLoadingTitle));
    const response = await request.post('accounts/reset_password', {
      encryptId,
      password,
    });
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({ type: actions.RESET_PASSWORD_SUCCESS, reply });
      dispatch(stopAppLoading());
      return reply;
    }
    dispatch(stopAppLoading());
    return 0;
  };
}
