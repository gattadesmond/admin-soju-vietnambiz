import moment from 'moment';
import { actions, EnumNewsRelationModal, EnumNewsLogTypeAction } from '../constants';
import request from '../utils/request';
import { EnumResponseCode } from '../constants/enum';
import { isEmpty, removeParamsForServer } from '../utils';

export function getNewsLog(
  logTypeId = null,
  logTypeActionId = null,
  appId = null,
  from = null,
  to = null,
  pageSize = 10,
  pageIndex = 1,
) {
  const queryString = {
    logTypeId,
    logTypeActionId,
    ...(!isEmpty(appId) && { appId }),
    ...(!isEmpty(from) && {
      from: moment(from, 'DD/MM/YYYY').format('YYYY/MM/DD'),
    }),
    ...(!isEmpty(to) && { to: moment(to, 'DD/MM/YYYY').format('YYYY/MM/DD') }),
    pageSize,
    pageIndex,
  };
  return async (dispatch) => {
    const response = await request.get(
      'api/logaction/get_log',
      removeParamsForServer(queryString, [], null),
    );
    const { responseCode, reply, total } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({ type: actions.GET_NEWS_LOG_RECEIVED, reply, total, pageSize });
    }
  };
}
export function getLogTypeAction(
  logTypeId,
) {
  return (dispatch) => {
    const dataNewsLogTypeAction = Object.values(EnumNewsLogTypeAction);
    const data = dataNewsLogTypeAction.filter((x) => x.parentid === logTypeId);
    const total = data.length;
    dispatch({ type: actions.GET_LOG_ACTION_BY_PARENT, data, total });
  };
}
export function resetNewsLog() {
  return (dispatch) => {
    dispatch({ type: actions.RESET_NEWS_LOG_RECEIVED });
  };
}