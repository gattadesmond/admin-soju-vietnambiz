/* eslint-disable */
import request from '../utils/request';
import { actions, EnumResponseCode } from '../constants';

export function autocompleteTag(keyword) {
  return async (dispatch) => {
    const response = await request.get('tag', {keyword});
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({ type: actions.GET_TAG_SUGGEST_RECEIVED, reply });
      return response;
    }
  };
}
