import { isEmpty, removeParamsForServer } from '../utils';
import request from '../utils/request';
import { actions, EnumResponseCode } from '../constants';

function formatPermissionMap(permissionZoneMap, permissionGroupId) {
  const permissionMaps = [];
  permissionZoneMap.forEach((value, key, map) => {
    const permissionMap = {
      permissionGroupId,
      permissionId: key,
      zoneIds: value.join(','),
    };
    permissionMaps.push(permissionMap);
  });
  return permissionMaps;
}

export function getPermissionGroup(
  isActive = null,
  name = null,
  pageSize = 10,
  pageIndex = 1,
) {
  const queryString = {
    ...(!isEmpty(isActive) && isActive !== -1 && { isActive }),
    ...(!isEmpty(name) && { name }),
    pageSize,
    pageIndex,
  };
  return async (dispatch) => {
    const response = await request.get(
      'permission/groups',
      removeParamsForServer(queryString, [], null),
    );
    const { responseCode, reply, total } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({ type: actions.GET_PERMISSION_GROUP_RECEIVED, reply, total });
    }
  };
}

export function configPermissionOpen(permissionGroupId) {
  return async (dispatch) => {
    const response = await request.get(
      `permission/groups/${permissionGroupId}/config_permission`,
    );
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({
        type: actions.CONFIG_PERMISSION_MODAL_OPEN,
        reply,
        permissionGroupId,
      });
    }
  };
}

export function configPermissionClose() {
  return async (dispatch) => {
    dispatch({ type: actions.CONFIG_PERMISSION_MODAL_CLOSE });
  };
}

export function getConfigPermission(permissionGroupId) {
  return async (dispatch) => {
    const response = await request.get(
      `permission/groups/${permissionGroupId}/config_permission`,
    );
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({
        type: actions.GET_CONFIG_PERMISSION_RECEIVED,
        reply,
        permissionGroupId,
      });
    }
  };
}

export function configAccountOpen(permissionGroupId) {
  return async (dispatch) => {
    const response = await request.get(
      `permission/groups/${permissionGroupId}/config_account`,
    );
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({
        type: actions.CONFIG_ACCOUNT_MODAL_OPEN,
        reply,
        permissionGroupId,
      });
    }
  };
}

export function configAccountClose() {
  return async (dispatch) => {
    dispatch({ type: actions.CONFIG_ACCOUNT_MODAL_CLOSE });
  };
}

export function configPermissionSave(permissionZoneMap, permissionGroupId) {
  return async (dispatch) => {
    const permissionMaps = formatPermissionMap(
      permissionZoneMap,
      permissionGroupId,
    );
    const response = await request.post(
      `permission/groups/${permissionGroupId}/config_permission/save`,
      { permissionMaps, permissionGroupId },
    );
    const { responseCode, reply } = response.data;
    if (responseCode === EnumResponseCode.SUCCESS.id) {
      dispatch({
        type: actions.CONFIG_PERMISSION_SAVE_SUCCESS,
      });
      return true;
    }
    return false;
  };
}

