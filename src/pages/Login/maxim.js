const maxim = [
  {
    content: 'Mỗi khi có ý định từ bỏ, hãy nghĩ đến lí do mà bạn đã bắt đầu',
    source: 'Khuyết danh',
  },
  {
    content:
      'Một suy nghĩ tiêu cực sẽ không bao giờ mang lại một cuộc sống tích cực',
    source: 'Khuyết danh',
  },
  {
    content: 'Thành công lớn nhất là đứng dậy sau mỗi lần vấp ngã',
    source: 'Khuyết danh',
  },
  {
    content:
      'Nếu kế hoạch A không dùng được, thì ta hãy còn tận 28 chữ cái cơ mà',
    source: 'Khuyết danh',
  },
  {
    content: 'Thước đo của cuộc đời không phải thời gian mà là sự cống hiến',
    source: 'Khuyết danh',
  },
  {
    content:
      'Lo lắng không làm cho những điều tồi tệ ngừng xảy ra, mà nó chỉ làm cho những điều tốt lành ngừng lại',
    source: 'Khuyết danh',
  },
  {
    content:
      'Người tiến xa nhất là người sẵn sàng hành động và biết nắm bắt cơ hội',
    source: 'Khuyết danh',
  },
  {
    content: 'Chẳng bao giờ có cái gọi là "thời điểm thích hợp" đâu',
    source: 'Kevin Plank - CEO của Under Amour',
  },
  {
    content: 'Nghị lực và bền bỉ có thể chinh phục mọi thứ',
    source: 'Benjamin Franklin',
  },
  {
    content: 'Người ta khá sợ hãi thất bại, vì vậy họ chẳng thành công',
    source: 'Tỷ phú Tadashi Yanai',
  },
];

export default maxim;
