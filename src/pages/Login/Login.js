import React, { Component } from 'react';
import { Form, Input, Button, Checkbox } from 'antd';
import { sample } from 'lodash';
import withRouter from 'react-router/withRouter';
import { connect } from 'react-redux';

import { UserOutlined, LockOutlined } from '@ant-design/icons';
import imgLogoColor from '../../assets/static/vietnambiz-logo-color.png';
import { login } from '../../actions/auth.action';
import { isEmpty } from '../../utils';
import maxim from './maxim';

class Login extends Component {
  constructor(props) {
    super(props);
    const maximItem = sample(maxim);
    const { content, source } = maximItem || {};
    this.state = {
      content,
      source,
      errorMsg: null,
    };
  }
  handleLoginClick = async (values) => {
    const { loginConnect, history } = this.props;
    const { username, password } = values;
    const response = await loginConnect(username, password, history);
    if (response.status === 'fail') {
      this.setState({ errorMsg: response.errorMsg });
    } else {
      history.push('/dashboard');
    }
  };

  render() {
    const { content, source, errorMsg } = this.state;

    return (
      <div className="login-container">
        <div className="login-grid">
          <div className="login-grid-left">
            <div className="mb-4">
              <img src={imgLogoColor} alt="VietnamBiz" width="120" />
            </div>

            <h3 className="tx-color-01 mg-b-5">Chào bạn</h3>
            <p className="tx-color-03 tx-16 mg-b-20">
              Mời bạn đăng nhập thông tin
            </p>

            <Form
              name="normal_login"
              className="login-form"
              initialValues={{
                remember: true,
                size: 'large',
              }}
              size="large"
              layout="vertical"
              onFinish={this.handleLoginClick}
            >
              <Form.Item
                label="Tên đăng nhập"
                name="username"
                rules={[
                  {
                    required: true,
                    message: 'Please input your Username!',
                  },
                ]}
              >
                <Input
                  prefix={<UserOutlined className="site-form-item-icon" />}
                  placeholder="Username"
                />
              </Form.Item>
              <Form.Item
                label="Mật khẩu"
                name="password"
                rules={[
                  {
                    required: true,
                    message: 'Please input your Password!',
                  },
                ]}
              >
                <Input.Password
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  placeholder="Password"
                />
              </Form.Item>
              <Form.Item>
                <Form.Item name="remember" valuePropName="checked" noStyle>
                  <Checkbox>Ghi nhớ mật khẩu</Checkbox>
                  {!isEmpty(errorMsg) && (
                    <div style={{ marginTop: '8px' }}>
                      <span style={{ color: 'red' }}>{errorMsg}</span>
                    </div>
                  )}
                </Form.Item>
              </Form.Item>

              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="login-form-button"
                >
                  Đăng nhập
                </Button>
              </Form.Item>
            </Form>
          </div>

          <div
            className="login-grid-right"
            style={{
              backgroundImage:
                'url(https://a0.muscache.com/im/pictures/lombard/MtExperience-1183442-pending/original/052da99e-3aa2-4c2f-a76a-be1ac01ec662.jpeg?aki_policy=exp_xl)',
            }}
          >
            <div className="login-quote-bg" />
            <div className="login-quote">
              <div className="login-quote-text">{content}</div>
              <div className="login-quote-source">{source}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = {
  loginConnect: login,
};

export default withRouter(connect(null, mapDispatchToProps)(Login));
