import React from 'react';
import { Input, Button, Select, DatePicker, AutoComplete } from 'antd';
import moment from 'moment';
import 'moment/locale/vi';
import { TIME_TO_QUERY_AFTER_STOP_TYPING } from '../../../../constants';

const { RangePicker } = DatePicker;

const dateFormat = 'DD/MM/YYYY';

const NewsFilter = (props) => {
  const {
    zoneOptions,
    filterState,
    onZoneChange,
    onKeywordChange,
    onRangePickerChange,
    onAuthorChange,
    onPenNameChange,
    onSearchClick,
  } = props;
  const { zoneId, keyword, author, penName } = filterState || {};
  const dataSource = ['Burns Bay Road', 'Downing Street', 'Wall Street'];

  return (
    <div className="news-filter ">
      <div className=" card card-body py-1 px-3">
        <div className="d-flex align-items-end  flex-nowrap">
          <div className="filter-form">
            <div className="filter-item">
              <div className="filter-item-label">Theo chuyên mục</div>
              <Select
                value={zoneId}
                style={{ width: 190 }}
                onChange={onZoneChange}
              >
                {zoneOptions}
              </Select>
            </div>
            <div className="filter-item">
              <div className="filter-item-label">Theo từ khoá</div>
              <Input
                placeholder="Nhập từ khoá"
                value={keyword}
                style={{ width: 160 }}
                onChange={(e) =>
                  setTimeout(
                    onKeywordChange(e.target.value),
                    TIME_TO_QUERY_AFTER_STOP_TYPING,
                  )
                }
              />
            </div>
            <div className="filter-item">
              <div className="filter-item-label">Theo ngày đăng</div>
              <RangePicker
                ranges={{
                  Today: [moment(), moment()],
                  'This Month': [
                    moment().startOf('month'),
                    moment().endOf('month'),
                  ],
                }}
                format={dateFormat}
                style={{ width: 210 }}
                onChange={onRangePickerChange}
              />
            </div>
            <div className="filter-item">
              <div className="filter-item-label">Theo tên tác giả</div>
              <AutoComplete
                style={{ width: 140 }}
                dataSource={dataSource}
                value={author}
                placeholder="Tên tác giả"
                filterOption={(inputValue, option) =>
                  option.props.children
                    .toUpperCase()
                    .indexOf(inputValue.toUpperCase()) !== -1
                }
                onChange={onAuthorChange}
              />
            </div>
            <div className="filter-item">
              <div className="filter-item-label">Theo tên người viết</div>
              <AutoComplete
                style={{ width: 140 }}
                dataSource={dataSource}
                value={penName}
                placeholder="Tên người viết"
                filterOption={(inputValue, option) =>
                  option.props.children
                    .toUpperCase()
                    .indexOf(inputValue.toUpperCase()) !== -1
                }
                onChange={onPenNameChange}
              />
            </div>
            <div className="filter-item">
              <div className="filter-item-label">&nbsp;</div>
              <Button type="primary" onClick={() => onSearchClick()}>
                Lọc
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewsFilter;
