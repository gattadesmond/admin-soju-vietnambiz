import React from 'react';
import { Select, Button } from 'antd';

import Filter from '../NewsFilter/NewsFilter';
import PostsList from '../PostsList/PostsList';

const { Option } = Select;
const children = [];
// eslint-disable-next-line no-plusplus
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}

const MainContent = (props) => {
  const {
    news,
    userName,
    total,
    zoneOptions,
    filterState,
    onAddNewsClick,
    onPostItemClick,
    onZoneChange,
    onKeywordChange,
    onRangePickerChange,
    onAuthorChange,
    onPenNameChange,
    onPageSizeChange,
    onPageIndexChange,
    onSearchClick,
  } = props;
  return (
    <div className="filemgr-content">
      <div className="filemgr-content-body ">
        <div className="d-none align-items-center mb-2 ">
          <div className="flex-grow-1">
            <h4 className="mb-0">Bài chờ biên tập</h4>
          </div>
          <div className="ml-3 ">
            <Button type="primary" icon="edit" onClick={onAddNewsClick}>
              VIẾT BÀI MỚI
            </Button>
          </div>
        </div>
        <Filter
          zoneOptions={zoneOptions}
          filterState={filterState}
          onZoneChange={onZoneChange}
          onKeywordChange={onKeywordChange}
          onRangePickerChange={onRangePickerChange}
          onAuthorChange={onAuthorChange}
          onPenNameChange={onPenNameChange}
          onSearchClick={onSearchClick}
        />
        <PostsList
          news={news}
          userName={userName}
          total={total}
          filterState={filterState}
          onPostItemClick={onPostItemClick}
          onPageSizeChange={onPageSizeChange}
          onPageIndexChange={onPageIndexChange}
        />
      </div>
    </div>
  );
};

export default MainContent;
