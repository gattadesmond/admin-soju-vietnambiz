import React from 'react';
import { List } from 'antd';
import PostItem from './components/PostItem';
import { numberWithCommas } from '../../../../utils';

const PostsList = (props) => {
  const {
    filterState,
    news,
    userName,
    total,
    onPageSizeChange,
    onPageIndexChange,
    onPostItemClick,
  } = props;
  const { pageSize } = filterState || {};
  return (
    <List
      itemLayout="vertical"
      pagination={{
        position: 'bottom',
        size: 'small',
        pageSize,
        total,
        showTotal: (total) => `Total ${numberWithCommas(total)} items`,
        showSizeChanger: true,
        showQuickJumper: true,
        onShowSizeChange: onPageSizeChange,
        onChange: onPageIndexChange,
      }}
      dataSource={news}
      renderItem={(item, index) => (
        <div className="mb-2 card-post-item">
          <PostItem
            key={index}
            item={item}
            userName={userName}
            onItemClick={() => onPostItemClick(item.encryptId)}
          />
        </div>
      )}
    />
  );
};

export default PostsList;
