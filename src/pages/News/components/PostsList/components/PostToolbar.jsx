import React from 'react';
import { generateToolbar } from '../../../routines';

const PostToolbar = (props) => {
  const {
    isShow,
    isOwner,
    status,
    onNewsEdit,
    onNewsSend,
    onNewsReturn,
    onNewsRemove,
    onNewsDetailView,
    onNewsRelease,
    onNewsTakenDown,
    onCompareVersion,
    onNewsWithdraw,
    onNewsReceived,
    onNewsUnlock,
    onNewsRestore,
  } = props || {};
  return (
    <div className={`postItem-action ${isShow ? 'is-show' : ''}`}>
      <div
        className="btn-group btn-group-sm shadow-sm"
        role="group"
        aria-label="Basic example"
      >
        {generateToolbar(
          status,
          isOwner,
          onNewsEdit,
          onNewsSend,
          onNewsWithdraw,
          onNewsReturn,
          onNewsRemove,
          onNewsDetailView,
          onNewsRelease,
          onNewsTakenDown,
          onCompareVersion,
          onNewsReceived,
          onNewsUnlock,
          onNewsRestore,
        )}
      </div>
    </div>
  );
};

export default PostToolbar;
