import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { Tag, Modal, Input } from 'antd';
import moment from 'moment';
import { ExclamationCircleOutlined } from '@ant-design/icons';

import PostToolbar from './PostToolbar';
import { EnumNewsSidebar } from '../../../../../constants';
import {
  showAddNewsDrawer,
  showCompareVersionDrawer,
  updateNewsInUsing,
  updateNewsStatus,
} from '../../../../../actions/news.action';
import { isEmpty } from '../../../../../utils';

import { __IMAGE_HOST__ } from 'global.js';

const { confirm } = Modal;
const { TextArea } = Input;

class PostItem extends Component {
  state = {
    isToolbarShow: false,
  };

  handleToolbarShow = (isShow) => {
    this.setState({
      isToolbarShow: isShow,
    });
  };

  handleNewsEdit = async (encryptId, currentStatus) => {
    const isEdit = true;
    const { showAddNewsDrawerConnect, updateNewsInUsingConnect } = this.props;
    if (
      [
        EnumNewsSidebar.WAITING_EDITING_POST.id,
        EnumNewsSidebar.WAITING_RELEASE_POST.id,
      ].includes(currentStatus)
    ) {
      confirm({
        title: 'Xác nhận',
        icon: <ExclamationCircleOutlined />,
        content:
          'Bài sẽ tự động chuyển trạng thái nhận khi bạn thực hiện sửa bài. Bạn có muốn sửa không?',
        onOk() {
          const callFunction = async () => {
            const result = await updateNewsInUsingConnect(encryptId, isEdit);
            if (result) {
              showAddNewsDrawerConnect(encryptId, false);
            }
          };
          callFunction();
        },
      });
    } else {
      const result = await updateNewsInUsingConnect(encryptId, isEdit);
      if (result) {
        showAddNewsDrawerConnect(encryptId, false);
      }
    }
  };

  handleNewsSend = (encryptId, currentStatus) => {
    const { updateNewsStatusConnect } = this.props;
    confirm({
      title: 'Xác nhận',
      icon: <ExclamationCircleOutlined />,
      content: 'Bạn muốn gửi bài này lên?',
      onOk() {
        if (currentStatus === EnumNewsSidebar.TEMP_POST.id) {
          updateNewsStatusConnect(
            encryptId,
            currentStatus,
            EnumNewsSidebar.WAITING_EDITING_POST.id,
          );
        } else if (currentStatus === EnumNewsSidebar.RECEIVED_EDITING_POST.id) {
          updateNewsStatusConnect(
            encryptId,
            currentStatus,
            EnumNewsSidebar.WAITING_RELEASE_POST.id,
          );
        }
      },
    });
  };

  handleNewsRelease = (encryptId) => {
    const action = () => {
      this.handleNewsEdit(encryptId);
    };
    confirm({
      title: 'Xác nhận',
      icon: <ExclamationCircleOutlined />,
      content:
        'Cần vào form soạn bài để kiểm tra thông tin xuất bản. Chuyển sang form soạn bài ngay?',
      onOk() {
        action();
      },
    });
  };

  handleNewsTakenDown = (encryptId, currentStatus) => {
    const { updateNewsStatusConnect } = this.props;
    confirm({
      title: 'Xác nhận',
      icon: <ExclamationCircleOutlined />,
      content: 'Bạn có chắc chắn muốn gỡ bài viết này xuống?',
      onOk() {
        updateNewsStatusConnect(
          encryptId,
          currentStatus,
          EnumNewsSidebar.TAKEN_DOWN_POST.id,
        );
      },
    });
  };

  handleNewsDetailView = (encryptId) => {
    const { showAddNewsDrawerConnect } = this.props;
    showAddNewsDrawerConnect(encryptId, true);
  };

  handleNewsRemove = (encryptId, currentStatus) => {
    const { updateNewsStatusConnect } = this.props;
    updateNewsStatusConnect(
      encryptId,
      currentStatus,
      EnumNewsSidebar.REMOVED_POST.id,
    );
  };

  handleNewsReceived = (encryptId, currentStatus) => {
    const { updateNewsStatusConnect } = this.props;
    confirm({
      title: 'Xác nhận',
      icon: <ExclamationCircleOutlined />,
      content: 'Bạn muốn nhận về bài này?',
      onOk() {
        if (currentStatus === EnumNewsSidebar.WAITING_EDITING_POST.id) {
          updateNewsStatusConnect(
            encryptId,
            currentStatus,
            EnumNewsSidebar.RECEIVED_EDITING_POST.id,
          );
        } else if (currentStatus === EnumNewsSidebar.WAITING_RELEASE_POST.id) {
          updateNewsStatusConnect(
            encryptId,
            currentStatus,
            EnumNewsSidebar.RECEIVED_RELEASE_POST.id,
          );
        }
      },
    });
  };

  handleNewsUnlock = (encryptId, currentStatus) => {
    const { updateNewsStatusConnect, updateNewsInUsingConnect } = this.props;
    confirm({
      title: 'Xác nhận',
      icon: <ExclamationCircleOutlined />,
      content: 'Bạn có chắc chắn muốn nhả bài viết ra?',
      onOk() {
        const callFunction = async () => {
          const isEdit = false;
          const result = await updateNewsInUsingConnect(encryptId, isEdit);
          if (result) {
            if (currentStatus === EnumNewsSidebar.RECEIVED_EDITING_POST.id) {
              updateNewsStatusConnect(
                encryptId,
                currentStatus,
                EnumNewsSidebar.WAITING_EDITING_POST.id,
              );
            } else if (
              currentStatus === EnumNewsSidebar.RECEIVED_RELEASE_POST.id
            ) {
              updateNewsStatusConnect(
                encryptId,
                currentStatus,
                EnumNewsSidebar.WAITING_RELEASE_POST.id,
              );
            }
          }
        };
        callFunction();
      },
    });
  };

  handleNewsReturn = (encryptId, currentStatus) => {
    const { updateNewsStatusConnect } = this.props;
    confirm({
      title: 'Trả lại bài viết',
      icon: <ExclamationCircleOutlined />,
      content: <TextArea rows={4} placeholder="Lời nhắn trả lại bài viết" />,
      onOk() {
        if (currentStatus === EnumNewsSidebar.WAITING_EDITING_POST.id) {
          updateNewsStatusConnect(
            encryptId,
            currentStatus,
            updateNewsStatusConnect(
              encryptId,
              currentStatus,
              EnumNewsSidebar.TEMP_POST.id,
            ),
          );
        } else if (currentStatus === EnumNewsSidebar.WAITING_RELEASE_POST.id) {
          updateNewsStatusConnect(
            encryptId,
            currentStatus,
            EnumNewsSidebar.RECEIVED_EDITING_POST.id,
          );
        } else if (currentStatus === EnumNewsSidebar.RECEIVED_EDITING_POST.id) {
          updateNewsStatusConnect(
            encryptId,
            currentStatus,
            EnumNewsSidebar.WAITING_EDITING_POST.id,
          );
        } else if (currentStatus === EnumNewsSidebar.RECEIVED_RELEASE_POST.id) {
          updateNewsStatusConnect(
            encryptId,
            currentStatus,
            EnumNewsSidebar.RECEIVED_EDITING_POST.id,
          );
        }
      },
    });
  };

  handleNewsWithdraw = (encryptId, currentStatus) => {
    const { updateNewsStatusConnect } = this.props;
    updateNewsStatusConnect(
      encryptId,
      currentStatus,
      EnumNewsSidebar.TEMP_POST.id,
    );
  };

  handleCompareVersion = async (encryptId) => {
    const { showCompareVersionDrawerConnect } = this.props;
    const result = await showCompareVersionDrawerConnect(encryptId);
    if (result === 'error') {
      Modal.info({
        title: 'Thông báo',
        content: (
          <div>Không thể so sánh (bài viết chỉ có 1 phiên bản duy nhất)</div>
        ),
        onOk() {},
      });
    }
  };

  handleNewsRestore = (encryptId, currentStatus) => {
    const { updateNewsStatusConnect } = this.props;
    confirm({
      title: 'Xác nhận',
      icon: <ExclamationCircleOutlined />,
      content: 'Bạn có chắc chắn muốn phục hồi viết này?',
      onOk() {
        updateNewsStatusConnect(
          encryptId,
          currentStatus,
          EnumNewsSidebar.RECEIVED_RELEASE_POST.id,
        );
      },
    });
  };

  render() {
    const { item, userName } = this.props;
    const {
      avatar,
      title,
      createdBy,
      publishedBy,
      status,
      zoneName,
      distributionDate,
      author,
      encryptId,
      note,
      royaltyBy,
    } = item || {};
    const avatarSrc = `${__IMAGE_HOST__}${avatar}`;
    const { isToolbarShow } = this.state;
    const { onItemClick } = this.props;
    const publishDate = moment(distributionDate).format('DD/MM/YYYY - HH:mm');
    const isOwner = createdBy === userName;
    return (
      <div
        className="card card-file card-post"
        onMouseOver={() => this.handleToolbarShow(true)}
        onMouseOut={() => this.handleToolbarShow(false)}
      >
        <div className="postItem-flex">
          <div className="postItem-thumb">
            <img className="rounded" width={70} alt="logo" src={avatarSrc} />
          </div>
          <div className="postItem-heading pl-3 pr-3">
            <div className="inside">
              <h5 className="postItem-title" onClick={onItemClick}>
                {title}
              </h5>
              {!isEmpty(note) && (
                <div className="txt-11">
                  <span style={{ color: 'red' }}>{`Ghi chú: ${note}`}</span>
                </div>
              )}
              <div className="txt-11">
                <span>
                  Viết bởi <strong>{createdBy}</strong>.{' '}
                </span>
                {!isEmpty(publishedBy) && (
                  <span>
                    Xuất bản bởi <strong>{publishedBy}</strong>.{' '}
                  </span>
                )}
                {!isEmpty(royaltyBy) && (
                  <span>
                    Chấm nhuận bởi <strong>{royaltyBy}</strong>.{' '}
                  </span>
                )}
              </div>
            </div>
          </div>
          <div className="postItem-cat pl-3">{zoneName}</div>
          <div className="postItem-date pl-3">{publishDate}</div>
          <div className="postItem-author pl-3">{author}</div>
        </div>
        <PostToolbar
          isShow={isToolbarShow}
          isOwner={isOwner}
          status={status}
          onNewsEdit={() => this.handleNewsEdit(encryptId, status)}
          onNewsSend={() => this.handleNewsSend(encryptId, status)}
          onNewsReturn={() => this.handleNewsReturn(encryptId, status)}
          onNewsRemove={() => this.handleNewsRemove(encryptId, status)}
          onNewsDetailView={() => this.handleNewsDetailView(encryptId)}
          onNewsRelease={() => this.handleNewsRelease(encryptId)}
          onNewsTakenDown={() => this.handleNewsTakenDown(encryptId, status)}
          onCompareVersion={() => this.handleCompareVersion(encryptId)}
          onNewsWithdraw={() => this.handleNewsWithdraw(encryptId, status)}
          onNewsReceived={() => this.handleNewsReceived(encryptId, status)}
          onNewsUnlock={() => this.handleNewsUnlock(encryptId, status)}
          onNewsRestore={() => this.handleNewsRestore(encryptId, status)}
        />
      </div>
    );
  }
}

const mapDispatchToProps = {
  updateNewsStatusConnect: updateNewsStatus,
  showAddNewsDrawerConnect: showAddNewsDrawer,
  showCompareVersionDrawerConnect: showCompareVersionDrawer,
  updateNewsInUsingConnect: updateNewsInUsing,
};
export default withRouter(connect(null, mapDispatchToProps)(PostItem));
