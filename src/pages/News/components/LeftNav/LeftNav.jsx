/* eslint-disable jsx-a11y/label-has-for, react/no-array-index-key */
import React, { Component } from 'react';
import { Tooltip } from 'antd';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';

import {
  ProfileOutlined,
  FileExclamationOutlined,
  PauseCircleOutlined,
  SaveOutlined,
  ClockCircleOutlined,
  HddOutlined,
  ExportOutlined,
  WarningOutlined,
  DeleteOutlined,
  ReloadOutlined,
  PlaySquareOutlined,
  EditOutlined,
  FileSyncOutlined,
  FileMarkdownOutlined,
  UserOutlined,
} from '@ant-design/icons';

import { EnumNewsSidebar } from '../../../../constants';
import { showAddNewsDrawer } from '../../../../actions/news.action';
import { isEmpty, isEmptyObject, numberWithCommas } from '../../../../utils';

const Data2 = [
  {
    id: EnumNewsSidebar.EVOLVE_POST.id,
    iconName: <PlaySquareOutlined />,
    title: 'Tường thuật sự kiện',
    count: 0,
  },
  {
    id: EnumNewsSidebar.MAGAZINE_POST.id,
    iconName: <ProfileOutlined />,
    title: 'Bài Magazine',
    count: 0,
  },
];

class LeftNav extends Component {
  static CountNews(statistic, title) {
    let count = 0;
    if (!isEmptyObject(statistic) && !isEmpty(title)) {
      Object.entries(statistic).forEach(([key, value]) => {
        if (key === title.toLowerCase()) {
          count = value;
        }
      });
    }
    return count;
  }

  handleDrawerShow = async () => {
    const { showAddNewsDrawerConnect } = this.props;
    showAddNewsDrawerConnect();
  };
  render() {
    const { menuActive, onSidebarClick, statistic, authReducer } = this.props;
    const { permission } = authReducer || {};
    const permissionUrl = permission.map((x) => x.url);
    const Data = [
      {
        id: EnumNewsSidebar.TEMP_POST.id,
        iconName: <FileExclamationOutlined />,
        title: 'Bài lưu tạm',
        count: LeftNav.CountNews(
          statistic,
          EnumNewsSidebar.TEMP_POST.description,
        ),
        isShow: permissionUrl.includes(EnumNewsSidebar.TEMP_POST.path),
      },
      {
        id: EnumNewsSidebar.WAITING_EDITING_POST.id,
        iconName: <PauseCircleOutlined />,
        title: 'Bài chờ biên tập',
        count: LeftNav.CountNews(
          statistic,
          EnumNewsSidebar.WAITING_EDITING_POST.description,
        ),
        isShow: permissionUrl.includes(
          EnumNewsSidebar.WAITING_EDITING_POST.path,
        ),
      },
      {
        id: EnumNewsSidebar.RECEIVED_EDITING_POST.id,
        iconName: <SaveOutlined />,
        title: 'Bài nhận biên tập',
        count: LeftNav.CountNews(
          statistic,
          EnumNewsSidebar.RECEIVED_EDITING_POST.description,
        ),
        isShow: permissionUrl.includes(
          EnumNewsSidebar.RECEIVED_EDITING_POST.path,
        ),
      },
      {
        id: EnumNewsSidebar.WAITING_RELEASE_POST.id,
        iconName: <ClockCircleOutlined />,
        title: 'Bài chờ xuất bản',
        count: LeftNav.CountNews(
          statistic,
          EnumNewsSidebar.WAITING_RELEASE_POST.description,
        ),
        isShow: permissionUrl.includes(
          EnumNewsSidebar.WAITING_RELEASE_POST.path,
        ),
      },
      {
        id: EnumNewsSidebar.RECEIVED_RELEASE_POST.id,
        iconName: <HddOutlined />,
        title: 'Bài nhận xuất bản',
        count: LeftNav.CountNews(
          statistic,
          EnumNewsSidebar.RECEIVED_RELEASE_POST.description,
        ),
        isShow: permissionUrl.includes(
          EnumNewsSidebar.RECEIVED_RELEASE_POST.path,
        ),
      },
      {
        id: EnumNewsSidebar.RELEASE_POST.id,
        iconName: <ExportOutlined />,
        title: 'Bài đã được xuất bản',
        count: LeftNav.CountNews(
          statistic,
          EnumNewsSidebar.RELEASE_POST.description,
        ),
        isShow: permissionUrl.includes(EnumNewsSidebar.RELEASE_POST.path),
      },
      {
        id: EnumNewsSidebar.MY_RETURN_POST.id,
        iconName: <FileExclamationOutlined />,
        title: 'Bài trả lại tôi',
        count: LeftNav.CountNews(
          statistic,
          EnumNewsSidebar.MY_RETURN_POST.description,
        ),
        isShow: permissionUrl.includes(EnumNewsSidebar.MY_RETURN_POST.path),
      },
      {
        id: EnumNewsSidebar.TAKEN_DOWN_POST.id,
        iconName: <WarningOutlined />,
        title: 'Bài bị gỡ xuống',
        count: LeftNav.CountNews(
          statistic,
          EnumNewsSidebar.TAKEN_DOWN_POST.description,
        ),
        isShow: permissionUrl.includes(EnumNewsSidebar.TAKEN_DOWN_POST.path),
      },
      {
        id: EnumNewsSidebar.REMOVED_POST.id,
        iconName: <DeleteOutlined />,
        title: 'Bài bị xoá',
        count: LeftNav.CountNews(
          statistic,
          EnumNewsSidebar.REMOVED_POST.description,
        ),
        isShow: permissionUrl.includes(EnumNewsSidebar.REMOVED_POST.path),
      },
      {
        id: EnumNewsSidebar.PROCESSING_POST.id,
        iconName: <ReloadOutlined />,
        title: 'Bài đang xử lý',
        count: 0,
        isShow: permissionUrl.includes(EnumNewsSidebar.PROCESSING_POST.path),
      },
    ];
    return (
      <React.Fragment>
        <div className="filemgr-sidebar">
          <div className="filemgr-sidebar-header">
            <div className="file-menu-hori">
              <Tooltip placement="bottom" title="Thêm tin ">
                <span
                  onClick={this.handleDrawerShow}
                  className="menu-hori-item  hori-item-create"
                >
                  <EditOutlined />
                </span>
              </Tooltip>
              <Tooltip placement="bottom" title="Bài tường thuật sự ">
                <span
                  onClick={this.handleDrawerShow}
                  className="menu-hori-item hori-item-live"
                >
                  <FileSyncOutlined />
                </span>
              </Tooltip>
              <Tooltip placement="bottom" title="Bài Magazine">
                <span
                  onClick={this.handleDrawerShow}
                  className="menu-hori-item hori-item-magazine"
                >
                  <FileMarkdownOutlined />
                </span>
              </Tooltip>
              <Tooltip placement="bottom" title="Bài tường thuật bóng đá">
                <span
                  onClick={this.handleDrawerShow}
                  className="menu-hori-item hori-item-soccer"
                >
                  <ProfileOutlined />
                </span>
              </Tooltip>
            </div>
          </div>

          <div className="filemgr-sidebar-body">
            <div className="pd-t-20 pd-b-10 pd-x-10">
              <nav className="nav nav-sidebar tx-13">
                <div
                  className={`nav-link ${
                    menuActive === EnumNewsSidebar.MY_POST.id ? ' active' : ''
                  }`}
                  onClick={() => onSidebarClick(EnumNewsSidebar.MY_POST.id)}
                >
                  <UserOutlined /> <span> Bài của tôi </span>
                </div>
              </nav>
            </div>

            <div className="pd-10">
              <label className="tx-sans tx-uppercase tx-medium tx-10 tx-spacing-1 tx-color-03 pd-l-10">
                Tin bài
              </label>
              <nav className="nav nav-sidebar tx-13">
                {Data.map((item, index) => {
                  const { iconName, title, count, id, isShow } = item;
                  if (isShow) {
                    return (
                      <div
                        key={index}
                        className={`nav-link ${
                          menuActive === id ? ' active' : ''
                        }`}
                        onClick={() => onSidebarClick(id)}
                      >
                        {iconName} <span> {title} </span>
                        {count > 0 ? (
                          <span className="badge">
                            {numberWithCommas(count)}
                          </span>
                        ) : null}
                      </div>
                    );
                  }
                  return null;
                })}
              </nav>
            </div>

            <div className="pd-10">
              <label className="tx-sans tx-uppercase tx-medium tx-10 tx-spacing-1 tx-color-03 pd-l-10">
                Dạng bài đặc biệt
              </label>
              <nav className="nav nav-sidebar tx-13">
                {Data2.map((item, index) => {
                  const { iconName, title, count, id } = item;
                  return (
                    <div
                      key={index}
                      className={`nav-link ${
                        menuActive === id ? ' active' : ''
                      }`}
                      onClick={() => onSidebarClick(id)}
                    >
                      {iconName} <span> {title} </span>
                      {count > 0 ? (
                        <span className="badge">{count}</span>
                      ) : null}
                    </div>
                  );
                })}
              </nav>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  authReducer: state.auth,
});

const mapDispatchToProps = {
  showAddNewsDrawerConnect: showAddNewsDrawer,
};
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(LeftNav),
);
