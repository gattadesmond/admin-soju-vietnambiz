import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { isEqual } from 'lodash';

import LeftNav from './components/LeftNav/LeftNav';

import MainContent from './components/MainContent/MainContent';
import { addNewsPermission, EnumNewsSidebar } from '../../constants';
import {
  getNews,
  getNewsStatistic, reloadNews,
  showAddNewsDrawer,
} from '../../actions/news.action';
import { generateZoneOptions } from '../../utils';

class News extends Component {
  constructor(props) {
    super(props);
    const { location } = props;
    const { pathname } = location || {};
    let id = null;
    Object.entries(EnumNewsSidebar).forEach(([key, value]) => {
      if (value.path === pathname) {
        id = value.id;
      }
    });
    this.state = {
      status: id,
      zoneId: -1,
      keyword: '',
      from: '',
      to: '',
      author: '',
      penName: '',
      pageSize: 10,
      pageIndex: 1,
    };
  }

  componentDidMount() {
    const { getNewsStatisticConnect, reloadNewsConnect } = this.props;
    getNewsStatisticConnect();
    reloadNewsConnect(false);
    this.handleSearchClick();
  }

  componentDidUpdate(prevProps, prevState, prevContext) {
    const { newsReducer: prevNewsReducer } = prevProps;
    const { newsReducer, getNewsStatisticConnect, reloadNewsConnect } = this.props;
    const { isReload: prevIsReload } = prevNewsReducer || {};
    const { isReload } = newsReducer || {};
    if (!isEqual(prevIsReload, isReload) && isReload) {
      this.handleSearchClick();
      getNewsStatisticConnect();
      reloadNewsConnect(false);
    }
  }

  handleRangePickerChange = (dates, dateStrings) => {
    this.setState({ from: dateStrings[0], to: dateStrings[1] });
  };

  handleKeywordChange = (keyword) => {
    this.setState({ keyword });
  };

  handleZoneChange = (zoneId) => {
    this.setState({ zoneId });
  };

  handleAuthorChange = (author) => {
    this.setState({ author });
  };

  handlePenNameChange = (penName) => {
    this.setState({ penName });
  };

  handlePageSizeChange = (currentPage, pageSize) => {
    this.setState({ pageSize }, () => this.handleSearchClick(currentPage));
  };

  handlePageIndexChange = (pageIndex) => {
    this.handleSearchClick(pageIndex);
  };

  handleSearchClick = (pageIndex = 1) => {
    this.setState({ pageIndex }, () => {
      const {
        status,
        zoneId,
        keyword,
        from,
        to,
        author,
        penName,
        pageSize,
        pageIndex,
      } = this.state;
      const { getNewsConnect } = this.props;
      getNewsConnect(
        status,
        zoneId,
        keyword,
        from,
        to,
        author,
        penName,
        pageSize,
        pageIndex,
      );
    });
  };

  handleDrawerShow = () => {
    this.props.showAddNewsDrawerConnect();
  };

  handleSidebarClick = async (active) => {
    const { history } = this.props;
    let url = null;
    Object.entries(EnumNewsSidebar).forEach(([key, value]) => {
      if (value.id === active) {
        url = value.path;
      }
    });
    history.replace(url);
    await this.setState({
      status: active,
    });
    this.handleSearchClick();
  };

  handlePostItemClick = (id) => {
    const { showAddNewsDrawerConnect } = this.props;
    const isDisable = true;
    showAddNewsDrawerConnect(id, isDisable);
  };

  render() {
    const { status } = this.state;
    const { globalReducer, newsReducer, authReducer } = this.props;
    const { news, statistic } = newsReducer || {};
    const { permission, account } = authReducer || {};
    const { userName } = account || {};
    const { data, total } = news || {};

    const { zones } = globalReducer || {};
    let permissionZone = null;
    if (permission && permission.length > 0) {
      const index = permission.findIndex((x) => x.id === addNewsPermission);
      if (index !== -1) {
        permissionZone = permission[index].zoneIds;
      }
    }

    return (
      <div className="filemgr-wrapper">
        <LeftNav
          statistic={statistic}
          menuActive={status}
          onAddNewsClick={this.handleDrawerShow}
          onSidebarClick={this.handleSidebarClick}
        />
        <MainContent
          news={data}
          userName={userName}
          total={total}
          filterState={this.state}
          zoneOptions={generateZoneOptions(zones, permissionZone, {
            id: -1,
            name: 'Tất cả chuyên mục',
          })}
          onAddNewsClick={this.handleDrawerShow}
          onZoneChange={this.handleZoneChange}
          onKeywordChange={this.handleKeywordChange}
          onRangePickerChange={this.handleRangePickerChange}
          onAuthorChange={this.handleAuthorChange}
          onPenNameChange={this.handlePenNameChange}
          onPageSizeChange={this.handlePageSizeChange}
          onPageIndexChange={this.handlePageIndexChange}
          onPostItemClick={this.handlePostItemClick}
          onSearchClick={this.handleSearchClick}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  newsReducer: state.news,
  globalReducer: state.global,
  authReducer: state.auth,
});

const mapDispatchToProps = {
  showAddNewsDrawerConnect: showAddNewsDrawer,
  getNewsConnect: getNews,
  getNewsStatisticConnect: getNewsStatistic,
  reloadNewsConnect: reloadNews,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(News));
