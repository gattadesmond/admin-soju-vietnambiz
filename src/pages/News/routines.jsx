import { Tooltip } from 'antd';
import React from 'react';
import { EnumNewsSidebar } from '../../constants';

export function generateToolbar(
  status,
  isOwner,
  onNewsEdit,
  onNewsSend,
  onNewsWithdraw,
  onNewsReturn,
  onNewsRemove,
  onNewsDetailView,
  onNewsRelease,
  onNewsTakenDown,
  onCompareVersion,
  onNewsReceived,
  onNewsUnlock,
  onNewsRestore,
) {
  const toolbar = [];
  if ([EnumNewsSidebar.WAITING_EDITING_POST.id].includes(status) && isOwner) {
    toolbar.push({
      name: 'Rút bài',
      icon: 'fa fa-backward',
      onClick: onNewsWithdraw,
    });
  }
  if (status !== EnumNewsSidebar.REMOVED_POST.id) {
    toolbar.push({
      name: 'Sửa',
      icon: 'fa fa-pencil',
      onClick: onNewsEdit,
    });
  }
  if ([EnumNewsSidebar.RELEASE_POST.id].includes(status)) {
    toolbar.push({
      name: 'Gỡ xuống',
      icon: 'fa fa-level-down',
      onClick: onNewsTakenDown,
    });
  }
  if ([EnumNewsSidebar.REMOVED_POST.id].includes(status)) {
    toolbar.push({
      name: 'Phục hồi',
      icon: 'fa fa-repeat',
      onClick: onNewsRestore,
    });
  }
  if (
    [
      EnumNewsSidebar.WAITING_EDITING_POST.id,
      EnumNewsSidebar.WAITING_RELEASE_POST.id,
      EnumNewsSidebar.TAKEN_DOWN_POST.id,
    ].includes(status)
  ) {
    toolbar.push({
      name: 'Nhận bài',
      icon: 'fa fa-download',
      onClick: onNewsReceived,
    });
  }
  if (
    [
      EnumNewsSidebar.TEMP_POST.id,
      EnumNewsSidebar.RECEIVED_EDITING_POST.id,
    ].includes(status)
  ) {
    toolbar.push({
      name: 'Gửi lên',
      icon: 'fa fa-paper-plane-o',
      onClick: onNewsSend,
    });
  }
  if (
    [
      EnumNewsSidebar.TEMP_POST.id,
      EnumNewsSidebar.RECEIVED_EDITING_POST.id,
      EnumNewsSidebar.RECEIVED_RELEASE_POST.id,
      EnumNewsSidebar.TAKEN_DOWN_POST.id,
    ].includes(status)
  ) {
    toolbar.push({
      name: 'Xuất bản',
      icon: 'fa fa-newspaper-o',
      onClick: onNewsRelease,
    });
  }
  if (
    [
      EnumNewsSidebar.WAITING_EDITING_POST.id,
      EnumNewsSidebar.RECEIVED_EDITING_POST.id,
      EnumNewsSidebar.WAITING_RELEASE_POST.id,
      EnumNewsSidebar.RECEIVED_RELEASE_POST.id,
      EnumNewsSidebar.TAKEN_DOWN_POST.id,
    ].includes(status) &&
    !isOwner
  ) {
    toolbar.push({
      name: 'Trả lại',
      icon: 'fa fa-reply',
      onClick: onNewsReturn,
    });
  }
  if (
    [
      EnumNewsSidebar.RECEIVED_EDITING_POST.id,
      EnumNewsSidebar.RECEIVED_RELEASE_POST.id,
    ].includes(status)
  ) {
    toolbar.push({
      name: 'Nhả ra',
      icon: 'fa fa-unlock',
      onClick: onNewsUnlock,
    });
  }
  toolbar.push({
    name: 'Xem chi tiết',
    icon: 'fa fa-eye',
    onClick: onNewsDetailView,
  });
  toolbar.push({
    name: 'Thảo luận',
    icon: 'fa fa-commenting-o',
  });
  toolbar.push({
    name: 'Chấm nhuận',
    icon: 'fa fa-money',
  });
  toolbar.push({
    name: 'Xem lịch sử',
    icon: 'fa fa-history',
  });
  toolbar.push({
    name: 'So sánh phiên bản',
    icon: 'fa fa-files-o',
    onClick: onCompareVersion,
  });
  if (
    (status !== EnumNewsSidebar.REMOVED_POST.id && isOwner) ||
    status === EnumNewsSidebar.TAKEN_DOWN_POST.id
  ) {
    toolbar.push({
      name: 'Xóa',
      icon: 'fa fa-trash-o',
      onClick: onNewsRemove,
    });
  }
  return toolbar.map((item) => {
    const { name, icon, onClick } = item;
    return (
      <Tooltip placement="bottom" title={name}>
        <button type="button" className="btn btn-secondary" onClick={onClick}>
          <i className={icon} />
        </button>
      </Tooltip>
    );
  });
}
