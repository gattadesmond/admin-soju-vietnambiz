import React, { Component } from 'react';
import ReactFlot from 'react-flot';
import { df1, df2, df3 } from '../../data';

class DeviceCard extends Component {
  render() {
    return (
      <div className="card">
        <div className="card-header">
          <h6 className="mg-b-0">Device Sessions</h6>
        </div>
        <div className="card-body">
          <div className="row row-xs">
            <div className="col-4 col-lg">
              <div className="d-flex align-items-baseline">
                <span className="d-block wd-8 ht-8 rounded-circle bg-primary" />
                <span className="d-block tx-10 tx-uppercase tx-medium tx-spacing-1 tx-color-03 mg-l-7">
                  Mobile
                </span>
              </div>
              <h4 className="tx-normal tx-rubik tx-spacing--1 mg-l-15 mg-b-0">
                6,098
              </h4>
            </div>
            <div className="col-4 col-lg">
              <div className="d-flex align-items-baseline">
                <span className="d-block wd-8 ht-8 rounded-circle bg-teal" />
                <span className="d-block tx-10 tx-uppercase tx-medium tx-spacing-1 tx-color-03 mg-l-7">
                  Desktop
                </span>
              </div>
              <h4 className="tx-normal tx-rubik tx-spacing--1 mg-l-15 mg-b-0">
                3,902
              </h4>
            </div>
            <div className="col-4 col-lg">
              <div className="d-flex align-items-baseline">
                <span className="d-block wd-8 ht-8 rounded-circle bg-gray-300" />
                <span className="d-block tx-10 tx-uppercase tx-medium tx-spacing-1 tx-color-03 mg-l-7">
                  Other
                </span>
              </div>
              <h4 className="tx-normal tx-rubik tx-spacing--1 mg-l-15 mg-b-0">
                1,065
              </h4>
            </div>
          </div>
          <div className="chart-nine">
            <div className="flot-chart">
              <ReactFlot
                id="new-chart"
                data={[
                  {
                    data: df2,
                    color: '#66a4fb',
                    lines: {
                      show: true,
                      lineWidth: 1.5,
                      fill: 0.03,
                    },
                  },
                  {
                    data: df1,
                    color: '#00cccc',
                    lines: {
                      show: true,
                      lineWidth: 1.5,
                      fill: true,
                      fillColor: '#fff',
                    },
                  },
                  {
                    data: df3,
                    color: '#e3e7ed',
                    bars: {
                      show: true,
                      lineWidth: 0,
                      barWidth: 0.5,
                      fill: 1,
                    },
                  },
                ]}
                options={{
                  series: {
                    shadowSize: 0,
                  },
                  grid: {
                    aboveData: true,
                    color: '#e5e9f2',
                    borderWidth: {
                      top: 0,
                      right: 1,
                      bottom: 1,
                      left: 1,
                    },
                    labelMargin: 0,
                  },
                  yaxis: {
                    show: false,
                    min: 0,
                    max: 100,
                  },
                  xaxis: {
                    show: true,
                    min: 40,
                    max: 80,
                    ticks: 6,
                    tickColor: 'rgba(0,0,0,0.04)',
                  },
                }}
                height="142px"
                width="341px"
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default DeviceCard;
