import React, { Component } from 'react';
import { Doughnut } from 'react-chartjs-2';

class SessionCard extends Component {
  render() {
    const data = {
      labels: ['Organic Search', 'Email', 'Referral', 'Social Media'],
      datasets: [
        {
          data: [20, 20, 30, 25],
          backgroundColor: ['#f77eb9', '#7ebcff', '#7ee5e5', '#fdbd88'],
        },
      ],
    };

    const options = {
      maintainAspectRatio: false,
      responsive: true,
      legend: {
        display: false,
      },
      animation: {
        animateScale: true,
        animateRotate: true,
      },
    };
    return (
      <div className="card">
        <div className="card-header">
          <h6 className="mg-b-0">Sessions By Channel</h6>
        </div>
        <div className="card-body pd-lg-25">
          <div className="chart-seven">
            <Doughnut data={data} options={options} />
          </div>
        </div>
        <div className="card-footer pd-20">
          <div className="row">
            <div className="col-6">
              <p className="tx-10 tx-uppercase tx-medium tx-color-03 tx-spacing-1 tx-nowrap mg-b-5">
                Organic Search
              </p>
              <div className="d-flex align-items-center">
                <div className="wd-10 ht-10 rounded-circle bg-pink mg-r-5" />
                <h5 className="tx-normal tx-rubik mg-b-0">
                  1,320 <small className="tx-color-04">25%</small>
                </h5>
              </div>
            </div>
            <div className="col-6">
              <p className="tx-10 tx-uppercase tx-medium tx-color-03 tx-spacing-1 mg-b-5">
                Email
              </p>
              <div className="d-flex align-items-center">
                <div className="wd-10 ht-10 rounded-circle bg-primary mg-r-5" />
                <h5 className="tx-normal tx-rubik mg-b-0">
                  987 <small className="tx-color-04">20%</small>
                </h5>
              </div>
            </div>
            <div className="col-6 mg-t-20">
              <p className="tx-10 tx-uppercase tx-medium tx-color-03 tx-spacing-1 mg-b-5">
                Referrral
              </p>
              <div className="d-flex align-items-center">
                <div className="wd-10 ht-10 rounded-circle bg-teal mg-r-5" />
                <h5 className="tx-normal tx-rubik mg-b-0">
                  2,010 <small className="tx-color-04">30%</small>
                </h5>
              </div>
            </div>
            <div className="col-6 mg-t-20">
              <p className="tx-10 tx-uppercase tx-medium tx-color-03 tx-spacing-1 mg-b-5">
                Social Media
              </p>
              <div className="d-flex align-items-center">
                <div className="wd-10 ht-10 rounded-circle bg-orange mg-r-5" />
                <h5 className="tx-normal tx-rubik mg-b-0">
                  1,054 <small className="tx-color-04">25%</small>
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SessionCard;
