/* eslint-disable */
import React, { Component } from 'react';
import ReactFlot from 'react-flot';

import { df1, df3 } from '../../data';

class AcquisitionCard extends Component {
  render() {
    return (
      <div className="card">
        <div className="card-header pd-b-0 bd-b-0 pd-t-20 pd-lg-t-25 pd-l-20 pd-lg-l-25">
          <h6 className="mg-b-5">Acquisition</h6>
          <p className="tx-12 tx-color-03 mg-b-0">
            Tells you where your visitors originated from, such as search
            engines, social networks or website referrals.{' '}
            <a href>Learn more</a>
          </p>
        </div>
        <div className="card-body pd-sm-20 pd-lg-25">
          <div className="row row-sm">
            <div className="col-sm-5 col-md-12 col-lg-6 col-xl-5">
              <div className="media">
                <div className="wd-40 ht-40 rounded bd bd-2 bd-primary d-flex flex-shrink-0 align-items-center justify-content-center op-6">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-bar-chart-2 wd-20 ht-20 tx-primary stroke-wd-3"
                  >
                    <line x1="18" y1="20" x2="18" y2="10" />
                    <line x1="12" y1="20" x2="12" y2="4" />
                    <line x1="6" y1="20" x2="6" y2="14" />
                  </svg>
                </div>
                <div className="media-body mg-l-10">
                  <h4 className="tx-normal tx-rubik tx-spacing--2 lh-1 mg-b-5">
                    33.50%
                  </h4>
                  <p className="tx-10 tx-uppercase tx-medium tx-color-03 tx-spacing-1 tx-nowrap mg-b-0">
                    Bounce Rate
                  </p>
                </div>
              </div>
            </div>
            <div className="col-sm-5 col-md-12 col-lg-6 col-xl-5 mg-t-20 mg-sm-t-0 mg-md-t-20 mg-lg-t-0">
              <div className="media">
                <div className="wd-40 ht-40 rounded bd bd-2 bd-gray-500 d-flex flex-shrink-0 align-items-center justify-content-center op-6">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-bar-chart-2 wd-20 ht-20 tx-gray-500 stroke-wd-3"
                  >
                    <line x1="18" y1="20" x2="18" y2="10" />
                    <line x1="12" y1="20" x2="12" y2="4" />
                    <line x1="6" y1="20" x2="6" y2="14" />
                  </svg>
                </div>
                <div className="media-body mg-l-10">
                  <h4 className="tx-normal tx-rubik tx-spacing--2 lh-1 mg-b-5">
                    9,065
                  </h4>
                  <p className="tx-10 tx-uppercase tx-medium tx-color-03 tx-spacing-1 tx-nowrap mg-b-0">
                    Page Sessions
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="chart-eight">
            <div className="flot-chart">
              <ReactFlot
                id="test"
                data={[
                  {
                    data: df1,
                    color: '#c0ccda',
                    lines: {
                      fill: true,
                      fillColor: '#f5f6fa',
                    },
                  },
                  {
                    data: df3,
                    color: '#0168fa',
                    lines: {
                      fill: true,
                      fillColor: '#d1e6fa',
                    },
                  },
                ]}
                options={{
                  series: {
                    shadowSize: 0,
                    lines: {
                      show: true,
                      lineWidth: 1.5,
                    },
                  },
                  grid: {
                    borderWidth: 0,
                    labelMargin: 0,
                  },
                  yaxis: {
                    show: false,
                    max: 65,
                  },
                  xaxis: {
                    show: false,
                    min: 40,
                    max: 100,
                  },
                }}
                height="126px"
                width="477px"
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default AcquisitionCard;
