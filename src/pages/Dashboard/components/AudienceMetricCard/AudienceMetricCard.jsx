import React, { Component } from 'react';
import { Bar } from 'react-chartjs-2';

class AudienceMetricCard extends Component {
  render() {
    const ctxLabel = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    const ctxData1 = [20, 60, 50, 45, 50, 60, 70, 40, 45, 35, 25, 30];
    const ctxData2 = [10, 40, 30, 40, 60, 55, 45, 35, 30, 20, 15, 20];
    const ctxColor1 = '#66a4fb';
    const ctxColor2 = '#65e0e0';
    const data = {
      labels: ctxLabel,
      datasets: [
        {
          data: ctxData1,
          backgroundColor: ctxColor1,
        },
        {
          data: ctxData2,
          backgroundColor: ctxColor2,
        },
      ],
    };

    const options = {
      maintainAspectRatio: false,
      responsive: true,
      legend: {
        display: false,
        labels: {
          display: false,
        },
      },
      scales: {
        yAxes: [
          {
            gridLines: {
              color: '#e5e9f2',
            },
            ticks: {
              beginAtZero: true,
              fontSize: 10,
              fontColor: '#182b49',
              max: 80,
            },
          },
        ],
        xAxes: [
          {
            gridLines: {
              display: false,
            },
            barPercentage: 0.6,
            ticks: {
              beginAtZero: true,
              fontSize: 11,
              fontColor: '#182b49',
            },
          },
        ],
      },
    };

    return (
      <div className="card">
        <div className="card-header bd-b-0 pd-t-20 pd-lg-t-25 pd-l-20 pd-lg-l-25 d-flex flex-column flex-sm-row align-items-sm-start justify-content-sm-between">
          <div>
            <h6 className="mg-b-5">Website Audience Metrics</h6>
            <p className="tx-12 tx-color-03 mg-b-0">
              Audience to which the users belonged while on the current date
              range.
            </p>
          </div>
          <div className="btn-group mg-t-20 mg-sm-t-0">
            <button className="btn btn-xs btn-white btn-uppercase">Day</button>
            <button className="btn btn-xs btn-white btn-uppercase">Week</button>
            <button className="btn btn-xs btn-white btn-uppercase active">
              Month
            </button>
          </div>
          {/* btn-group */}
        </div>
        {/* card-header */}
        <div className="card-body pd-lg-25">
          <div className="row align-items-sm-end">
            <div className="col-lg-7 col-xl-8">
              <div className="chart-six">
                <Bar data={data} options={options} height={300} width={529} />
              </div>
            </div>
            <div className="col-lg-5 col-xl-4 mg-t-30 mg-lg-t-0">
              <div className="row">
                <div className="col-sm-6 col-lg-12">
                  <div className="d-flex align-items-center justify-content-between mg-b-5">
                    <h6 className="tx-uppercase tx-10 tx-spacing-1 tx-color-02 tx-semibold mg-b-0">
                      New Users
                    </h6>
                    <span className="tx-10 tx-color-04">65% goal reached</span>
                  </div>
                  <div className="d-flex align-items-end justify-content-between mg-b-5">
                    <h5 className="tx-normal tx-rubik lh-2 mg-b-0">13,596</h5>
                    <h6 className="tx-normal tx-rubik tx-color-03 lh-2 mg-b-0">
                      20,000
                    </h6>
                  </div>
                  <div className="progress ht-4 mg-b-0 op-5">
                    <div
                      className="progress-bar bg-teal wd-65p"
                      role="progressbar"
                      aria-valuenow={65}
                      aria-valuemin={0}
                      aria-valuemax={100}
                    />
                  </div>
                </div>
                <div className="col-sm-6 col-lg-12 mg-t-30 mg-sm-t-0 mg-lg-t-30">
                  <div className="d-flex align-items-center justify-content-between mg-b-5">
                    <h6 className="tx-uppercase tx-10 tx-spacing-1 tx-color-02 tx-semibold mg-b-0">
                      Page Views
                    </h6>
                    <span className="tx-10 tx-color-04">45% goal reached</span>
                  </div>
                  <div className="d-flex justify-content-between mg-b-5">
                    <h5 className="tx-normal tx-rubik mg-b-0">83,123</h5>
                    <h5 className="tx-normal tx-rubik tx-color-03 mg-b-0">
                      <small>250,000</small>
                    </h5>
                  </div>
                  <div className="progress ht-4 mg-b-0 op-5">
                    <div
                      className="progress-bar bg-orange wd-45p"
                      role="progressbar"
                      aria-valuenow={45}
                      aria-valuemin={0}
                      aria-valuemax={100}
                    />
                  </div>
                </div>
                <div className="col-sm-6 col-lg-12 mg-t-30">
                  <div className="d-flex align-items-center justify-content-between mg-b-5">
                    <h6 className="tx-uppercase tx-10 tx-spacing-1 tx-color-02 tx-semibold mg-b-0">
                      Page Sessions
                    </h6>
                    <span className="tx-10 tx-color-04">20% goal reached</span>
                  </div>
                  <div className="d-flex justify-content-between mg-b-5">
                    <h5 className="tx-normal tx-rubik mg-b-0">16,869</h5>
                    <h5 className="tx-normal tx-rubik tx-color-03 mg-b-0">
                      <small>85,000</small>
                    </h5>
                  </div>
                  <div className="progress ht-4 mg-b-0 op-5">
                    <div
                      className="progress-bar bg-pink wd-20p"
                      role="progressbar"
                      aria-valuenow={20}
                      aria-valuemin={0}
                      aria-valuemax={100}
                    />
                  </div>
                </div>
                <div className="col-sm-6 col-lg-12 mg-t-30">
                  <div className="d-flex align-items-center justify-content-between mg-b-5">
                    <h6 className="tx-uppercase tx-10 tx-spacing-1 tx-color-02 tx-semibold mg-b-0">
                      Bounce Rate
                    </h6>
                    <span className="tx-10 tx-color-04">85% goal reached</span>
                  </div>
                  <div className="d-flex justify-content-between mg-b-5">
                    <h5 className="tx-normal tx-rubik mg-b-0">28.50%</h5>
                    <h5 className="tx-normal tx-rubik tx-color-03 mg-b-0">
                      <small>30.50%</small>
                    </h5>
                  </div>
                  <div className="progress ht-4 mg-b-0 op-5">
                    <div
                      className="progress-bar bg-primary wd-85p"
                      role="progressbar"
                      aria-valuenow={85}
                      aria-valuemin={0}
                      aria-valuemax={100}
                    />
                  </div>
                </div>
              </div>
              {/* row */}
            </div>
          </div>
        </div>
        {/* card-body */}
      </div>
    );
  }
}
export default AudienceMetricCard;
