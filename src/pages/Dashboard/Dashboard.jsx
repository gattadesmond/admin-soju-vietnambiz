import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import AudienceMetricCard from './components/AudienceMetricCard/AudienceMetricCard';
import SessionCard from './components/SessionCard/SessionCard';
// import AcquisitionCard from './components/AcquisitionCard/AcquisitionCard';
// import DeviceCard from './components/DeviceCard/DeviceCard';

class Dashboard extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="content content-fixed">
          <div className="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
            <div className="row row-xs">
              <div className="col-lg-8 col-xl-9">
                <AudienceMetricCard />
              </div>
              <div className="col-md-6 col-lg-4 col-xl-3 mg-t-10 mg-lg-t-0">
                <SessionCard />
              </div>
              <div className="col-md-6 col-lg-5 mg-t-10">
                {/* <AcquisitionCard /> */}
              </div>
              <div className="col-sm-7 col-md-8 col-lg-4 col-xl mg-t-10">
                {/* <DeviceCard /> */}
              </div>
              <div className="col-sm-5 col-md-4 col-lg-3 mg-t-10">
                <div className="card">
                  <div className="card-header">
                    <h6 className="mg-b-0">Top Traffic Source</h6>
                  </div>
                  {/* card-header */}
                  <div className="card-body tx-center">
                    <h4 className="tx-normal tx-rubik tx-40 tx-spacing--1 mg-b-0">
                      29,931
                    </h4>
                    <p className="tx-12 tx-uppercase tx-semibold tx-spacing-1 tx-color-02">
                      Organic Search
                    </p>
                    <p className="tx-12 tx-color-03 mg-b-0">
                      {`Measures your user's sources that generate traffic metrics
                      to your website for this month.`}
                    </p>
                  </div>
                  {/* card-body */}
                  <div className="card-footer bd-t-0 pd-t-0">
                    <button className="btn btn-sm btn-block btn-outline-primary btn-uppercase tx-spacing-1">
                      Learn More
                    </button>
                  </div>
                  {/* card-footer */}
                </div>
                {/* card */}
              </div>
              <div className="col-lg-6 mg-t-10">
                <div className="card">
                  <div className="card-header d-flex align-items-start justify-content-between">
                    <h6 className="lh-5 mg-b-0">Total Visits</h6>
                    <a href className="tx-13 link-03">
                      Mar 01 - Mar 20, 2019{' '}
                      <i className="icon ion-ios-arrow-down" />
                    </a>
                  </div>
                  <div className="card-body pd-y-15 pd-x-10">
                    <div className="table-responsive">
                      <table className="table table-borderless table-sm tx-13 tx-nowrap mg-b-0">
                        <thead>
                          <tr className="tx-10 tx-spacing-1 tx-color-03 tx-uppercase">
                            <th className="wd-5p">Link</th>
                            <th>Page Title</th>
                            <th className="text-right">Percentage (%)</th>
                            <th className="text-right">Value</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td className="align-middle text-center">
                              <a href>
                                <i
                                  data-feather="external-link"
                                  className="wd-12 ht-12 stroke-wd-3"
                                />
                              </a>
                            </td>
                            <td className="align-middle tx-medium">Homepage</td>
                            <td className="align-middle text-right">
                              <div className="wd-150 d-inline-block">
                                <div className="progress ht-4 mg-b-0">
                                  <div
                                    className="progress-bar bg-teal wd-65p"
                                    role="progressbar"
                                    aria-valuenow={65}
                                    aria-valuemin={0}
                                    aria-valuemax={100}
                                  />
                                </div>
                              </div>
                            </td>
                            <td className="align-middle text-right">
                              <span className="tx-medium">65.35%</span>
                            </td>
                          </tr>
                          <tr>
                            <td className="align-middle text-center">
                              <a href>
                                <i
                                  data-feather="external-link"
                                  className="wd-12 ht-12 stroke-wd-3"
                                />
                              </a>
                            </td>
                            <td className="align-middle tx-medium">
                              Our Services
                            </td>
                            <td className="align-middle text-right">
                              <div className="wd-150 d-inline-block">
                                <div className="progress ht-4 mg-b-0">
                                  <div
                                    className="progress-bar bg-primary wd-85p"
                                    role="progressbar"
                                    aria-valuenow={85}
                                    aria-valuemin={0}
                                    aria-valuemax={100}
                                  />
                                </div>
                              </div>
                            </td>
                            <td className="text-right">
                              <span className="tx-medium">84.97%</span>
                            </td>
                          </tr>
                          <tr>
                            <td className="align-middle text-center">
                              <a href>
                                <i
                                  data-feather="external-link"
                                  className="wd-12 ht-12 stroke-wd-3"
                                />
                              </a>
                            </td>
                            <td className="align-middle tx-medium">
                              List of Products
                            </td>
                            <td className="align-middle text-right">
                              <div className="wd-150 d-inline-block">
                                <div className="progress ht-4 mg-b-0">
                                  <div
                                    className="progress-bar bg-warning wd-45p"
                                    role="progressbar"
                                    aria-valuenow={45}
                                    aria-valuemin={0}
                                    aria-valuemax={100}
                                  />
                                </div>
                              </div>
                            </td>
                            <td className="text-right">
                              <span className="tx-medium">38.66%</span>
                            </td>
                          </tr>
                          <tr>
                            <td className="align-middle text-center">
                              <a href>
                                <i
                                  data-feather="external-link"
                                  className="wd-12 ht-12 stroke-wd-3"
                                />
                              </a>
                            </td>
                            <td className="align-middle tx-medium">
                              Contact Us
                            </td>
                            <td className="align-middle text-right">
                              <div className="wd-150 d-inline-block">
                                <div className="progress ht-4 mg-b-0">
                                  <div
                                    className="progress-bar bg-pink wd-15p"
                                    role="progressbar"
                                    aria-valuenow={15}
                                    aria-valuemin={0}
                                    aria-valuemax={100}
                                  />
                                </div>
                              </div>
                            </td>
                            <td className="text-right">
                              <span className="tx-medium">16.11%</span>
                            </td>
                          </tr>
                          <tr>
                            <td className="align-middle text-center">
                              <a href>
                                <i
                                  data-feather="external-link"
                                  className="wd-12 ht-12 stroke-wd-3"
                                />
                              </a>
                            </td>
                            <td className="align-middle tx-medium">
                              Product 50% Sale
                            </td>
                            <td className="align-middle text-right">
                              <div className="wd-150 d-inline-block">
                                <div className="progress ht-4 mg-b-0">
                                  <div
                                    className="progress-bar bg-teal wd-60p"
                                    role="progressbar"
                                    aria-valuenow={60}
                                    aria-valuemin={0}
                                    aria-valuemax={100}
                                  />
                                </div>
                              </div>
                            </td>
                            <td className="text-right">
                              <span className="tx-medium">59.34%</span>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 mg-t-10">
                <div className="card">
                  <div className="card-header d-sm-flex align-items-start justify-content-between">
                    <h6 className="lh-5 mg-b-0">Browser Used By Users</h6>
                    <a href className="tx-13 link-03">
                      Mar 01 - Mar 20, 2019{' '}
                      <i className="icon ion-ios-arrow-down" />
                    </a>
                  </div>
                  <div className="card-body pd-y-15 pd-x-10">
                    <div className="table-responsive">
                      <table className="table table-borderless table-sm tx-13 tx-nowrap mg-b-0">
                        <thead>
                          <tr className="tx-10 tx-spacing-1 tx-color-03 tx-uppercase">
                            <th className="wd-5p">&nbsp;</th>
                            <th>Browser</th>
                            <th className="text-right">Sessions</th>
                            <th className="text-right">Bounce Rate</th>
                            <th className="text-right">Conversion Rate</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              <i className="fab fa-chrome tx-primary op-6" />
                            </td>
                            <td className="tx-medium">Google Chrome</td>
                            <td className="text-right">13,410</td>
                            <td className="text-right">40.95%</td>
                            <td className="text-right">19.45%</td>
                          </tr>
                          <tr>
                            <td>
                              <i className="fab fa-firefox tx-orange" />
                            </td>
                            <td className="tx-medium">Mozilla Firefox</td>
                            <td className="text-right">1,710</td>
                            <td className="text-right">47.58%</td>
                            <td className="text-right">19.99%</td>
                          </tr>
                          <tr>
                            <td>
                              <i className="fab fa-safari tx-primary" />
                            </td>
                            <td className="tx-medium">Apple Safari</td>
                            <td className="text-right">1,340</td>
                            <td className="text-right">56.50%</td>
                            <td className="text-right">11.00%</td>
                          </tr>
                          <tr>
                            <td>
                              <i className="fab fa-edge tx-primary" />
                            </td>
                            <td className="tx-medium">Microsoft Edge</td>
                            <td className="text-right">713</td>
                            <td className="text-right">59.62%</td>
                            <td className="text-right">4.69%</td>
                          </tr>
                          <tr>
                            <td>
                              <i className="fab fa-opera tx-danger" />
                            </td>
                            <td className="tx-medium">Opera</td>
                            <td className="text-right">380</td>
                            <td className="text-right">52.50%</td>
                            <td className="text-right">8.75%</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    {/* table-responsive */}
                  </div>
                  {/* card-body */}
                </div>
                {/* card */}
              </div>
              <div className="col mg-t-10">
                <div className="card card-dashboard-table">
                  <div className="table-responsive">
                    <table className="table table-bordered">
                      <thead>
                        <tr>
                          <th>&nbsp;</th>
                          <th colSpan={3}>Acquisition</th>
                          <th colSpan={3}>Behavior</th>
                          <th colSpan={3}>Conversions</th>
                        </tr>
                        <tr>
                          <th>Source</th>
                          <th>Users</th>
                          <th>New Users</th>
                          <th>Sessions</th>
                          <th>Bounce Rate</th>
                          <th>Pages/Session</th>
                          <th>Avg. Session</th>
                          <th>Transactions</th>
                          <th>Revenue</th>
                          <th>Rate</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <a href>Organic search</a>
                          </td>
                          <td>
                            <strong>350</strong>
                          </td>
                          <td>
                            <strong>22</strong>
                          </td>
                          <td>
                            <strong>5,628</strong>
                          </td>
                          <td>
                            <strong>25.60%</strong>
                          </td>
                          <td>
                            <strong>1.92</strong>
                          </td>
                          <td>
                            <strong>00:01:05</strong>
                          </td>
                          <td>
                            <strong>340,103</strong>
                          </td>
                          <td>
                            <strong>$2.65M</strong>
                          </td>
                          <td>
                            <strong>4.50%</strong>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <a href>Social media</a>
                          </td>
                          <td>
                            <strong>276</strong>
                          </td>
                          <td>
                            <strong>18</strong>
                          </td>
                          <td>
                            <strong>5,100</strong>
                          </td>
                          <td>
                            <strong>23.66%</strong>
                          </td>
                          <td>
                            <strong>1.89</strong>
                          </td>
                          <td>
                            <strong>00:01:03</strong>
                          </td>
                          <td>
                            <strong>321,960</strong>
                          </td>
                          <td>
                            <strong>$2.51M</strong>
                          </td>
                          <td>
                            <strong>4.36%</strong>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <a href>Referral</a>
                          </td>
                          <td>
                            <strong>246</strong>
                          </td>
                          <td>
                            <strong>17</strong>
                          </td>
                          <td>
                            <strong>4,880</strong>
                          </td>
                          <td>
                            <strong>26.22%</strong>
                          </td>
                          <td>
                            <strong>1.78</strong>
                          </td>
                          <td>
                            <strong>00:01:09</strong>
                          </td>
                          <td>
                            <strong>302,767</strong>
                          </td>
                          <td>
                            <strong>$2.1M</strong>
                          </td>
                          <td>
                            <strong>4.34%</strong>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <a href>Email</a>
                          </td>
                          <td>
                            <strong>187</strong>
                          </td>
                          <td>
                            <strong>14</strong>
                          </td>
                          <td>
                            <strong>4,450</strong>
                          </td>
                          <td>
                            <strong>24.97%</strong>
                          </td>
                          <td>
                            <strong>1.35</strong>
                          </td>
                          <td>
                            <strong>00:02:07</strong>
                          </td>
                          <td>
                            <strong>279,300</strong>
                          </td>
                          <td>
                            <strong>$1.86M</strong>
                          </td>
                          <td>
                            <strong>3.99%</strong>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  {/* table-responsive */}
                </div>
                {/* card */}
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default withRouter(Dashboard);
