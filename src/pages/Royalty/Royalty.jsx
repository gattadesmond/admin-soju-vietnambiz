import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Button, Col, Row } from 'antd';
import { connect } from 'react-redux';
import {
  getNews,
  getNewsStatistic,
  showAddNewsDrawer,
} from '../../actions/news.action';
import request from '../../utils/request';
import { isEmpty } from '../../utils';

class Royalty extends Component {
  handleExportClick = async () => {
    const response = await request.post(
      'royalty/export',
      {
        pageSize: 50,
        pageIndex: 1,
      },
      'application/json',
      true,
    );
  };
  render() {
    return (
      <React.Fragment>
        <Row>
          <Col span={24}>
            <h1>Royalty</h1>
            <Button onClick={this.handleExportClick}>Export</Button>
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  newsReducer: state.news,
  globalReducer: state.global,
  authReducer: state.auth,
});

const mapDispatchToProps = {
  showAddNewsDrawerConnect: showAddNewsDrawer,
  getNewsConnect: getNews,
  getNewsStatisticConnect: getNewsStatistic,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Royalty),
);
