import React from 'react';
import { List, Spin } from 'antd';
import InfiniteScroll from 'react-infinite-scroll-component';
import NewsLogItem from './components/NewsLogItem';

const NewsLogList = (props) => {
  const {
    newsLog,
    onFetchMoreData,
    hasMore,
  } = props;

  const data = Object.values(newsLog);
  return (
    <div id="scrollableDiv" className="news-log-infinite-container">
      <InfiniteScroll
        dataLength={data.length}
        next={onFetchMoreData}
        hasMore={hasMore}
        loader={<h4 className="news-log-loading-container"><Spin /></h4>}
        scrollableTarget="scrollableDiv"
      >
        <List
          dataSource={newsLog}
          renderItem={(item, index) => (
            <div className="mb-2 card-post-item">
              <NewsLogItem
                key={index}
                item={item}
              />
            </div>
          )}
        />
      </InfiniteScroll>
    </div>
  );
};

export default NewsLogList;
