import React, { Component } from 'react';
import moment from 'moment';

export default class NewsLogItem extends Component {
  render() {
    const { item } = this.props;
    const {
      actionText,
      createdDate,
      type,
      sourceId,
      destinationId,
      message,
    } = item || {};
    if (type === 4) {
      return (
        <div className="card card-file card-post">
          <div className="postItem-flex">
            <div className="html-content">
              <div>
                <p>
                  Tài khoản <b>{sourceId}</b> đã đăng nhập tại máy có địa chỉ IP:
                  <b> {destinationId}</b>
                </p>
                Trình duyệt:<i> {message}</i>
              </div>
            </div>
            <div className="log-time">
              <div className="string">{moment(createdDate).startOf('hour').fromNow()}</div>
              <div className="time">{moment(new Date(createdDate)).format('DD/MM/YYYY hh:mm')}</div>
            </div>
          </div>
        </div>
      );
    }
    return (
      <div className="card card-file card-post">
        <div className="postItem-flex">
          {/* eslint-disable-next-line react/no-danger */}
          <div className="html-content" dangerouslySetInnerHTML={{ __html: actionText }} />
          <div className="log-time">
            <div className="string">{moment(createdDate).startOf('hour').fromNow()}</div>
            <div className="time">{moment(new Date(createdDate)).format('DD/MM/YYYY hh:mm')}</div>
          </div>
        </div>
      </div>
    );
  }
}
