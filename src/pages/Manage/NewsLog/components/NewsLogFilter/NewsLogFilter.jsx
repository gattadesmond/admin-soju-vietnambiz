import React from 'react';
import { Input, Button, Select, DatePicker } from 'antd';
import moment from 'moment';
import 'moment/locale/vi';
import { TIME_TO_QUERY_AFTER_STOP_TYPING, EnumNewsLogType } from '../../../../../constants';
import OptionLogTypeAction from './OptionLogTypeAction';

const { RangePicker } = DatePicker;
const { Option } = Select;
const dateFormat = 'DD/MM/YYYY';

const NewsLogFilter = (props) => {
  const {
    logTypeAction,
    filterState,
    onAppIdChange,
    onRangePickerChange,
    onSearchClick,
    onLogTypeChange,
    onLogTypeActionChange,
  } = props;
  const { logTypeId, logTypeActionId, appId } = filterState || {};
  const dataNewsLogType = Object.values(EnumNewsLogType);

  return (
    <div className="news-filter ">
      <div className=" card card-body py-1 px-3">
        <div className="d-flex align-items-end  flex-nowrap">
          <div className="filter-form">
            <div className="filter-item">
              <div className="filter-item-label">Đối tượng log</div>
              <Select
                value={logTypeId}
                style={{ width: 190 }}
                onChange={onLogTypeChange}
              >
                {dataNewsLogType.map((item) => {
                  const { id, description } = item;
                  return (
                    <Option key={`${description}-${id}`} value={id} title={description}>{description}</Option>
                  );
                })}
              </Select>
            </div>
            <OptionLogTypeAction
              logTypeAction={logTypeAction}
              logTypeId={logTypeId}
              onLogTypeActionChange={onLogTypeActionChange}
              filterState={filterState}
            />
            <div className="filter-item">
              <div className="filter-item-label">Theo ngày đăng</div>
              <RangePicker
                ranges={{
                  Today: [moment(), moment()],
                  'This Month': [
                    moment().startOf('month'),
                    moment().endOf('month'),
                  ],
                }}
                format={dateFormat}
                style={{ width: 250 }}
                onChange={onRangePickerChange}
              />
            </div>
            <div className="filter-item">
              <div className="filter-item-label">Đối tượng ID</div>
              <Input
                placeholder="Nhập Id"
                value={appId}
                style={{ width: 160 }}
                onChange={(e) =>
                  setTimeout(
                    onAppIdChange(e.target.value),
                    TIME_TO_QUERY_AFTER_STOP_TYPING,
                  )
                }
              />
            </div>
            <div className="filter-item">
              <div className="filter-item-label">&nbsp;</div>
              <Button type="primary" onClick={() => onSearchClick()}>
                LỌC
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewsLogFilter;
