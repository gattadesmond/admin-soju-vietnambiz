import React from 'react';
import { Select } from 'antd';

const { Option } = Select;
const OptionLogTypeAction = (props) => {
  const { logTypeAction, logTypeId, onLogTypeActionChange, filterState } = props;
  const { logTypeActionId } = filterState || {};
  if (logTypeId === 1 || logTypeId === 2) {
    return (
      <div className="filter-item">
        <div className="filter-item-label">Loại hành động</div>
        <Select
          value={logTypeActionId}
          style={{ width: 190 }}
          onChange={onLogTypeActionChange}
        >
          {logTypeAction.map((item) => {
            const { id, description } = item;
            return (
              <Option key={id} value={id} title={description}>{description}</Option>
            );
          })}
        </Select>
      </div>
    );
  }
  return (
    <div />
  );
};
export default OptionLogTypeAction;