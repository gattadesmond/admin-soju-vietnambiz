import React from 'react';
import NewsLogFilter from '../NewsLogFilter/NewsLogFilter';
import NewsLogList from '../NewsLogList/NewsLogList';

const NewsLogContent = (props) => {
  const {
    newsLog,
    logTypeAction,
    filterState,
    onAppIdChange,
    onRangePickerChange,
    onSearchClick,
    onLogTypeChange,
    onLogTypeActionChange,
    onFetchMoreData,
    hasMore,
  } = props;
  return (
    <React.Fragment>
      <NewsLogFilter
        logTypeAction={logTypeAction}
        filterState={filterState}
        onAppIdChange={onAppIdChange}
        onRangePickerChange={onRangePickerChange}
        onSearchClick={onSearchClick}
        onLogTypeChange={onLogTypeChange}
        onLogTypeActionChange={onLogTypeActionChange}
      />
      <NewsLogList
        newsLog={newsLog}
        filterState={filterState}
        onFetchMoreData={onFetchMoreData}
        hasMore={hasMore}
      />
    </React.Fragment>
  );
};

export default NewsLogContent;
