import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import LeftNav from '../LeftNav/LeftNav';
import NewsLogContent from './components/NewsLogContent/NewsLogContent';
import {
  getNewsLog,
  getLogTypeAction,
  resetNewsLog,
} from '../../../actions/newslog.action';

class NewsLog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logTypeId: 1,
      logTypeActionId: -1,
      appId: '',
      from: '',
      to: '',
      pageSize: 10,
      pageIndex: 1,
      hasMore: true,
      loading: false,
    };
  }
  componentDidMount() {
    this.handleSearchClick();
    const { getLogTypeActionConnect } = this.props;
    getLogTypeActionConnect(this.state.logTypeId);
  }
  handleLogTypeChange = (logTypeId) => {
    this.setState({ logTypeActionId: -1 });
    const { getLogTypeActionConnect } = this.props;
    getLogTypeActionConnect(logTypeId);
    this.setState({ logTypeId });
  };
  handleLogTypeActionChange = (logTypeActionId) => {
    this.setState({ logTypeActionId });
    this.setState({
      pageIndex: 1,
    });
  };
  handleAppIdChange = (appId) => {
    this.setState({ appId });
  };
  handleRangePickerChange = (dates, dateStrings) => {
    this.setState({ from: dateStrings[0], to: dateStrings[1] });
  };
  handleSearchClick = (pageIndex = 1, hasMore = true) => {
    const { resetNewsLogConnect } = this.props;
    resetNewsLogConnect();
    this.setState({ pageIndex, hasMore }, () => {
      const {
        logTypeId,
        logTypeActionId,
        appId,
        from,
        to,
        pageSize,
        pageIndex,
      } = this.state;
      const { getNewsLogConnect } = this.props;
      getNewsLogConnect(
        logTypeId,
        logTypeActionId,
        appId,
        from,
        to,
        pageSize,
        pageIndex,
      );
    });
  };
  handleFetchMoreData = () => {
    const { newsLogReducer } = this.props;
    const { newslog } = newsLogReducer || {};
    const total1 = newslog.total;
    if (total1 < this.state.pageSize && this.state.pageIndex > 1) {
      this.setState({ hasMore: false, loading: false });
      return;
    }
    this.setState({
      loading: true,
    });
    setTimeout(() => {
      let { pageIndex } = this.state;
      pageIndex += 1;
      this.setState({ pageIndex }, () => {
        const {
          logTypeId,
          logTypeActionId,
          appId,
          from,
          to,
          pageSize,
          pageIndex,
        } = this.state;
        const { getNewsLogConnect } = this.props;
        getNewsLogConnect(
          logTypeId,
          logTypeActionId,
          appId,
          from,
          to,
          pageSize,
          pageIndex,
        );
      });
    }, 1000);
    this.setState({
      loading: false,
    });
  };
  render() {
    const { newsLogReducer, newsLogActionReducer } = this.props;
    const dataLogAction = newsLogActionReducer.data || {};
    const { newslog, hasMore } = newsLogReducer || {};
    const dataNewsLog = newslog.data || {};

    return (
      <NewsLogContent
        newsLog={dataNewsLog}
        logTypeAction={dataLogAction}
        filterState={this.state}
        onSearchClick={this.handleSearchClick}
        onLogTypeChange={this.handleLogTypeChange}
        onLogTypeActionChange={this.handleLogTypeActionChange}
        onRangePickerChange={this.handleRangePickerChange}
        onAppIdChange={this.handleAppIdChange}
        onFetchMoreData={this.handleFetchMoreData}
        hasMore={hasMore}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  newsLogReducer: state.newslog,
  newsLogActionReducer: state.newsLogAction,
});

const mapDispatchToProps = {
  getNewsLogConnect: getNewsLog,
  getLogTypeActionConnect: getLogTypeAction,
  resetNewsLogConnect: resetNewsLog,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(NewsLog),
);
