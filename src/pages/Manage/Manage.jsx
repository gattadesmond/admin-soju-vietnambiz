import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import LeftNav from './LeftNav/LeftNav';
import { EnumManageSidebar } from '../../constants';
import Account from './Account/Account';
import NewsLog from './NewsLog/NewsLog';
import Tags from './Tags/Tags';
import PermissionGroup from './PermissionGroup/PermissionGroup';

class Manage extends Component {
  constructor(props) {
    super(props);
    const { location } = props;
    const { pathname } = location || {};
    let id = null;
    Object.entries(EnumManageSidebar).forEach(([key, value]) => {
      if (value.path === pathname) {
        id = value.id;
      }
    });
    this.state = {
      status: id,
    };
  }

  handleSidebarClick = async (active) => {
    const { history } = this.props;
    let url = null;
    Object.entries(EnumManageSidebar).forEach(([key, value]) => {
      if (value.id === active) {
        url = value.path;
      }
    });
    history.replace(url);
    await this.setState({
      status: active,
    });
  };

  render() {
    const { status } = this.state;
    return (
      <div className="filemgr-wrapper">
        <LeftNav
          menuActive={status}
          onSidebarClick={this.handleSidebarClick}
        />
        <div className="filemgr-content">
          <div className="filemgr-content-body ">
            <Switch>
              <Route exact path="/manage/account" component={Account} />
              <Route exact path="/manage/permission" component={PermissionGroup} />
              <Route exact path="/manage/news-log" component={NewsLog} />
              <Route exact path="/manage/tags" component={Tags} />
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

export default Manage;
