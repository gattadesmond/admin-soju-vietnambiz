/* eslint-disable jsx-a11y/label-has-for, react/no-array-index-key */
import React, { Component } from 'react';
import { Divider } from 'antd';
import {
  UserOutlined,
  TagOutlined,
  FolderOutlined,
  TagsOutlined,
  ControlOutlined,
  ProfileOutlined,
  HistoryOutlined,
  PaperClipOutlined,
  VideoCameraAddOutlined,
  FundOutlined,
  SolutionOutlined,
  CrownOutlined,
} from '@ant-design/icons';
import { EnumManageSidebar } from '../../../constants';

const dataGroup1 = [
  {
    id: EnumManageSidebar.MANAGE_ACCOUNT.id,
    iconName: <UserOutlined />,
    title: 'Quản lý tài khoản',
  },
  {
    id: EnumManageSidebar.MANAGE_PERMISSION.id,
    iconName: <CrownOutlined />,
    title: 'Phân quyền tài khoản',
  },
  {
    id: EnumManageSidebar.MANAGE_TAG.id,
    iconName: <TagOutlined />,
    title: 'Quản lý Tag',
  },
  {
    id: EnumManageSidebar.MANAGE_ZONE.id,
    iconName: <FolderOutlined />,
    title: 'Quản lý chuyên mục',
  },
  {
    id: EnumManageSidebar.MANAGE_SEO_META_TAG.id,
    iconName: <TagsOutlined />,
    title: 'Meta chuyên mục',
  },
  {
    id: EnumManageSidebar.MANAGE_CONFIG.id,
    iconName: <ControlOutlined />,
    title: 'Cấu hình chung',
  },
];
const dataGroup2 = [
  {
    id: EnumManageSidebar.MANAGE_TOPIC.id,
    iconName: <ProfileOutlined />,
    title: 'Quản lý chủ đề',
  },
  {
    id: EnumManageSidebar.MANAGE_EXPERTS.id,
    iconName: <SolutionOutlined />,
    title: 'Quản lý chuyên gia',
  },
  {
    id: EnumManageSidebar.NEWS_LOG.id,
    iconName: <HistoryOutlined />,
    title: 'Log Action',
  },
  {
    id: EnumManageSidebar.MANAGE_FILE.id,
    iconName: <PaperClipOutlined />,
    title: 'Quản lý File',
  },
];
const dataGroup3 = [
  {
    id: EnumManageSidebar.MANAGE_BOX_VIDEO.id,
    iconName: <VideoCameraAddOutlined />,
    title: 'Box nhúng video',
  },
  {
    id: EnumManageSidebar.MANAGE_BOX_ONPAGE.id,
    iconName: <FundOutlined />,
    title: 'Box nhúng trên trang',
  },
  {
    id: EnumManageSidebar.MANAGE_BOX_TAG.id,
    iconName: <FundOutlined />,
    title: 'Box nhúng tag',
  },
  {
    id: EnumManageSidebar.MANAGE_BOX_TOPIC.id,
    iconName: <FundOutlined />,
    title: 'Box nhúng chủ đề',
  },
  {
    id: EnumManageSidebar.MANAGE_BOX_EXPERTS.id,
    iconName: <FundOutlined />,
    title: 'Box nhúng bài chuyên gia',
  },
];

class LeftNav extends Component {
  render() {
    const { menuActive, onSidebarClick } = this.props;
    return (
      <React.Fragment>
        <div className="filemgr-sidebar">
          <div className="filemgr-sidebar-header">
            <div className="pd-l-25">
              <h4 className="mb-0">CẤU HÌNH</h4>
            </div>
          </div>

          <div className="filemgr-sidebar-body">
            <div className="pd-10">
              <nav className="nav nav-sidebar tx-13">
                {dataGroup1.map((item, index) => {
                  const { iconName, title, id } = item;
                  return (
                    <div
                      key={index}
                      className={`nav-link ${
                        menuActive === id ? ' active' : ''
                      }`}
                      onClick={() => onSidebarClick(id)}
                    >
                      {iconName} <span> {title} </span>
                    </div>
                  );
                })}
              </nav>
            </div>
            <Divider className="mt-2 mb-2" />
            <div className="pd-10">
              <nav className="nav nav-sidebar tx-13">
                {dataGroup2.map((item, index) => {
                  const { iconName, title, id } = item;
                  return (
                    <div
                      key={index}
                      className={`nav-link ${
                        menuActive === id ? ' active' : ''
                      }`}
                      onClick={() => onSidebarClick(id)}
                    >
                      {iconName} <span> {title} </span>
                    </div>
                  );
                })}
              </nav>
            </div>
            <Divider className="mt-2 mb-2" />
            <div className="pd-10">
              <nav className="nav nav-sidebar tx-13">
                {dataGroup3.map((item, index) => {
                  const { iconName, title, id } = item;
                  return (
                    <div
                      key={index}
                      className={`nav-link ${
                        menuActive === id ? ' active' : ''
                      }`}
                      onClick={() => onSidebarClick(id)}
                    >
                      {iconName} <span> {title} </span>
                    </div>
                  );
                })}
              </nav>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default LeftNav;
