import { Button, Form, Input, Modal } from 'antd';
import React, { Component } from 'react';

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
};

class ResetPasswordModal extends Component {
  formRef = React.createRef();

  handleModalSubmit = () => {
    const { onResetPasswordSubmit, encryptId } = this.props;
    const form = this.formRef.current;
    form.validateFields().then((values) => {
      onResetPasswordSubmit(encryptId, values);
    });
  };

  render() {
    const { isVisible, onResetPasswordClose } = this.props;

    return (
      <Modal
        title="Đổi mật khẩu mới"
        visible={isVisible}
        width="600px"
        className="modal-postItem"
        destroyOnClose
        centered
        onCancel={onResetPasswordClose}
        footer={[
          <Button key="back" onClick={onResetPasswordClose}>
            Hủy
          </Button>,
          <Button key="submit" type="primary" onClick={this.handleModalSubmit}>
            Lưu
          </Button>,
        ]}
      >
        <Form
          {...formItemLayout}
          ref={this.formRef}
          size="middle"
          scrollToFirstError
        >
          <Form.Item
            name="password"
            label="Mật khẩu mới"
            rules={[
              {
                required: true,
                message: 'Vui lòng nhập mật khẩu',
              },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            name="confirm"
            label="Xác nhận Mật khẩu"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Vui lòng xác nhận mật khẩu',
              },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  // eslint-disable-next-line prefer-promise-reject-errors
                  return Promise.reject('Hai mật khẩu không khớp nhau');
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}

export default ResetPasswordModal;
