import React from 'react';
import { Table, Tag, Tooltip } from 'antd';
import EditOutlined from '@ant-design/icons/lib/icons/EditOutlined';
import LockOutlined from '@ant-design/icons/lib/icons/LockOutlined';
import SettingOutlined from '@ant-design/icons/lib/icons/SettingOutlined';
import CrownOutlined from '@ant-design/icons/lib/icons/CrownOutlined';
import { isEmpty, numberWithCommas } from '../../../../../utils';

import imgAvatarDefault from '../../../../../assets/static/avatar.png';

const AccountList = (props) => {
  const {
    dataSource,
    selectedRowKeys,
    onRowSelectedChange,
    pageSize,
    total,
    onPageSizeChange,
    onPageIndexChange,
    onDetailClick,
    onResetPasswordClick,
    onAddEditPermissionOpen,
  } = props;
  const rowSelection = {
    selectedRowKeys,
    onChange: onRowSelectedChange,
  };
  const columns = [
    {
      title: 'Avatar',
      dataIndex: 'avatar',
      render: (text, record) => {
        if (!isEmpty(record.avatar)) {
          return (
            <img
              src={record.avatar}
              alt={record.username}
              width={30}
              height={30}
            />
          );
        }
        return (
          <img
            src={imgAvatarDefault}
            alt={record.username}
            width={30}
            height={30}
          />
        );
      },
    },
    {
      title: 'Tên tài khoản',
      dataIndex: 'userName',
    },
    {
      title: 'Họ tên',
      dataIndex: 'fullName',
    },
    {
      title: 'Email',
      dataIndex: 'email',
    },
    {
      title: 'Điện thoại',
      dataIndex: 'mobile',
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      render: (text, record) => {
        if (record.status === 1) {
          return <Tag color="success">Đang hoạt động</Tag>;
        }
        return <Tag color="error">Đang bị khóa</Tag>;
      },
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <span>
          <Tooltip placement="bottom" title="Chỉnh sửa thông tin">
            <EditOutlined
              onClick={() => onDetailClick(record.encryptId)}
              style={{
                cursor: 'pointer',
                marginRight: '5px',
                fontSize: '16px',
              }}
            />
          </Tooltip>
          <Tooltip placement="bottom" title="Đổi mật khẩu">
            <LockOutlined
              onClick={() => onResetPasswordClick(record.encryptId)}
              style={{
                cursor: 'pointer',
                marginRight: '5px',
                fontSize: '16px',
              }}
            />
          </Tooltip>
          <Tooltip placement="bottom" title="Phân quyền">
            <CrownOutlined
              onClick={() => onAddEditPermissionOpen(record.id)}
              style={{
                cursor: 'pointer',
                marginRight: '5px',
                fontSize: '16px',
              }}
            />
          </Tooltip>
          <Tooltip placement="bottom" title="Reset OTP">
            <SettingOutlined style={{ cursor: 'pointer', fontSize: '16px' }} />
          </Tooltip>
        </span>
      ),
    },
  ];
  return (
    <Table
      pagination={{
        position: 'bottom',
        size: 'small',
        pageSize,
        total,
        showTotal: (total) => `Total ${numberWithCommas(total)} items`,
        showSizeChanger: true,
        showQuickJumper: true,
        onShowSizeChange: onPageSizeChange,
        onChange: onPageIndexChange,
      }}
      rowSelection={rowSelection}
      columns={columns}
      dataSource={dataSource}
    />
  );
};

export default AccountList;
