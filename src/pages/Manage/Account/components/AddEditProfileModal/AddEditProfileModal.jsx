import React, { Component } from 'react';
import { Button, DatePicker, Form, Input, Modal, Upload } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import isEqual from 'react-fast-compare';
import moment from 'moment';
import { __API_ROOT__ } from 'global.js';
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
};

const minDate = moment.utc('0001-01-01');

class AddEditProfileModal extends Component {
  componentDidUpdate(prevProps, prevState, prevContext) {
    const { detail: prevDetail } = prevProps;
    const { detail: thisDetail } = this.props;
    if (!isEqual(prevDetail, thisDetail)) {
      const { avatar, email, fullName, userName, mobile, birthDay, address } =
        thisDetail || {};
      const form = this.formRef.current;
      form.setFieldsValue({
        avatar,
        email,
        fullName,
        userName,
        mobile,
        birthDay: moment(birthDay).isAfter(minDate) ? moment(birthDay) : '',
        address,
      });
    }
  }

  formRef = React.createRef();

  handleModalSubmit = () => {
    const { onAddEditProfileSubmit } = this.props;
    const form = this.formRef.current;
    form.validateFields().then((values) => {
      onAddEditProfileSubmit(values);
    });
  };

  render() {
    const {
      isVisible,
      detail,
      onBeforeUpload,
      onAvatarChange,
      onAddEditProfileClose,
    } = this.props;
    const { avatar, id } = detail || {};
    return (
      <Modal
        title="Thông tin tài khoản"
        visible={isVisible}
        width="900px"
        className="modal-postItem"
        destroyOnClose
        centered
        onCancel={onAddEditProfileClose}
        footer={[
          <Button key="back" onClick={onAddEditProfileClose}>
            Hủy
          </Button>,
          <Button key="submit" type="primary" onClick={this.handleModalSubmit}>
            Lưu
          </Button>,
        ]}
      >
        <Form
          {...formItemLayout}
          ref={this.formRef}
          size="middle"
          scrollToFirstError
        >
          <Form.Item label="Avatar" name="avatar" valuePropName="avatar">
            <Upload
              name="avatar"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action={`${__API_ROOT__}news/uploadAvatar`}
              beforeUpload={onBeforeUpload}
              onChange={onAvatarChange}
            >
              {avatar ? (
                <img src={avatar} alt="avatar" style={{ width: '100%' }} />
              ) : (
                <div>
                  <UploadOutlined />
                </div>
              )}
            </Upload>
          </Form.Item>
          <Form.Item
            label="Tên tài khoản"
            name="userName"
            rules={[
              { required: true, message: 'Vui lòng nhập tên tài khoản' },
              { min: 3, message: 'Tên tài khoản tối thiểu 3 ký tự' },
              { max: 15, message: 'Tên tài khoản tối đa 15 ký tự' },
            ]}
            hasFeedback
          >
            <Input min={5} max={30} disabled={id !== -1} />
          </Form.Item>
          {id === -1 && (
            <React.Fragment>
              <Form.Item
                name="password"
                label="Mật khẩu"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập mật khẩu',
                  },
                ]}
                hasFeedback
              >
                <Input.Password />
              </Form.Item>
              <Form.Item
                name="confirm"
                label="Xác nhận Mật khẩu"
                dependencies={['password']}
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng xác nhận mật khẩu',
                  },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (!value || getFieldValue('password') === value) {
                        return Promise.resolve();
                      }
                      // eslint-disable-next-line prefer-promise-reject-errors
                      return Promise.reject('Hai mật khẩu không khớp nhau');
                    },
                  }),
                ]}
              >
                <Input.Password />
              </Form.Item>
            </React.Fragment>
          )}
          <Form.Item
            label="Họ tên"
            name="fullName"
            rules={[
              { required: true, message: 'Vui lòng nhập tên người dùng' },
            ]}
            hasFeedback
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                type: 'email',
                message: 'Vui lòng nhập Email đúng định dạng',
              },
              {
                required: true,
                message: 'Vui lòng nhập email',
              },
            ]}
            hasFeedback
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Địện thoại"
            name="mobile"
            hasFeedback
          >
            <Input />
          </Form.Item>
          <Form.Item label="Ngày sinh" name="birthDay">
            <DatePicker style={{ width: '100%' }} format="DD-MM-YYYY" />
          </Form.Item>
          <Form.Item label="Địa chỉ" name="address">
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}

export default AddEditProfileModal;
