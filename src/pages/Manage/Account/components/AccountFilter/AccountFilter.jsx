import React from 'react';
import { Button, Card, Col, Input, Row, Select, Switch } from 'antd';

const AccountFilter = (props) => {
  const {
    optionActionFilter,
    optionsKeywordFilter,
    filterSelected,
    actionSelected,
    selectedRowKeys,
    keyword,
    optionsAccountStatus,
    status,
    onAccountStatusChange,
    onKeywordFilterChange,
    onActionFilterChange,
    onSearchClick,
    onKeywordChange,
    onAccountCreate,
  } = props;
  return (
    <React.Fragment>
      <Row>
        {selectedRowKeys.length > 0 && (
          <Col span={8}>
            <Card>
              <Select value={actionSelected} onChange={onActionFilterChange}>
                {optionActionFilter}
              </Select>
              <Button type="primary">THỰC HIỆN</Button>
            </Card>
          </Col>
        )}
        <Col span={selectedRowKeys.length > 0 ? 16 : 24}>
          <Card>
            <Row>
              <Input.Group compact style={{ width: '400px' }}>
                <Input
                  style={{ width: '60%' }}
                  value={keyword}
                  onChange={onKeywordChange}
                />
                <Select
                  value={filterSelected}
                  style={{ width: '40%' }}
                  onChange={onKeywordFilterChange}
                >
                  {optionsKeywordFilter}
                </Select>
              </Input.Group>
              <Select
                value={status}
                onChange={onAccountStatusChange}
              >
                {optionsAccountStatus}
              </Select>
              <Button type="primary" onClick={() => onSearchClick()}>
                LỌC
              </Button>
              <Button type="primary" onClick={() => onAccountCreate()}>
                TẠO MỚI TÀI KHOẢN
              </Button>
            </Row>
          </Card>
        </Col>
      </Row>
    </React.Fragment>
  );
};
export default AccountFilter;
