import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { message, Modal } from 'antd';
import isEqual from 'react-fast-compare';
import { ExclamationCircleOutlined } from '@ant-design/icons';

import AccountFilter from './components/AccountFilter/AccountFilter';
import AccountList from './components/AccountList/AccountList';
import {
  EnumAccountKeywordFilter,
  EnumAccountStatus,
} from '../../../constants';
import { EnumAccountActionFilter } from '../../../constants/enum';
import {
  addEditPermissionClose,
  addEditPermissionOpen,
  addEditProfileClose,
  addEditProfileOpen,
  getAccounts,
  reloadAccount,
  resetPassword,
  resetPasswordClose,
  resetPasswordOpen,
  saveAccount,
} from '../../../actions/account.action';
import {
  generateAntOptionsFromEnum,
  isEmpty,
  removeParamsForServer,
} from '../../../utils';
import AddEditProfileModal from './components/AddEditProfileModal/AddEditProfileModal';
import ResetPasswordModal from './components/ResetPasswordModal/ResetPasswordModal';
import AddEditPermissionModal from './components/AddEditPermissionModal/AddEditPermissionModal';

const { confirm } = Modal;

class Account extends Component {
  constructor(props) {
    super(props);
    const { accountReducer } = this.props;
    const { detail } = accountReducer || {};
    this.state = {
      filterSelected: EnumAccountKeywordFilter.USERNAME.id,
      actionSelected: EnumAccountActionFilter.LOCK_ACCOUNT.id,
      keyword: '',
      username: null,
      fullname: null,
      mobile: null,
      email: null,
      status: -1,
      editingAccount: detail,
      selectedRowKeys: [],
      optionsKeywordFilter: generateAntOptionsFromEnum(
        EnumAccountKeywordFilter,
      ),
      optionActionFilter: generateAntOptionsFromEnum(EnumAccountActionFilter),
      optionsAccountStatus: generateAntOptionsFromEnum(EnumAccountStatus, {
        id: -1,
        name: 'Tất cả',
      }),
      pageSize: 10,
      pageIndex: 1,
      encryptId: '',
    };
  }

  componentDidMount() {
    const { reloadAccountConnect } = this.props;
    reloadAccountConnect(false);
    this.handleSearchClick();
  }

  componentDidUpdate(prevProps, prevState, prevContext) {
    const { accountReducer: prevAccountReducer } = prevProps;
    const { detail: prevDetail, isReload: prevIsReload } = prevAccountReducer || {};
    const { accountReducer: thisAccountReducer } = this.props;
    const { detail: thisDetail, isReload } = thisAccountReducer || {};
    if (!isEqual(prevDetail, thisDetail)) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        editingAccount: thisDetail,
      });
    }
    if (!isEqual(prevIsReload, isReload) && isReload) {
      this.handleSearchClick();
    }
  }

  handleSearchClick = (pageIndex = 1) => {
    this.setState({ pageIndex }, () => {
      const {
        status,
        username,
        fullname,
        email,
        mobile,
        pageSize,
        pageIndex,
      } = this.state;
      const { getAccountsConnect } = this.props;
      getAccountsConnect(
        status,
        username,
        fullname,
        email,
        mobile,
        pageSize,
        pageIndex,
      );
    });
  };

  handleRowSelectedChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
  };

  handleAccountStatusChange = (status) => {
    this.setState({ status });
  };

  handleKeywordFilterChange = (filterSelected) => {
    const { keyword } = this.state;
    switch (filterSelected) {
      case EnumAccountKeywordFilter.FULLNAME.id: {
        this.setState({
          filterSelected,
          fullname: keyword,
          username: null,
          mobile: null,
          email: null,
        });
        break;
      }
      case EnumAccountKeywordFilter.EMAIL.id: {
        this.setState({
          filterSelected,
          fullname: null,
          username: null,
          mobile: null,
          email: keyword,
        });
        break;
      }
      case EnumAccountKeywordFilter.MOBILE.id: {
        this.setState({
          filterSelected,
          fullname: null,
          username: null,
          mobile: keyword,
          email: null,
        });
        break;
      }
      default: {
        this.setState({
          filterSelected,
          fullname: null,
          username: keyword,
          mobile: null,
          email: null,
        });
        break;
      }
    }
  };

  handleActionFilterChange = (actionSelected) => {
    this.setState({ actionSelected });
  };

  handleKeywordChange = (e) => {
    const { filterSelected } = this.state;
    const { value } = e.target;
    switch (filterSelected) {
      case EnumAccountKeywordFilter.FULLNAME.id: {
        this.setState({
          keyword: value,
          fullname: value,
          username: null,
          mobile: null,
          email: null,
        });
        break;
      }
      case EnumAccountKeywordFilter.EMAIL.id: {
        this.setState({
          keyword: value,
          fullname: null,
          username: null,
          mobile: null,
          email: value,
        });
        break;
      }
      case EnumAccountKeywordFilter.MOBILE.id: {
        this.setState({
          keyword: value,
          fullname: null,
          username: null,
          mobile: value,
          email: null,
        });
        break;
      }
      default: {
        this.setState({
          keyword: value,
          fullname: null,
          username: value,
          mobile: null,
          email: null,
        });
        break;
      }
    }
  };

  handlePageSizeChange = (currentPage, pageSize) => {
    this.setState({ pageSize }, () => this.handleSearchClick(currentPage));
  };

  handlePageIndexChange = (pageIndex) => {
    this.handleSearchClick(pageIndex);
  };

  handleDetailClick = (encryptId = null) => {
    const { addEditProfileOpenConnect } = this.props;
    addEditProfileOpenConnect(encryptId);
  };

  handleResetPasswordClick = (encryptId) => {
    this.setState({ encryptId }, () => {
      const { resetPasswordOpenConnect } = this.props;
      resetPasswordOpenConnect();
    });
  };

  handleAddEditProfileClose = () => {
    const { addEditProfileCloseConnect } = this.props;
    addEditProfileCloseConnect();
    this.handleSearchClick();
  };

  handleResetPasswordClose = () => {
    const { resetPasswordCloseConnect } = this.props;
    resetPasswordCloseConnect();
    this.handleSearchClick();
  };

  handleResetPasswordSubmit = async (encryptId, values) => {
    const { resetPasswordConnect } = this.props;
    const password = values.password;
    const response = await resetPasswordConnect(encryptId, password);
    const onOkClick = () => {
      this.handleResetPasswordClose();
    };
    if (response > 0) {
      Modal.info({
        title: 'Thông báo',
        content: 'Đổi mật khẩu thành công',
        onOk() {
          onOkClick();
        },
      });
    } else {
      Modal.error({
        title: 'Lỗi',
        content: 'Lưu bài viết không thành công',
      });
    }
  };

  handleAddEditProfileSubmit = (values) => {
    const cloneValues = { ...values };
    if (typeof values.avatar === 'object') {
      cloneValues.avatar = values.avatar.file.response.file.url;
    }
    if (!isEmpty(values.birthDay)) {
      cloneValues.birthDay = values.birthDay.format('YYYY-MM-DD');
    }
    const { editingAccount } = this.state;
    const onOKClick = () => {
      this.handleAddEditProfileClose();
      this.handleSearchClick();
    };
    this.setState(
      {
        editingAccount: {
          ...editingAccount,
          ...removeParamsForServer(cloneValues, [], undefined),
        },
      },
      async () => {
        const { editingAccount } = this.state;
        const { saveAccountConnect } = this.props;
        const response = await saveAccountConnect(editingAccount);
        if (response > 0) {
          confirm({
            title: 'Xác nhận',
            icon: <ExclamationCircleOutlined />,
            content:
              'Tài khoản đã được lưu thành công. Bạn có muốn đóng form chỉnh sửa?',
            onOk() {
              onOKClick();
            },
          });
        } else {
          Modal.error({
            title: 'Lỗi',
            content: 'Lưu bài viết không thành công',
          });
        }
      },
    );
  };

  handleAccountCreate = () => {
    const { addEditProfileOpenConnect } = this.props;
    addEditProfileOpenConnect();
  };

  handleBeforeUpload = (file) => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJpgOrPng && isLt2M;
  };

  handleAvatarChange = (info) => {
    const { editingAccount } = this.state;
    if (info.file.status === 'done') {
      this.setState({
        editingAccount: {
          ...editingAccount,
          avatar: info.file.response.file.url,
        },
      });
    }
  };

  handleAddEditPermissionOpen = (encryptId) => {
    const { addEditPermissionOpenConnect } = this.props;
    console.log('encryptId', encryptId);
    addEditPermissionOpenConnect(encryptId);
  };

  handleAddEditPermissionClose = () => {
    const { addEditPermissionCloseConnect } = this.props;
    addEditPermissionCloseConnect();
  };
  render() {
    const {
      optionsKeywordFilter,
      optionActionFilter,
      optionsAccountStatus,
      filterSelected,
      selectedRowKeys,
      actionSelected,
      status,
      pageSize,
      pageIndex,
      editingAccount,
      encryptId,
    } = this.state;
    const { accountReducer } = this.props;
    const {
      accounts,
      total,
      addEditProfileModalOpen,
      resetPasswordModalOpen,
      addEditPermissionModalOpen,
      accountId,
    } = accountReducer || {};
    return (
      <React.Fragment>
        <AccountFilter
          status={status}
          selectedRowKeys={selectedRowKeys}
          actionSelected={actionSelected}
          optionsAccountStatus={optionsAccountStatus}
          optionActionFilter={optionActionFilter}
          optionsKeywordFilter={optionsKeywordFilter}
          filterSelected={filterSelected}
          onAccountStatusChange={this.handleAccountStatusChange}
          onKeywordFilterChange={this.handleKeywordFilterChange}
          onActionFilterChange={this.handleActionFilterChange}
          onSearchClick={this.handleSearchClick}
          onKeywordChange={this.handleKeywordChange}
          onAccountCreate={this.handleAccountCreate}
        />
        <AccountList
          selectedRowKeys={selectedRowKeys}
          total={total}
          pageSize={pageSize}
          pageIndex={pageIndex}
          dataSource={accounts}
          onAddEditPermissionOpen={this.handleAddEditPermissionOpen}
          onPageSizeChange={this.handlePageSizeChange}
          onPageIndexChange={this.handlePageIndexChange}
          onRowSelectedChange={this.handleRowSelectedChange}
          onDetailClick={this.handleDetailClick}
          onResetPasswordClick={this.handleResetPasswordClick}
        />
        <AddEditProfileModal
          isVisible={addEditProfileModalOpen}
          detail={editingAccount}
          onAddEditProfileClose={this.handleAddEditProfileClose}
          onAddEditProfileSubmit={this.handleAddEditProfileSubmit}
          onBeforeUpload={this.handleBeforeUpload}
          onAvatarChange={this.handleAvatarChange}
        />
        <ResetPasswordModal
          isVisible={resetPasswordModalOpen}
          encryptId={encryptId}
          onResetPasswordClose={this.handleResetPasswordClose}
          onResetPasswordSubmit={this.handleResetPasswordSubmit}
        />
        <AddEditPermissionModal
          accountId={accountId}
          isVisible={addEditPermissionModalOpen}
          onAddEditPermissionClose={this.handleAddEditPermissionClose}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  accountReducer: state.account,
});

const mapDispatchToProps = {
  getAccountsConnect: getAccounts,
  addEditProfileOpenConnect: addEditProfileOpen,
  addEditProfileCloseConnect: addEditProfileClose,
  resetPasswordOpenConnect: resetPasswordOpen,
  resetPasswordCloseConnect: resetPasswordClose,
  saveAccountConnect: saveAccount,
  resetPasswordConnect: resetPassword,
  addEditPermissionOpenConnect: addEditPermissionOpen,
  addEditPermissionCloseConnect: addEditPermissionClose,
  reloadAccountConnect: reloadAccount,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Account),
);
