import React, { Component } from 'react';
import { Button, Checkbox, Col, Input, Modal, Row, Tree } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import isEqual from 'react-fast-compare';
import _ from 'lodash';
import {
  configAccountClose,
  configAccountOpen,
  configPermissionSave,
  configPermissionClose,
  configPermissionOpen,
  getConfigPermission,
  getPermissionGroup,
} from '../../../../../actions/permission.action';

const { confirm } = Modal;

class AddEditPermissionModal extends Component {
  constructor(props) {
    super(props);
    const { globalReducer } = props;
    const { permissions, zones } = globalReducer;
    const permissionData = [];
    const zoneData = [];
    permissions.forEach((permission) => {
      const { id, parentId, isZoneAccess, name } = permission;
      if (parentId === 0) {
        permissionData.push({
          key: id,
          title: name,
          isZoneAccess,
          children: [],
        });
      }
    });

    permissions.forEach((permission) => {
      const { id, parentId, isZoneAccess, name } = permission;
      if (parentId !== 0) {
        const index = permissionData.findIndex((x) => x.key === parentId);
        permissionData[index].children.push({
          key: id,
          title: name,
          isZoneAccess,
        });
      }
    });

    zones.forEach((zone) => {
      const { id, parentId, name } = zone;
      if (parentId === 0) {
        zoneData.push({ key: id, title: name, children: [] });
      }
    });

    zones.forEach((zone) => {
      const { id, parentId, name } = zone;
      if (parentId !== 0) {
        const index = zoneData.findIndex((x) => x.key === parentId);
        if (index !== -1) {
          zoneData[index - 1].children.push({ key: id, title: name });
        }
      }
    });

    this.state = {
      expandedKeys: [],
      checkedKeys: [],
      selectedKeys: [],
      autoExpandParent: true,
      zoneData,
      permissionData,
      zoneExpandedKeys: [],
      zoneCheckedKeys: [],
      zoneAutoExpandParent: true,
      permissionZoneMap: new Map(),
      isCheckAllZones: false,
    };
  }

  componentDidUpdate(prevProps, prevState, prevContext) {
    const { permissionReducer: prevPermissionReducer } = prevProps;
    const { permissionReducer } = this.props;
    const {
      permissionCheckedKeys: prevPermissionCheckedKeys,
      permissionZoneMap: prevPermissionZoneMap,
    } = prevPermissionReducer || {};
    const { permissionCheckedKeys, permissionZoneMap } =
      permissionReducer || {};
    if (!isEqual(prevPermissionCheckedKeys, permissionCheckedKeys)) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({ checkedKeys: permissionCheckedKeys });
    }
    if (!isEqual(prevPermissionZoneMap, permissionZoneMap)) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({ permissionZoneMap });
    }
  }

  handleCheckAllZonesChecked = (e) => {
    const { checked } = e.target;
    const { selectedKeys, permissionZoneMap } = this.state;
    const { globalReducer } = this.props;
    const { zones } = globalReducer;
    let zoneCheckedKeys = [];
    if (checked) {
      permissionZoneMap.set(
        selectedKeys[0],
        zones.map((zone) => zone.id).join(','),
      );
      zoneCheckedKeys = zones.map((zone) => zone.id);
    } else {
      permissionZoneMap.delete(selectedKeys[0]);
    }
    this.setState({
      isCheckAllZones: checked,
      permissionZoneMap,
      zoneCheckedKeys,
    });
  };

  handleModalSubmit = async () => {
    const { permissionZoneMap } = this.state;
    const {
      permissionGroupId,
      configPermissionSaveConnect,
      configPermissionCloseConnect,
      getPermissionGroupConnect,
    } = this.props;
    const request = await configPermissionSaveConnect(
      permissionZoneMap,
      permissionGroupId,
    );
    if (request) {
      confirm({
        title: 'Xác nhận',
        icon: <ExclamationCircleOutlined />,
        content:
          'Thiết lập phân quyền thành công. Bạn có muốn đóng form chỉnh sửa?',
        onOk() {
          getPermissionGroupConnect();
          configPermissionCloseConnect();
        },
      });
    }
  };

  handlePermissionTreeExpand = (expandedKeys) => {
    // or, you can remove all expanded children keys.

    this.setState({
      expandedKeys,
      autoExpandParent: false,
    });
  };

  handlePermissionTreeCheck = (values) => {
    const { checkedKeys: currentCheckedKeys, permissionZoneMap } = this.state;
    const { globalReducer } = this.props;
    const { zones } = globalReducer || {};
    const checkedKeys = values.checked;
    const clonePermissionZoneMap = new Map(permissionZoneMap);
    const removedValues = _.difference(currentCheckedKeys, checkedKeys);
    const addedValues = _.difference(checkedKeys, currentCheckedKeys);
    if (removedValues.length > 0) {
      removedValues.forEach((item) => clonePermissionZoneMap.delete(item));
    }
    if (addedValues.length > 0) {
      addedValues.forEach((item) =>
        clonePermissionZoneMap.set(
          item,
          zones.map((x) => x.id),
        ),
      );
    }
    this.setState({ checkedKeys, permissionZoneMap: clonePermissionZoneMap });
  };

  handlePermissionTreeSelect = (selectedKeys) => {
    if (selectedKeys && selectedKeys.length > 0) {
      const { permissionZoneMap } = this.state;
      const currentCheckedKeys = permissionZoneMap.get(selectedKeys[0]);
      this.setState({
        selectedKeys,
        zoneCheckedKeys: currentCheckedKeys,
      });
    } else {
      this.setState({ selectedKeys });
    }
  };

  handleZoneTreeExpand = (zoneExpandedKeys) => {
    // or, you can remove all expanded children keys.

    this.setState({
      zoneExpandedKeys,
      zoneAutoExpandParent: false,
    });
  };

  handleZoneTreeCheck = (zoneCheckedKeys) => {
    const { selectedKeys, permissionZoneMap } = this.state;
    permissionZoneMap.set(selectedKeys[0], zoneCheckedKeys);
    this.setState({
      zoneCheckedKeys,
      permissionZoneMap,
    });
  };

  render() {
    const {
      autoExpandParent,
      zoneAutoExpandParent,
      expandedKeys,
      checkedKeys,
      selectedKeys,
      zoneExpandedKeys,
      zoneCheckedKeys,
      zoneData,
      permissionData,
      isCheckAllZones,
    } = this.state;
    const {
      onAddEditPermissionClose,
      isVisible,
      permissionGroupName,
    } = this.props;
    return (
      <Modal
        title="Chỉnh sửa quyền đang áp dụng"
        visible={isVisible}
        width="900px"
        className="modal-postItem"
        destroyOnClose
        centered
        onCancel={onAddEditPermissionClose}
        footer={[
          <Button key="back" onClick={onAddEditPermissionClose}>
            Hủy
          </Button>,
          <Button key="submit" type="primary" onClick={this.handleModalSubmit}>
            Lưu
          </Button>,
        ]}
      >
        <Row>
          <Col span={8}>Tên nhóm quyền</Col>
          <Col span={8}>
            <Input value={permissionGroupName} disabled />
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <Tree
              checkStrictly
              checkable
              treeData={permissionData}
              autoExpandParent={autoExpandParent}
              expandedKeys={expandedKeys}
              checkedKeys={checkedKeys}
              selectedKeys={selectedKeys}
              onExpand={this.handlePermissionTreeExpand}
              onCheck={this.handlePermissionTreeCheck}
              onSelect={this.handlePermissionTreeSelect}
            />
          </Col>
          {selectedKeys.length > 0 && (
            <Col span={12}>
              <Checkbox
                checked={isCheckAllZones}
                onChange={this.handleCheckAllZonesChecked}
                style={{ marginLeft: '24px' }}
              >
                Chọn tất cả chuyên mục
              </Checkbox>
              <Tree
                checkable
                selectable={false}
                treeData={zoneData}
                autoExpandParent={zoneAutoExpandParent}
                expandedKeys={zoneExpandedKeys}
                checkedKeys={zoneCheckedKeys}
                onExpand={this.handleZoneTreeExpand}
                onCheck={this.handleZoneTreeCheck}
              />
            </Col>
          )}
        </Row>
      </Modal>
    );
  }
}

const mapStateToProps = (state) => ({
  permissionReducer: state.permission,
  globalReducer: state.global,
});

const mapDispatchToProps = {
  getPermissionGroupConnect: getPermissionGroup,
  configPermissionOpenConnect: configPermissionOpen,
  configPermissionCloseConnect: configPermissionClose,
  configAccountOpenConnect: configAccountOpen,
  configAccountCloseConnect: configAccountClose,
  configPermissionSaveConnect: configPermissionSave,
  getConfigPermissionConnect: getConfigPermission,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(AddEditPermissionModal),
);
