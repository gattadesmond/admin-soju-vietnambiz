import React from 'react';
import { Table, Tag, Tooltip } from 'antd';
import EditOutlined from '@ant-design/icons/lib/icons/EditOutlined';

const PermissionGroupList = (props) => {
  const { dataSource, onConfigPermissionClick, onConfigAccountClick } = props;
  const columns = [
    {
      title: 'Tên nhóm quyền',
      dataIndex: 'name',
    },
    {
      title: 'Số quyền đang áp dụng',
      dataIndex: 'totalPermission',
      align: 'center',
      render: (text, record) => (
        <Tooltip placement="bottom" title="Chỉnh sửa quyền đang áp dụng">
          <Tag
            color="magenta"
            style={{ cursor: 'pointer' }}
            onClick={() => onConfigPermissionClick(record.id)}
          >
            {`${record.totalPermission} quyền`}
          </Tag>
        </Tooltip>
      ),
    },
    {
      title: 'Trạng thái',
      dataIndex: 'isActive',
      align: 'center',
      render: (text, record) => {
        if (record.isActive) {
          return <Tag color="success">Đang hoạt động</Tag>;
        }
        return <Tag color="error">Đang bị khóa</Tag>;
      },
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <span>
          <Tooltip placement="bottom" title="Chỉnh sửa thông tin">
            <EditOutlined
              style={{
                cursor: 'pointer',
                marginRight: '5px',
                fontSize: '16px',
              }}
            />
          </Tooltip>
        </span>
      ),
    },
  ];
  return <Table columns={columns} dataSource={dataSource} />;
};

export default PermissionGroupList;
