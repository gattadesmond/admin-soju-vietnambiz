import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PermissionGroupList from './components/PermissionGroupList/PermissionGroupList';
import {
  configPermissionClose,
  configPermissionOpen,
  getPermissionGroup,
} from '../../../actions/permission.action';
import AddEditPermissionModal from './components/AddEditPermissionModal/AddEditPermissionModal';
import { isEmpty } from '../../../utils';

class PermissionGroup extends Component {
  componentDidMount() {
    const { getPermissionGroupConnect } = this.props;
    getPermissionGroupConnect();
  }

  handleConfigPermissionClick = (permissionGroupId) => {
    const { configPermissionOpenConnect } = this.props;
    configPermissionOpenConnect(permissionGroupId);
  };

  handleAddEditPermissionClose = () => {
    const { configPermissionCloseConnect } = this.props;
    configPermissionCloseConnect();
  };

  handleConfigAccountClick = (permissionGroupId) => {
    const { configAccountOpenConnect } = this.props;
    configAccountOpenConnect(permissionGroupId);
  };

  handleAddEditAccountClose = () => {
    const { configAccountCloseConnect } = this.props;
    configAccountCloseConnect();
  };

  render() {
    const { permissionReducer } = this.props;
    const {
      permissionGroups,
      permissionGroupTotal,
      configPermissionModalOpen,
      configAccountModalOpen,
      accountConfig,
      permissionGroupId,
    } = permissionReducer;

    let permissionGroupName = '';
    if (!isEmpty(permissionGroupId)) {
      permissionGroupName = permissionGroups.find(
        (x) => x.id === permissionGroupId,
      ).name;
    }
    return (
      <React.Fragment>
        <PermissionGroupList
          dataSource={permissionGroups}
          total={permissionGroupTotal}
          onConfigPermissionClick={this.handleConfigPermissionClick}
          onConfigAccountClick={this.handleConfigAccountClick}
        />
        <AddEditPermissionModal
          permissionGroupId={permissionGroupId}
          permissionGroupName={permissionGroupName}
          isVisible={configPermissionModalOpen}
          onAddEditPermissionClose={this.handleAddEditPermissionClose}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  permissionReducer: state.permission,
});

const mapDispatchToProps = {
  getPermissionGroupConnect: getPermissionGroup,
  configPermissionOpenConnect: configPermissionOpen,
  configPermissionCloseConnect: configPermissionClose,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(PermissionGroup),
);
